//----------------------------------------------------------------------------------------------------------------------
// Middleware End-to-End Tests
//----------------------------------------------------------------------------------------------------------------------

import { setTimeout } from 'timers/promises';
import { expect } from 'chai';

// Clients
import { StrataService } from '../../src/index.js';

// Managers
import backendMan from '../../src/managers/backend.js';

// Lib
import { StrataRequest } from '../../src/lib/request.js';
import { StrataContext } from '../../src/lib/context.js';

// Test Utils
import { TestMiddleware } from '../utils/middleware.js';

//----------------------------------------------------------------------------------------------------------------------

describe('Middleware Functionality', () =>
{
    let request;
    let service : StrataService;
    let context : StrataContext;
    let testMiddlewareInst : TestMiddleware;

    beforeEach(async() =>
    {
        context = new StrataContext();

        service = new StrataService({ service: { serviceGroup: 'test' }, backend: { type: 'null' } });
        service.registerContext('test', context);
        await service.start();

        request = new StrataRequest({
            id: '1234567890',
            context: 'test',
            operation: 'foo',
            messageType: 'request',
            payload: {},
            metadata: {},
        });

        testMiddlewareInst = new TestMiddleware();

        backendMan.clear();
        backendMan.registerInternalBackends();
    });

    it('it stops calling `beforeRequest` functions if any middleware throws a error', async() =>
    {
        let opCalled = false;
        const mw1 = new TestMiddleware('ErrorMiddleware', {
            beforeFn: () =>
            {
                throw new Error('Fail!');
            },
        });

        const mw2 = new TestMiddleware('SecondMiddleware');
        const mw3 = new TestMiddleware('ThirdMiddleware');

        service.useMiddleware(mw1);
        service.useMiddleware(mw2);
        context.useMiddleware(mw3);

        context.registerOperation('foo', async() =>
        {
            opCalled = true;
        });

        try
        {
            // Call operation
            service.queueEngine.backend.emit('incomingRequest', request.renderRequest());
            await setTimeout(10);
        }
        catch (_ex) { /* do nothing */ }

        // Tests
        expect(opCalled).to.equal(false);
        expect(mw1.beforeCalled).to.equal(1);
        expect(mw2.beforeCalled).to.equal(0);
        expect(mw3.beforeCalled).to.equal(0);
    });

    it('it stops calling `beforeRequest` functions if any middleware succeeds or fails a request', async() =>
    {
        let opCalled = false;
        const mwA = new TestMiddleware('OtherMiddleware');
        const mwB = new TestMiddleware('OtherMiddleware');
        const mw1 = new TestMiddleware('ErrorMiddleware', {
            beforeFn: () =>
            {
                request = service.outstandingRequests.get(request.id);
                request.fail({});
            },
        });

        const mw2 = new TestMiddleware('SecondMiddleware');
        const mw3 = new TestMiddleware('ThirdMiddleware');

        service.useMiddleware([ mwA, mwB, mw1, mw2 ]);
        context.useMiddleware(mw3);

        context.registerOperation('foo', async() =>
        {
            opCalled = true;
        });

        // Call operation
        service.queueEngine.backend.emit('incomingRequest', request.renderRequest());
        await setTimeout(10);

        // Tests
        expect(opCalled).to.equal(false);
        expect(mwA.beforeCalled).to.equal(1);
        expect(mwB.beforeCalled).to.equal(1);
        expect(mw1.beforeCalled).to.equal(1);
        expect(mw2.beforeCalled).to.equal(0);
        expect(mw3.beforeCalled).to.equal(0);
    });

    // eslint-disable-next-line @stylistic/max-len
    it('it still calls `success` or `failure` on all middleware if a request is succeeded or failed in `beforeRequest`', async() =>
    {
        let opCalled = false;
        const mw1 = new TestMiddleware('ErrorMiddleware', {
            beforeFn: () =>
            {
                request = service.outstandingRequests.get(request.id);
                request.succeed({});
            },
        });

        const mw2 = new TestMiddleware('SecondMiddleware');

        service.useMiddleware([ mw1, mw2 ]);

        context.registerOperation('foo', async() =>
        {
            opCalled = true;
        });

        // Call operation
        service.queueEngine.backend.emit('incomingRequest', request.renderRequest());
        await setTimeout(10);

        // Tests
        expect(opCalled).to.equal(false);
        expect(mw1.beforeCalled).to.equal(1);
        expect(mw2.beforeCalled).to.equal(0);
        expect(mw1.successCalled).to.equal(1);
        expect(mw2.successCalled).to.equal(1);
    });

    it('fails the request if the operation throws.', async() =>
    {
        let opCalled = false;
        service.useMiddleware(testMiddlewareInst);
        context.registerOperation('foo', async() =>
        {
            opCalled = true;
            request = service.outstandingRequests.get(request.id);
            throw new Error('Failure!');
        });

        try
        {
            // Call operation
            service.queueEngine.backend.emit('incomingRequest', request.renderRequest());
            await setTimeout(10);
        }
        catch (_ex) { /* do nothing */ }

        // Tests
        expect(opCalled).to.equal(true);
        expect(request.status).to.equal('failed');
    });

    it('calls the `success` function on all registered middleware if the operation succeeds the request', async() =>
    {
        const mw1 = new TestMiddleware('FirstMiddleware');
        const mw2 = new TestMiddleware('SecondMiddleware');

        service.useMiddleware(mw1);
        service.useMiddleware(mw2);

        context.registerOperation('foo', async() =>
        {
            request = service.outstandingRequests.get(request.id);
            request.succeed({});
        });

        // Call operation
        service.queueEngine.backend.emit('incomingRequest', request.renderRequest());
        await setTimeout(10);

        // Tests
        expect(mw1.successCalled).to.equal(1, 'FirstMiddleware');
        expect(mw2.successCalled).to.equal(1, 'SecondMiddleware');
    });

    it('calls the `failure` function on all registered middleware if the operation fails the request', async() =>
    {
        const mw1 = new TestMiddleware('FirstMiddleware');
        const mw2 = new TestMiddleware('SecondMiddleware');

        service.useMiddleware(mw1);
        service.useMiddleware(mw2);

        context.registerOperation('foo', async() =>
        {
            request = service.outstandingRequests.get(request.id);
            request.failure({});
        });

        // Call operation
        service.queueEngine.backend.emit('incomingRequest', request.renderRequest());
        await setTimeout(10);

        // Tests
        expect(mw1.failureCalled).to.equal(1, 'FirstMiddleware');
        expect(mw2.failureCalled).to.equal(1, 'SecondMiddleware');
    });

    it('calls `beforeRequest in the following order: service -> context -> operation', async() =>
    {
        let mw1TS;
        let mw2TS;
        let mw3TS;

        const mw1 = new TestMiddleware('FirstMiddleware', {
            beforeFn()
            {
                mw1TS = process.hrtime.bigint();
            },
        });
        const mw2 = new TestMiddleware('SecondMiddleware', {
            beforeFn()
            {
                mw2TS = process.hrtime.bigint();
            },
        });
        const mw3 = new TestMiddleware('ThirdMiddleware', {
            beforeFn()
            {
                mw3TS = process.hrtime.bigint();
            },
        });

        service.useMiddleware(mw1);
        context.useMiddleware(mw2);

        context.registerOperation('foo', async() =>
        {
            return {};
        }, [ 'payload' ], [ mw3 ]);

        // Call operation
        service.queueEngine.backend.emit('incomingRequest', request.renderRequest());
        await setTimeout(10);

        // Tests
        expect(Number(mw1TS)).to.be.lessThan(Number(mw2TS));
        expect(Number(mw2TS)).to.be.lessThan(Number(mw3TS));
    });

    it('calls `success` or `failure` in the following order: operation -> context -> service', async() =>
    {
        let mw1TS;
        let mw2TS;
        let mw3TS;

        const mw1 = new TestMiddleware('FirstMiddleware', {
            successFn()
            {
                mw1TS = process.hrtime.bigint();
            },
        });
        const mw2 = new TestMiddleware('SecondMiddleware', {
            successFn()
            {
                mw2TS = process.hrtime.bigint();
            },
        });
        const mw3 = new TestMiddleware('ThirdMiddleware', {
            beforeFn()
            {
                mw3TS = process.hrtime.bigint();
            },
        });

        service.useMiddleware(mw1);
        context.useMiddleware(mw2);

        context.registerOperation('foo', async() =>
        {
            return {};
        }, [ 'payload' ], [ mw3 ]);

        // Call operation
        service.queueEngine.backend.emit('incomingRequest', request.renderRequest());
        await setTimeout(10);

        // Tests
        expect(Number(mw3TS)).to.be.lessThan(Number(mw2TS));
        expect(Number(mw2TS)).to.be.lessThan(Number(mw1TS));
    });

    it('calls middleware in the order they were registered', async() =>
    {
        let mw1TS;
        let mw2TS;

        const mw1 = new TestMiddleware('FirstMiddleware', {
            successFn()
            {
                mw1TS = process.hrtime.bigint();
            },
        });
        const mw2 = new TestMiddleware('SecondMiddleware', {
            successFn()
            {
                mw2TS = process.hrtime.bigint();
            },
        });

        service.useMiddleware([ mw2, mw1 ]);

        context.registerOperation('foo', async() =>
        {
            return {};
        }, [ 'payload' ]);

        // Call operation
        service.queueEngine.backend.emit('incomingRequest', request.renderRequest());
        await setTimeout(10);

        // Tests
        expect(Number(mw2TS)).to.be.lessThan(Number(mw1TS));
    });
});

//----------------------------------------------------------------------------------------------------------------------
