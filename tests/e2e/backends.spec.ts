//----------------------------------------------------------------------------------------------------------------------
// Backend End-to-End Tests
//----------------------------------------------------------------------------------------------------------------------

import { expect } from 'chai';

// Backends
import { TestBackend } from '../utils/backend.js';
import { StrataClient, StrataService } from '../../src/index.js';
import { beforeEach } from 'mocha';
import { NullBackend } from '../../src/resource-access/backends/null.js';
import backendMan from '../../src/managers/backend.js';

//----------------------------------------------------------------------------------------------------------------------

describe('Backend Functionality', () =>
{
    let service : StrataService;

    beforeEach(() =>
    {
        service = new StrataService({
            service: {
                serviceGroup: 'test',
            },
            backend: {
                type: 'test',
            },
        });

        backendMan.clear();
        backendMan.registerInternalBackends();
    });

    it('supports external backends', async() =>
    {
        // Register the backend
        service.registerBackend('test', TestBackend);
        expect(service.backends).to.include('test');

        // Get an instance of the backend
        const backend = service.getBackend('test');
        expect(backend).to.be.an.instanceOf(TestBackend);

        // Start the service
        await service.start();
        expect(service.queueEngine.backend).to.be.instanceOf(TestBackend);
        expect(service.queueEngine.backend).to.have.property('initialized', true);
    });

    it('supports internal backends', async() =>
    {
        service = new StrataService({
            service: {
                serviceGroup: 'test',
            },
            backend: {
                type: 'null',
            },
        });

        // Get an instance of the backend
        const backend = service.getBackend('null');
        expect(backend).to.be.an.instanceOf(NullBackend);

        // Start the service
        await service.start();
        expect(service.queueEngine.backend).to.be.instanceOf(NullBackend);
        expect(service.queueEngine.backend).to.have.property('initialized', true);
    });

    it('supports service and client having different backends', async() =>
    {
        // Register the backend
        service.registerBackend('test', TestBackend);
        expect(service.backends).to.include('test');

        // Start the service
        await service.start();

        const client = new StrataClient({
            service: {
                serviceGroup: 'test',
            },
            backend: {
                type: 'null',
            },
        });

        // Start the client
        await client.start();

        expect(service.queueEngine.backend).to.be.instanceOf(TestBackend);
        expect(service.queueEngine.backend).to.have.property('initialized', true);

        expect(client.queueEngine.backend).to.be.instanceOf(NullBackend);
        expect(client.queueEngine.backend).to.have.property('initialized', true);
    });

    it('supports multiple backend instances of the same backend', async() =>
    {
        // Register the backend
        service.registerBackend('test', TestBackend);
        expect(service.backends).to.include('test');

        // Start the service
        await service.start();

        const client = new StrataClient({
            service: {
                serviceGroup: 'test',
            },
            backend: {
                type: 'test',
            },
        });

        const client2 = new StrataClient({
            service: {
                serviceGroup: 'test',
            },
            backend: {
                type: 'test',
            },
        });

        // Start the clients
        await client.start();
        await client2.start();

        expect(service.queueEngine.backend).to.be.instanceOf(TestBackend);
        expect(service.queueEngine.backend).to.have.property('initialized', true);

        expect(client.queueEngine.backend).to.be.instanceOf(TestBackend);
        expect(client.queueEngine.backend).to.have.property('initialized', true);

        expect(service.queueEngine.backend).to.not.equal(client.queueEngine.backend);
        expect(client.queueEngine.backend).to.not.equal(client2.queueEngine.backend);
    });
});

//----------------------------------------------------------------------------------------------------------------------
