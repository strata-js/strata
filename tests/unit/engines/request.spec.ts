// ---------------------------------------------------------------------------------------------------------------------
// Request Engine Tests
// ---------------------------------------------------------------------------------------------------------------------

import { expect } from 'chai';

// Engines
import { RequestEngine } from '../../../src/engines/request.js';

// ---------------------------------------------------------------------------------------------------------------------

describe('Request Engine', () =>
{
    let requestEngine : RequestEngine;

    beforeEach(() =>
    {
        requestEngine = new RequestEngine();
    });

    describe('callBeforeMiddleware()', () =>
    {
        it('calls before middleware', async() =>
        {
            const middleware = {
                beforeRequest: (request) =>
                {
                    request.called = true;

                    return request;
                },
            };

            const service = {
                middleware: [ middleware ],
            };

            const request = {
                status: 'pending',
            };

            const result = await requestEngine.callBeforeMiddleware(service as any, request as any);

            expect(result).to.have.property('called', true);
        });

        it('stops calling before middleware when request is no longer pending', async() =>
        {
            const middleware = {
                beforeRequest: (request) =>
                {
                    request.called = true;

                    return request;
                },
            };

            const service = {
                middleware: [ middleware ],
            };

            const request = {
                status: 'success',
            };

            const result = await requestEngine.callBeforeMiddleware(service as any, request as any);

            expect(result).to.not.have.property('called');
            expect(result).to.have.property('status', 'success');
        });

        it('does not call before middleware if it does not exist', async() =>
        {
            const middleware = {};

            const service = {
                middleware: [ middleware ],
            };

            const request = {
                status: 'pending',
            };

            const result = await requestEngine.callBeforeMiddleware(service as any, request as any);

            expect(result).to.not.have.property('called');
        });

        it('does not call before middleware if it is not a function', async() =>
        {
            const middleware = {
                beforeRequest: 'test',
            };

            const service = {
                middleware: [ middleware ],
            };

            const request = {
                status: 'pending',
            };

            const result = await requestEngine.callBeforeMiddleware(service as any, request as any);

            expect(result).to.not.have.property('called');
        });
    });

    describe('callSuccessMiddleware()', () =>
    {
        it('calls success middleware', async() =>
        {
            const middleware = {
                success: (request) =>
                {
                    request.called = true;

                    return request;
                },
            };

            const service = {
                middleware: [ middleware ],
            };

            const request = {
                status: 'pending',
            };

            const result = await requestEngine.callSuccessMiddleware(service as any, request as any);

            expect(result).to.have.property('called', true);
        });

        it('does not call success middleware if it does not exist', async() =>
        {
            const middleware = {};

            const service = {
                middleware: [ middleware ],
            };

            const request = {
                status: 'pending',
            };

            const result = await requestEngine.callSuccessMiddleware(service as any, request as any);

            expect(result).to.not.have.property('called');
        });

        it('does not call success middleware if it is not a function', async() =>
        {
            const middleware = {
                success: 'test',
            };

            const service = {
                middleware: [ middleware ],
            };

            const request = {
                status: 'pending',
            };

            const result = await requestEngine.callSuccessMiddleware(service as any, request as any);

            expect(result).to.not.have.property('called');
        });

        it('calls success middleware in order', async() =>
        {
            const middleware1 = {
                success: (request) =>
                {
                    request.called = 1;

                    return request;
                },
            };

            const middleware2 = {
                success: (request) =>
                {
                    request.called = 2;

                    return request;
                },
            };

            const service = {
                middleware: [ middleware1, middleware2 ],
            };

            const request = {
                status: 'pending',
            };

            const result = await requestEngine.callSuccessMiddleware(service as any, request as any);

            expect(result).to.have.property('called', 2);
        });
    });

    describe('callFailureMiddleware()', () =>
    {
        it('calls failure middleware', async() =>
        {
            const middleware = {
                failure: (request) =>
                {
                    request.called = true;

                    return request;
                },
            };

            const service = {
                middleware: [ middleware ],
            };

            const request = {
                status: 'pending',
            };

            const result = await requestEngine.callFailureMiddleware(service as any, request as any);

            expect(result).to.have.property('called', true);
        });

        it('does not call failure middleware if it does not exist', async() =>
        {
            const middleware = {};

            const service = {
                middleware: [ middleware ],
            };

            const request = {
                status: 'pending',
            };

            const result = await requestEngine.callFailureMiddleware(service as any, request as any);

            expect(result).to.not.have.property('called');
        });

        it('does not call failure middleware if it is not a function', async() =>
        {
            const middleware = {
                failure: 'test',
            };

            const service = {
                middleware: [ middleware ],
            };

            const request = {
                status: 'pending',
            };

            const result = await requestEngine.callFailureMiddleware(service as any, request as any);

            expect(result).to.not.have.property('called');
        });

        it('calls failure middleware in order', async() =>
        {
            const middleware1 = {
                failure: (request) =>
                {
                    request.called = 1;

                    return request;
                },
            };

            const middleware2 = {
                failure: (request) =>
                {
                    request.called = 2;

                    return request;
                },
            };

            const service = {
                middleware: [ middleware1, middleware2 ],
            };

            const request = {
                status: 'pending',
            };

            const result = await requestEngine.callFailureMiddleware(service as any, request as any);

            expect(result).to.have.property('called', 2);
        });
    });
});

// ---------------------------------------------------------------------------------------------------------------------
