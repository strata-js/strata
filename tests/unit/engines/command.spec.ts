// ---------------------------------------------------------------------------------------------------------------------
// Command Engine Tests
// ---------------------------------------------------------------------------------------------------------------------

import { hostname } from 'os';
import { expect } from 'chai';

// Engines
import { CommandEngine } from '../../../src/engines/command.js';

// Utils
import shutdownUtil from '../../../src/utils/shutdown.js';

// TestUtils
import { mockMember, unmockMember } from '../../utils/mocks/function.js';
import { MockStrataService } from '../../utils/mocks/service.js';

// ---------------------------------------------------------------------------------------------------------------------

describe('Command Engine', () =>
{
    let service : MockStrataService;
    let commandEngine : CommandEngine;

    beforeEach(() =>
    {
        service = new MockStrataService({
            service: {
                serviceGroup: 'test',
            },
            backend: {
                type: 'null',
            },
        });

        commandEngine = new CommandEngine();
    });

    describe('handleCommand()', () =>
    {
        it('rejects unknown commands', async() =>
        {
            const response = await commandEngine.handleCommand(service as any, {
                id: 'test',
                // @ts-expect-error - Testing invalid command
                command: 'unknown',
            });

            expect(response).to.be.undefined;
        });

        it('handles the info command', async() =>
        {
            const response = await commandEngine.handleCommand(service as any, {
                id: '123456789',
                command: 'info',
                responseChannel: 'test-1234',
            });

            expect(response).to.not.be.undefined;
            expect(response).to.have.property('id', '123456789');
            expect(response).to.have.property('payload');

            const payload = response?.payload;
            expect(payload).to.have.property('id', service.id);
            expect(payload).to.have.property('name', service.name);
            expect(payload).to.have.property('version');
            expect(payload).to.have.property('strataVersion', service.version);
            expect(payload).to.have.property('concurrency', service.concurrency);
            expect(payload).to.have.property('outstanding', service.outstandingRequests.size);
            expect(payload).to.have.property('hostname', hostname());
            expect(payload).to.have.property('lastBusy', service.lastBusy);
            expect(payload).to.have.deep.property('contexts', service.contexts);
        });

        it('handles the concurrency command', async() =>
        {
            const response = await commandEngine.handleCommand(service as any, {
                id: '123456789',
                command: 'concurrency',
                payload: { concurrency: 5 },
            });

            expect(response).to.be.undefined;
            expect(service.concurrency).to.equal(5);
        });

        it('handles the shutdown command if graceful is false', async() =>
        {
            let exitCalled = false;
            let exitCode;

            // Mock process.exit
            mockMember(process, 'exit', (code) => { exitCalled = true; exitCode = code; });

            const response = await commandEngine.handleCommand(service as any, {
                id: '123456789',
                command: 'shutdown',
                payload: { graceful: false, exitCode: 1 },
            });

            expect(response).to.be.undefined;
            expect(exitCalled).to.be.true;
            expect(exitCode).to.equal(1);

            // Unmock process.exit
            unmockMember(process, 'exit');
        });

        it('handles the shutdown command if graceful is true', async() =>
        {
            let gracefulShutdownCalled = false;
            let gracefulShutdownExitCode;

            // Mock gracefulShutdown
            mockMember(shutdownUtil, 'gracefulShutdown', (exitCode) =>
            {
                gracefulShutdownCalled = true;
                gracefulShutdownExitCode = exitCode;
            });

            const response = await commandEngine.handleCommand(service as any, {
                id: '123456789',
                command: 'shutdown',
                payload: { graceful: true, exitCode: 1 },
            });

            expect(response).to.be.undefined;
            expect(gracefulShutdownCalled).to.be.true;
            expect(gracefulShutdownExitCode).to.equal(1);

            // Unmock gracefulShutdown
            unmockMember(shutdownUtil, 'gracefulShutdown');
        });

        it('handles the toobusy command', async() =>
        {
            const response = await commandEngine.handleCommand(service as any, {
                id: '123456789',
                command: 'toobusy',
                payload: { maxLag: 112, interval: 121, smoothingFactorOnRise: 0.18, smoothingFactorOnFall: 0.19 },
            });

            expect(response).to.be.undefined;

            const toobusy = service.config.service?.toobusy ?? {};

            expect(toobusy.maxLag).to.equal(112);
            expect(toobusy.interval).to.equal(121);
            expect(toobusy.smoothingFactorOnRise).to.equal(0.18);
            expect(toobusy.smoothingFactorOnFall).to.equal(0.19);
        });
    });
});

// ---------------------------------------------------------------------------------------------------------------------
