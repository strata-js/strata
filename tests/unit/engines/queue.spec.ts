// ---------------------------------------------------------------------------------------------------------------------
// Queue Engine Tests
// ---------------------------------------------------------------------------------------------------------------------

import { use as chaiUse, expect } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import { setTimeout } from 'timers/promises';

// Interfaces
import { PostEnvelope, RequestEnvelope } from '../../../src/interfaces/messages.js';

// Managers
import backendMan from '../../../src/managers/backend.js';

// Engines
import { ClientQueueEngine, ServiceQueueEngine } from '../../../src/engines/queue.js';

// Backends
import { NullBackend } from '../../../src/resource-access/backends/null.js';

// TestUtil
import { MockStrataRequest } from '../../utils/mocks/request.js';
import { MockStrataService } from '../../utils/mocks/service.js';
import { MockStrataContext } from '../../utils/mocks/context.js';
import { MockStrataClient } from '../../utils/mocks/client.js';
import { RequestTimeoutError } from '../../../src/lib/errors.js';

// ---------------------------------------------------------------------------------------------------------------------

chaiUse(chaiAsPromised);

// ---------------------------------------------------------------------------------------------------------------------

describe('Queue Engine', () =>
{
    describe('Service Queue Engine', () =>
    {
        let service : MockStrataService;
        let context : MockStrataContext;
        let queueEngine : ServiceQueueEngine;

        beforeEach(() =>
        {
            service = new MockStrataService({
                backend: { type: 'null' },
                service: { serviceGroup: 'test' },
            });

            // Build a mock context
            context = new MockStrataContext();
            context.registerTestOperation('test', async() => ({ test: true }));

            // Add the mock context to the service
            service.registerContext('test', context);

            // Since the MockStrataService instantiates a new queue engine, no reason not to just grab it here. This
            // ensures that all the properties on the service and queue engine line up.
            queueEngine = service.queueEngine;

            // Clear the backend instances in between runs
            backendMan.clear();
            backendMan.registerInternalBackends();
        });

        it('can be instantiated', () =>
        {
            expect(queueEngine).to.be.instanceOf(ServiceQueueEngine);
        });

        it('creates a backend instance based on service configuration', async() =>
        {
            expect(queueEngine.backend).to.not.be.undefined;
            expect(queueEngine.backend).to.be.instanceOf(NullBackend);
        });

        it('throws and error if an unknown backend is configured', async() =>
        {
            service.config.backend.type = 'unknown';

            expect(queueEngine.init()).to.rejectedWith(`Missing or unrecognized backend 'unknown'.`);
        });

        it('exposes the backend instance', async() =>
        {
            expect(queueEngine.backend).to.not.be.undefined;
        });

        it('exposes the outstanding requests', async() =>
        {
            expect(queueEngine.outstandingRequests).to.not.be.undefined;
            expect(queueEngine.outstandingRequests).to.be.instanceOf(Map);
        });

        it('initializes the backend', async() =>
        {
            await queueEngine.init();

            expect(queueEngine.backend).to.not.be.undefined;
            expect(queueEngine.backend).to.be.instanceOf(NullBackend);
            expect((queueEngine.backend as NullBackend).initialized).to.be.true;
        });

        describe('respond()', () =>
        {
            it('calls the backend to send a response', async() =>
            {
                const mockRequest = new MockStrataRequest(service as any);
                mockRequest.messageType = 'request';
                mockRequest.responseQueue = 'test';

                await queueEngine.init();
                await queueEngine.respond(mockRequest as any);

                expect(queueEngine.backend).to.not.be.undefined;

                const backend = queueEngine.backend as NullBackend;

                expect(backend).to.be.instanceOf(NullBackend);
                expect(backend.response).to.not.be.undefined;
                expect(backend.response).to.deep.equal(mockRequest.renderResponse());
            });

            it('fails the request if the request is still pending', async() =>
            {
                const mockRequest = new MockStrataRequest(service as any);
                mockRequest.status = 'pending';
                mockRequest.messageType = 'request';
                mockRequest.responseQueue = 'test';

                await queueEngine.init();
                await queueEngine.respond(mockRequest as any);

                expect(queueEngine.backend).to.not.be.undefined;

                const backend = queueEngine.backend as NullBackend;

                expect(backend).to.be.instanceOf(NullBackend);
                expect(backend.response).to.not.be.undefined;
                expect(backend.response).to.deep.equal(mockRequest.renderResponse());
                expect(backend.response?.status).to.equal('failed');
            });

            it('does not respond if the message type is not a request', async() =>
            {
                const mockRequest = new MockStrataRequest(service as any);
                mockRequest.status = 'succeeded';
                mockRequest.messageType = 'post';
                mockRequest.responseQueue = 'test';

                await queueEngine.init();
                await queueEngine.respond(mockRequest as any);

                expect(queueEngine.backend).to.not.be.undefined;

                const backend = queueEngine.backend as NullBackend;

                expect(backend).to.be.instanceOf(NullBackend);
                expect(backend.response).to.be.undefined;
            });

            it('does not respond if there is no response queue specified', async() =>
            {
                const mockRequest = new MockStrataRequest(service as any);
                mockRequest.status = 'succeeded';
                mockRequest.messageType = 'request';
                mockRequest.responseQueue = undefined;

                await queueEngine.init();
                await queueEngine.respond(mockRequest as any);

                expect(queueEngine.backend).to.not.be.undefined;

                const backend = queueEngine.backend as NullBackend;

                expect(backend).to.be.instanceOf(NullBackend);
                expect(backend.response).to.be.undefined;
            });
        });

        describe('Listening', () =>
        {
            it('calls the backend to listen for commands', async() =>
            {
                await queueEngine.init();
                await queueEngine.listenForCommands('test', service.id);

                expect(queueEngine.backend).to.not.be.undefined;
                expect(queueEngine.backend).to.be.instanceOf(NullBackend);
                expect((queueEngine.backend as NullBackend).listeningForCommands).to.be.true;
            });

            it('calls the backend to listen for requests', async() =>
            {
                await queueEngine.init();
                await queueEngine.listenForRequests('test', service.id, async() => true);

                expect(queueEngine.backend).to.not.be.undefined;
                expect(queueEngine.backend).to.be.instanceOf(NullBackend);
                expect((queueEngine.backend as NullBackend).listeningForRequests).to.be.true;
            });

            it('calls the backend to stop listening for commands', async() =>
            {
                await queueEngine.init();
                await queueEngine.listenForCommands('test', service.id);
                expect(queueEngine.backend).to.not.be.undefined;
                expect(queueEngine.backend).to.be.instanceOf(NullBackend);
                expect((queueEngine.backend as NullBackend).listeningForCommands).to.be.true;

                // Stop listening for commands
                await queueEngine.stopListeningForCommands();
                expect((queueEngine.backend as NullBackend).listeningForCommands).to.be.false;
            });

            it('calls the backend to stop listening for requests', async() =>
            {
                await queueEngine.init();
                await queueEngine.listenForRequests('test', service.id, async() => true);
                expect(queueEngine.backend).to.not.be.undefined;
                expect(queueEngine.backend).to.be.instanceOf(NullBackend);
                expect((queueEngine.backend as NullBackend).listeningForRequests).to.be.true;

                // Stop listening for requests
                await queueEngine.stopListeningForRequests('test');
                expect((queueEngine.backend as NullBackend).listeningForRequests).to.be.false;
            });
        });

        describe('Discovery', () =>
        {
            it('calls the backend to enable discovery', async() =>
            {
                await queueEngine.init();
                await queueEngine.enableDiscovery();

                expect(queueEngine.backend).to.not.be.undefined;
                expect(queueEngine.backend).to.be.instanceOf(NullBackend);
                expect((queueEngine.backend as NullBackend).discoveryEnabled).to.be.true;
            });

            it('calls the backend to disable discovery', async() =>
            {
                await queueEngine.init();
                await queueEngine.enableDiscovery();
                expect(queueEngine.backend).to.not.be.undefined;
                expect(queueEngine.backend).to.be.instanceOf(NullBackend);
                expect((queueEngine.backend as NullBackend).discoveryEnabled).to.be.true;

                // Stop listening for commands
                await queueEngine.disableDiscovery();
                expect((queueEngine.backend as NullBackend).discoveryEnabled).to.be.false;
            });
        });

        describe('Teardown', () =>
        {
            it('calls the backend to teardown', async() =>
            {
                await queueEngine.init();
                await queueEngine.teardown();

                expect(queueEngine.backend).to.not.be.undefined;
                expect(queueEngine.backend).to.be.instanceOf(NullBackend);
                expect((queueEngine.backend as NullBackend).teardownCalled).to.be.true;
            });
        });

        describe('Command Handling', () =>
        {
            it('handles incoming commands', async() =>
            {
                const commandEnvelope = {
                    id: '123456789',
                    command: 'concurrency' as const,
                    payload: {
                        concurrency: 1,
                    },
                };

                await queueEngine.init();

                expect(service.concurrency).to.equal(16);

                // Trigger the request handler by emitting the backend's request event
                queueEngine.backend?.emit('incomingCommand', commandEnvelope);

                expect(service.concurrency).to.equal(1);
            });

            it('handles incoming commands with a response', async() =>
            {
                const commandEnvelope = {
                    id: '123456789',
                    command: 'info' as const,
                    payload: undefined,
                    responseChannel: 'test',
                };

                await queueEngine.init();

                // Trigger the request handler by emitting the backend's request event
                queueEngine.backend?.emit('incomingCommand', commandEnvelope);

                const backend = queueEngine.backend as NullBackend;

                // Wait 10ms for the events to fire off
                await setTimeout(10);

                expect(backend.commandResponse).to.not.be.undefined;
                expect(backend.commandResponse).to.deep.equal({
                    id: commandEnvelope.id,
                    payload: service.info,
                });
            });
        });

        describe('Request Handling', () =>
        {
            it('adds a request to the outstanding requests list', async() =>
            {
                const requestEnvelope : RequestEnvelope = {
                    id: '123456789',
                    context: 'test',
                    operation: 'test',
                    metadata: {
                        unitTest: true,
                    },
                    payload: {
                        test: true,
                    },
                    responseQueue: 'test-1234',
                    messageType: 'request',
                    timestamp: (new Date()).toISOString(),
                    client: 'test',
                    requestChain: [],
                };

                await queueEngine.init();

                // Trigger the request handler by emitting the backend's request event
                queueEngine.backend?.emit('incomingRequest', requestEnvelope);

                expect(queueEngine.outstandingRequests).to.not.be.undefined;
                expect(queueEngine.outstandingRequests.size).to.equal(1);

                const request = queueEngine.outstandingRequests.get(requestEnvelope.id);
                expect(request).to.not.be.undefined;
                expect(request).to.have.a.property('id', requestEnvelope.id);
                expect(request).to.have.a.property('context', requestEnvelope.context);
                expect(request).to.have.a.property('operation', requestEnvelope.operation);
                expect(request).to.have.a.property('metadata');
                expect(request?.metadata).to.deep.equal(requestEnvelope.metadata);
                expect(request).to.have.a.property('payload');
                expect(request?.payload).to.deep.equal(requestEnvelope.payload);
                expect(request).to.have.a.property('responseQueue', requestEnvelope.responseQueue);
                expect(request).to.have.a.property('messageType', requestEnvelope.messageType);
                expect(request).to.have.a.property('timestamp', requestEnvelope.timestamp);
                expect(request).to.have.a.property('client', requestEnvelope.client);
                expect(request).to.have.a.property('requestChain');
                expect(request?.requestChain).to.deep.equal([ requestEnvelope.id ]);
            });

            it('removes a request from the outstanding requests list when it is responded to', async() =>
            {
                const requestEnvelope : RequestEnvelope = {
                    id: '123456789',
                    context: 'test',
                    operation: 'test',
                    metadata: {
                        unitTest: true,
                    },
                    payload: {
                        test: true,
                    },
                    responseQueue: 'test-1234',
                    messageType: 'request',
                    timestamp: (new Date()).toISOString(),
                    client: 'test',
                    requestChain: [],
                };

                await queueEngine.init();

                // Trigger the request handler by emitting the backend's request event
                queueEngine.backend?.emit('incomingRequest', requestEnvelope);

                await setTimeout(10);

                expect(queueEngine.outstandingRequests).to.not.be.undefined;
                expect(queueEngine.outstandingRequests.size).to.equal(1);

                expect(queueEngine.outstandingRequests.get(requestEnvelope.id)).to.not.be.undefined;
                expect(queueEngine.outstandingRequests.get(requestEnvelope.id)?.status).to.equal('pending');

                // Let the deferred operation continue.
                const deferred = context.testOperations['test'];
                deferred.resolve(undefined);

                // We have to wait a bit. The request handler is async, and we need to wait for it to finish.
                await setTimeout(10);

                expect(queueEngine.outstandingRequests.size).to.equal(0);
                expect(queueEngine.outstandingRequests.get(requestEnvelope.id)).to.be.undefined;
            });

            it('removes a request from the outstanding requests list when it is failed', async() =>
            {
                const requestEnvelope : RequestEnvelope = {
                    id: '123456789',
                    context: 'test',
                    operation: 'test',
                    metadata: {
                        unitTest: true,
                    },
                    payload: {
                        test: true,
                    },
                    responseQueue: 'test-1234',
                    messageType: 'request',
                    timestamp: (new Date()).toISOString(),
                    client: 'test',
                    requestChain: [],
                };

                await queueEngine.init();

                // Trigger the request handler by emitting the backend's request event
                queueEngine.backend?.emit('incomingRequest', requestEnvelope);

                await setTimeout(10);

                expect(queueEngine.outstandingRequests).to.not.be.undefined;
                expect(queueEngine.outstandingRequests.size).to.equal(1);

                const request = queueEngine.outstandingRequests.get(requestEnvelope.id);

                expect(request).to.not.be.undefined;
                expect(request?.status).to.equal('pending');

                // Let the deferred operation continue.
                const deferred = context.testOperations['test'];
                deferred.reject(new Error('Test Error'));

                // We have to wait a bit. The request handler is async, and we need to wait for it to finish.
                await setTimeout(10);

                expect(request?.status).to.equal('failed');

                expect(queueEngine.outstandingRequests.size).to.equal(0);
                expect(queueEngine.outstandingRequests.get(requestEnvelope.id)).to.be.undefined;
            });
        });
    });

    describe('Client Queue Engine', () =>
    {
        let client : MockStrataClient;
        let queueEngine : ClientQueueEngine;

        beforeEach(() =>
        {
            client = new MockStrataClient({
                backend: { type: 'null' },
                service: { serviceGroup: 'test' },
            });

            // Since the MockStrataClient instantiates a new queue engine, no reason not to just grab it here. This
            // ensures that all the properties on the service and queue engine line up.
            queueEngine = client.queueEngine;
        });

        it('can be instantiated', () =>
        {
            expect(queueEngine).to.be.instanceOf(ClientQueueEngine);
        });

        it('creates a backend instance based on service configuration', async() =>
        {
            expect(queueEngine.backend).to.not.be.undefined;
            expect(queueEngine.backend).to.be.instanceOf(NullBackend);
        });

        it('throws and error if an unknown backend is configured', async() =>
        {
            client.config.backend.type = 'unknown';

            expect(queueEngine.init()).to.rejectedWith(`Missing or unrecognized backend 'unknown'.`);
        });

        it('exposes the backend instance', async() =>
        {
            expect(queueEngine.backend).to.not.be.undefined;
        });

        it('exposes the outstanding requests', async() =>
        {
            expect(queueEngine.outstandingRequests).to.not.be.undefined;
            expect(queueEngine.outstandingRequests).to.be.instanceOf(Map);
        });

        it('initializes the backend', async() =>
        {
            await queueEngine.init();

            expect(queueEngine.backend).to.not.be.undefined;
            expect(queueEngine.backend).to.be.instanceOf(NullBackend);
            expect((queueEngine.backend as NullBackend).initialized).to.be.true;
        });

        describe('command()', () =>
        {
            it('calls the backend to send a command', async() =>
            {
                const commandEnvelope = {
                    id: '123456789',
                    command: 'concurrency' as const,
                    payload: {
                        concurrency: 1,
                    },
                };

                await queueEngine.init();
                await queueEngine.command(commandEnvelope.command, commandEnvelope);

                expect(queueEngine.backend).to.not.be.undefined;

                const backend = queueEngine.backend as NullBackend;

                expect(backend).to.be.instanceOf(NullBackend);
                expect(backend.command).to.not.be.undefined;
                expect(backend.command).to.deep.equal(commandEnvelope);
            });

            it('adds a command to the outstanding requests list if it requires a response', async() =>
            {
                const commandEnvelope = {
                    id: '123456789',
                    command: 'info' as const,
                    payload: undefined,
                    responseChannel: 'test',
                };

                await queueEngine.init();

                // Don't await, we won't be responding to this one.
                queueEngine.command(commandEnvelope.command, commandEnvelope);

                await setTimeout(10);

                expect(queueEngine.backend).to.not.be.undefined;
                expect(queueEngine.outstandingCommands.size).to.equal(1);

                const backend = queueEngine.backend as NullBackend;

                expect(backend).to.be.instanceOf(NullBackend);
                expect(backend.command).to.not.be.undefined;
                expect(backend.command).to.deep.equal(commandEnvelope);
            });

            it('does not add a command to the outstanding requests list if it does not require a response', async() =>
            {
                const commandEnvelope = {
                    id: '123456789',
                    command: 'concurrency' as const,
                    payload: {
                        concurrency: 1,
                    },
                };

                await queueEngine.init();

                // Don't await, instead store this so that we can await it later.
                const commandPromise = queueEngine.command(commandEnvelope.command, commandEnvelope);

                await setTimeout(10);

                expect(queueEngine.backend).to.not.be.undefined;
                expect(queueEngine.outstandingCommands.size).to.equal(0);

                const backend = queueEngine.backend as NullBackend;

                expect(backend).to.be.instanceOf(NullBackend);
                expect(backend.command).to.not.be.undefined;
                expect(backend.command).to.deep.equal(commandEnvelope);

                return commandPromise;
            });

            it('removes a command from the outstanding requests list when it is responded to', async() =>
            {
                const commandEnvelope = {
                    id: '123456789',
                    command: 'info' as const,
                    payload: undefined,
                    responseChannel: 'test',
                };

                await queueEngine.init();

                // Don't await, instead store this so that we can await it later.
                const commandPromise = queueEngine.command(commandEnvelope.command, commandEnvelope);

                await setTimeout(10);

                expect(queueEngine.backend).to.not.be.undefined;
                expect(queueEngine.outstandingCommands.size).to.equal(1);

                const backend = queueEngine.backend as NullBackend;

                expect(backend).to.be.instanceOf(NullBackend);
                expect(backend.command).to.not.be.undefined;
                expect(backend.command).to.deep.equal(commandEnvelope);

                // Have the backend emit the response
                backend.emit('incomingCommandResponse', {
                    id: commandEnvelope.id,
                    payload: { test: true },
                });

                const result = await commandPromise;

                expect(queueEngine.outstandingCommands.size).to.equal(0);
                expect(result).to.have.property('id', commandEnvelope.id);
                expect(result).to.have.property('payload');
                expect(result?.payload).to.deep.equal({ test: true });
            });
        });

        describe('post()', () =>
        {
            it('calls the backend to send a post', async() =>
            {
                const serviceGroup = 'TestService';
                const context = 'test';
                const operation = 'test';
                const payload = { test: true };
                const metadata = { unitTest: true };
                const auth = 'test';
                const clientString = `${ client.name }:${ client.id } v${ client.version }`;

                await queueEngine.init();
                await queueEngine.post(serviceGroup, context, operation, payload, metadata, auth, clientString);

                expect(queueEngine.backend).to.not.be.undefined;

                const backend = queueEngine.backend as NullBackend;

                expect(backend).to.be.instanceOf(NullBackend);

                const postEnvelope = backend.request as PostEnvelope;
                expect(postEnvelope).to.not.be.undefined;
                expect(postEnvelope).to.have.property('id');
                expect(postEnvelope).to.have.property('context', context);
                expect(postEnvelope).to.have.property('operation', operation);
                expect(postEnvelope).to.have.property('payload');
                expect(postEnvelope?.payload).to.deep.equal(payload);
                expect(postEnvelope).to.have.property('metadata');
                expect(postEnvelope?.metadata).to.deep.equal(metadata);
                expect(postEnvelope).to.have.property('auth', auth);
                expect(postEnvelope).to.have.property('client', clientString);
            });

            it('does not add a post to the outstanding requests list', async() =>
            {
                const serviceGroup = 'TestService';
                const context = 'test';
                const operation = 'test';
                const payload = { test: true };
                const metadata = { unitTest: true };
                const auth = 'test';
                const clientString = `${ client.name }:${ client.id } v${ client.version }`;

                await queueEngine.init();
                await queueEngine.post(serviceGroup, context, operation, payload, metadata, auth, clientString);

                expect(queueEngine.backend).to.not.be.undefined;

                const backend = queueEngine.backend as NullBackend;

                expect(backend).to.be.instanceOf(NullBackend);
                expect(queueEngine.outstandingRequests.size).to.equal(0);
            });
        });

        describe('request()', () =>
        {
            it('calls the backend to send a request', async() =>
            {
                const serviceGroup = 'TestService';
                const context = 'test';
                const operation = 'test';
                const payload = { test: true };
                const metadata = { unitTest: true };
                const auth = 'test';
                const clientString = `${ client.name }:${ client.id } v${ client.version }`;

                await queueEngine.init();
                queueEngine.request(
                    client.id,
                    serviceGroup,
                    context,
                    operation,
                    payload,
                    metadata,
                    auth,
                    undefined,
                    clientString
                );

                expect(queueEngine.backend).to.not.be.undefined;

                const backend = queueEngine.backend as NullBackend;

                expect(backend).to.be.instanceOf(NullBackend);

                const requestEnvelope = backend.request as RequestEnvelope;
                expect(requestEnvelope).to.not.be.undefined;
                expect(requestEnvelope).to.have.property('id');
                expect(requestEnvelope).to.have.property('context', context);
                expect(requestEnvelope).to.have.property('operation', operation);
                expect(requestEnvelope).to.have.property('payload');
                expect(requestEnvelope?.payload).to.deep.equal(payload);
                expect(requestEnvelope).to.have.property('metadata');
                expect(requestEnvelope?.metadata).to.deep.equal(metadata);
                expect(requestEnvelope).to.have.property('auth', auth);
                expect(requestEnvelope).to.have.property('client', clientString);
            });

            it('adds a request to the outstanding requests list', async() =>
            {
                const serviceGroup = 'TestService';
                const context = 'test';
                const operation = 'test';
                const payload = { test: true };
                const metadata = { unitTest: true };
                const auth = 'test';
                const clientString = `${ client.name }:${ client.id } v${ client.version }`;

                await queueEngine.init();
                queueEngine.request(
                    client.id,
                    serviceGroup,
                    context,
                    operation,
                    payload,
                    metadata,
                    auth,
                    undefined,
                    clientString
                );

                await setTimeout(10);

                expect(queueEngine.backend).to.not.be.undefined;
                expect(queueEngine.outstandingRequests.size).to.equal(1);
            });

            it('removes a request from the outstanding requests list when it is responded to', async() =>
            {
                const serviceGroup = 'TestService';
                const context = 'test';
                const operation = 'test';
                const payload = { test: true };
                const metadata = { unitTest: true };
                const auth = 'test';
                const clientString = `${ client.name }:${ client.id } v${ client.version }`;

                await queueEngine.init();
                const requestPromise = queueEngine.request(
                    client.id,
                    serviceGroup,
                    context,
                    operation,
                    payload,
                    metadata,
                    auth,
                    undefined,
                    clientString
                );

                await setTimeout(10);

                expect(queueEngine.backend).to.not.be.undefined;
                expect(queueEngine.outstandingRequests.size).to.equal(1);

                const backend = queueEngine.backend as NullBackend;

                expect(backend).to.be.instanceOf(NullBackend);
                expect(backend.request).to.not.be.undefined;
                expect(backend.request).to.have.property('id');

                // Have the backend emit the response
                backend.emit('incomingResponse', {
                    id: backend.request?.id ?? '123456789',
                    payload: { test: true },
                    messageType: 'response',
                    context,
                    operation: context,
                    timestamp: (new Date()).toISOString(),
                    status: 'succeeded',
                    messages: [],
                    service: 'TestService',
                });

                await setTimeout(10);

                expect(queueEngine.outstandingRequests.size).to.equal(0);

                const result = await requestPromise;
                expect(result).to.have.property('id', backend.request?.id);
                expect(result).to.have.property('payload');
                expect(result?.payload).to.deep.equal({ test: true });
                expect(result).to.have.property('messageType', 'response');
                expect(result).to.have.property('context', context);
                expect(result).to.have.property('operation', context);
                expect(result).to.have.property('timestamp');
                expect(result).to.have.property('status', 'succeeded');
                expect(result).to.have.property('messages');
                expect(result?.messages).to.deep.equal([]);
                expect(result).to.have.property('service', 'TestService');
            });

            it('removes a request from the outstanding requests list when it is failed', async() =>
            {
                const serviceGroup = 'TestService';
                const context = 'test';
                const operation = 'test';
                const payload = { test: true };
                const metadata = { unitTest: true };
                const auth = 'test';
                const clientString = `${ client.name }:${ client.id } v${ client.version }`;

                await queueEngine.init();
                const requestPromise = queueEngine.request(
                    client.id,
                    serviceGroup,
                    context,
                    operation,
                    payload,
                    metadata,
                    auth,
                    undefined,
                    clientString
                );

                expect(requestPromise).to.be.rejectedWith('Request failed.');

                await setTimeout(10);

                expect(queueEngine.backend).to.not.be.undefined;
                expect(queueEngine.outstandingRequests.size).to.equal(1);

                const backend = queueEngine.backend as NullBackend;

                expect(backend).to.be.instanceOf(NullBackend);
                expect(backend.request).to.not.be.undefined;
                expect(backend.request).to.have.property('id');

                // Have the backend emit the response
                backend.emit('incomingResponse', {
                    id: backend.request?.id ?? '123456789',
                    payload: { test: true },
                    messageType: 'response',
                    context,
                    operation: context,
                    timestamp: (new Date()).toISOString(),
                    status: 'failed',
                    messages: [],
                    service: 'TestService',
                });

                await setTimeout(10);

                expect(queueEngine.outstandingRequests.size).to.equal(0);
            });

            it('removes a request from the outstanding requests list when it times out', async() =>
            {
                const serviceGroup = 'TestService';
                const context = 'test';
                const operation = 'test';
                const payload = { test: true };
                const metadata = { unitTest: true };
                const auth = 'test';
                const clientString = `${ client.name }:${ client.id } v${ client.version }`;

                await queueEngine.init();
                const requestPromise = queueEngine.request(
                    client.id,
                    serviceGroup,
                    context,
                    operation,
                    payload,
                    metadata,
                    auth,
                    20,
                    clientString
                );

                expect(requestPromise).to.be.rejectedWith('Request failed.');

                await setTimeout(10);

                // Make sure the request is still in the outstanding requests list
                expect(queueEngine.backend).to.not.be.undefined;
                expect(queueEngine.outstandingRequests.size).to.equal(1);

                const backend = queueEngine.backend as NullBackend;

                // Now, we wait for the timeout to expire
                try
                {
                    await requestPromise;
                    expect.fail('Request should have failed.');
                }
                catch (err)
                {
                    expect(err).to.be.an.instanceOf(RequestTimeoutError);
                    expect(err).to.have.property('message', `Request '${ backend.request?.id }' timed out.`);
                }
            });
        });

        describe('Response Handling', () =>
        {
            it('handles incoming command responses', async() =>
            {
                const commandEnvelope = {
                    id: '123456789',
                    command: 'info' as const,
                    payload: undefined,
                    responseChannel: 'test',
                };

                await queueEngine.init();

                expect(queueEngine.outstandingCommands).to.not.be.undefined;
                expect(queueEngine.outstandingCommands.size).to.equal(0);

                const responsePromise = queueEngine.command(commandEnvelope.command, commandEnvelope);

                const backend = queueEngine.backend as NullBackend;
                commandEnvelope.id = backend.command?.id ?? '';

                await setTimeout(10);

                expect(queueEngine.outstandingCommands.size).to.equal(1);
                expect(queueEngine.outstandingCommands.get(commandEnvelope.id)).to.not.be.undefined;

                // Trigger the request handler by emitting the backend's request event
                queueEngine.backend?.emit('incomingCommandResponse', {
                    id: commandEnvelope.id,
                    payload: { test: true },
                });

                const response = await responsePromise;

                expect(queueEngine.outstandingCommands.size).to.equal(0);

                const command = queueEngine.outstandingCommands.get(commandEnvelope.id);
                expect(command).to.be.undefined;

                expect(response).to.have.property('id', commandEnvelope.id);
                expect(response).to.have.property('payload');
                expect(response?.payload).to.deep.equal({ test: true });
            });

            it('handles incoming responses', async() =>
            {
                const requestEnvelope : RequestEnvelope = {
                    id: '',
                    context: 'test',
                    operation: 'test',
                    metadata: {
                        unitTest: true,
                    },
                    payload: {
                        test: true,
                    },
                    responseQueue: 'test-1234',
                    messageType: 'request',
                    timestamp: (new Date()).toISOString(),
                    client: 'test',
                    requestChain: [],
                };

                await queueEngine.init();

                expect(queueEngine.outstandingRequests).to.not.be.undefined;
                expect(queueEngine.outstandingRequests.size).to.equal(0);

                const responsePromise = queueEngine.request(
                    client.id,
                    'TestService',
                    requestEnvelope.context,
                    requestEnvelope.operation,
                    requestEnvelope.payload,
                    requestEnvelope.metadata,
                    'test',
                    undefined,
                    'test'
                );

                const backend = queueEngine.backend as NullBackend;
                requestEnvelope.id = backend.request?.id ?? '';
                requestEnvelope.timestamp = backend.request?.timestamp ?? '';

                await setTimeout(10);

                expect(queueEngine.outstandingRequests.size).to.equal(1);
                expect(queueEngine.outstandingRequests.get(requestEnvelope.id)).to.not.be.undefined;

                // Trigger the request handler by emitting the backend's request event
                queueEngine.backend?.emit('incomingResponse', {
                    id: requestEnvelope.id,
                    payload: { test: true },
                    messageType: 'response',
                    context: requestEnvelope.context,
                    operation: requestEnvelope.operation,
                    timestamp: requestEnvelope.timestamp,
                    status: 'succeeded',
                    messages: [],
                    service: 'TestService',
                });

                const response = await responsePromise;

                expect(queueEngine.outstandingRequests.size).to.equal(0);

                const request = queueEngine.outstandingRequests.get(requestEnvelope.id);
                expect(request).to.be.undefined;

                expect(response).to.have.property('id', requestEnvelope.id);
                expect(response).to.have.property('payload');
                expect(response?.payload).to.deep.equal({ test: true });
                expect(response).to.have.property('messageType', 'response');
                expect(response).to.have.property('context', requestEnvelope.context);
                expect(response).to.have.property('operation', requestEnvelope.operation);
                expect(response).to.have.property('timestamp', requestEnvelope.timestamp);
                expect(response).to.have.property('status', 'succeeded');
                expect(response).to.have.property('messages');
                expect(response?.messages).to.deep.equal([]);
                expect(response).to.have.property('service', 'TestService');
            });
        });

        describe('Listening', () =>
        {
            it('calls the backend to listen for command responses', async() =>
            {
                await queueEngine.init();
                await queueEngine.listenForCommandResponses(client.id);

                expect(queueEngine.backend).to.not.be.undefined;
                expect(queueEngine.backend).to.be.instanceOf(NullBackend);
                expect((queueEngine.backend as NullBackend).listeningForCommandResponses).to.be.true;
            });

            it('calls the backend to stop listening for command responses', async() =>
            {
                await queueEngine.init();
                await queueEngine.listenForCommandResponses(client.id);
                expect(queueEngine.backend).to.not.be.undefined;
                expect(queueEngine.backend).to.be.instanceOf(NullBackend);
                expect((queueEngine.backend as NullBackend).listeningForCommandResponses).to.be.true;

                // Stop listening for commands
                await queueEngine.stopListeningForCommandResponses();
                expect((queueEngine.backend as NullBackend).listeningForCommandResponses).to.be.false;
            });

            it('calls the backend to listen for responses', async() =>
            {
                await queueEngine.init();
                await queueEngine.listenForResponses(client.id);

                expect(queueEngine.backend).to.not.be.undefined;
                expect(queueEngine.backend).to.be.instanceOf(NullBackend);
                expect((queueEngine.backend as NullBackend).listeningForResponses).to.be.true;
            });

            it('calls the backend to stop listening for responses', async() =>
            {
                await queueEngine.init();
                await queueEngine.listenForResponses(client.id);
                expect(queueEngine.backend).to.not.be.undefined;
                expect(queueEngine.backend).to.be.instanceOf(NullBackend);
                expect((queueEngine.backend as NullBackend).listeningForResponses).to.be.true;

                // Stop listening for responses
                await queueEngine.stopListeningForResponses();
                expect((queueEngine.backend as NullBackend).listeningForResponses).to.be.false;
            });
        });

        describe('Discovery', () =>
        {
            it('calls the backend to get the list of service groups', async() =>
            {
                await queueEngine.init();
                await queueEngine.listServices();

                expect(queueEngine.backend).to.not.be.undefined;
                expect(queueEngine.backend).to.be.instanceOf(NullBackend);
                expect((queueEngine.backend as NullBackend).listServicesCalled).to.be.true;
            });
        });

        describe('Teardown', () =>
        {
            it('calls the backend to teardown', async() =>
            {
                await queueEngine.init();
                await queueEngine.teardown();

                expect(queueEngine.backend).to.not.be.undefined;
                expect(queueEngine.backend).to.be.instanceOf(NullBackend);
                expect((queueEngine.backend as NullBackend).teardownCalled).to.be.true;
            });
        });
    });
});

// ---------------------------------------------------------------------------------------------------------------------
