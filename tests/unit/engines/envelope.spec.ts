// ---------------------------------------------------------------------------------------------------------------------
// Message Format Engine tests
// ---------------------------------------------------------------------------------------------------------------------

import { expect } from 'chai';

// Engines
import envelopeEng from '../../../src/engines/envelope.js';

// ---------------------------------------------------------------------------------------------------------------------

describe('Envelope Engine', () =>
{
    it('builds post envelopes', () =>
    {
        const envelope = envelopeEng.buildPost({
            id: '123456789',
            context: 'test',
            operation: 'test',
            metadata: {
                unitTest: true,
            },
            payload: {
                test: true,
            },
        });

        // Test the envelope structure
        expect(envelope).to.not.be.undefined;
        expect(envelope).to.have.property('id', '123456789');
        expect(envelope).to.have.property('messageType', 'post');
        expect(envelope).to.have.property('context', 'test');
        expect(envelope).to.have.property('operation', 'test');
        expect(envelope).to.have.property('timestamp');
        expect(envelope).to.have.property('auth');
        expect(envelope).to.have.property('client', 'unknown');
        expect(envelope).to.not.have.property('responseQueue');
        expect(envelope).to.not.have.property('timeout');
        expect(envelope).to.have.property('priorRequest', undefined);
        expect(envelope).to.have.property('requestChain');

        expect(envelope).to.have.property('payload');
        expect(envelope.payload).to.have.property('test', true);

        expect(envelope).to.have.property('metadata');
        expect(envelope.metadata).to.have.property('unitTest', true);
    });

    it('builds request envelopes', () =>
    {
        const envelope = envelopeEng.buildRequest({
            id: '123456789',
            context: 'test',
            operation: 'test',
            metadata: {
                unitTest: true,
            },
            payload: {
                test: true,
            },
            responseQueue: 'test-1234',
        });

        // Test the envelope structure
        expect(envelope).to.not.be.undefined;
        expect(envelope).to.have.property('id', '123456789');
        expect(envelope).to.have.property('messageType', 'request');
        expect(envelope).to.have.property('context', 'test');
        expect(envelope).to.have.property('operation', 'test');
        expect(envelope).to.have.property('timestamp');
        expect(envelope).to.have.property('auth');
        expect(envelope).to.have.property('client', 'unknown');
        expect(envelope).to.have.property('responseQueue', 'test-1234');
        expect(envelope).to.have.property('timeout', undefined);
        expect(envelope).to.have.property('priorRequest', undefined);
        expect(envelope).to.have.property('requestChain');

        expect(envelope).to.have.property('payload');
        expect(envelope.payload).to.have.property('test', true);

        expect(envelope).to.have.property('metadata');
        expect(envelope.metadata).to.have.property('unitTest', true);
    });

    it('builds response envelopes', () =>
    {
        const envelope = envelopeEng.buildResponse({
            id: '123456789',
            context: 'test',
            operation: 'test',
            payload: {
                test: true,
            },
            status: 'succeeded',
        });

        // Test the envelope structure
        expect(envelope).to.not.be.undefined;
        expect(envelope).to.have.property('id', '123456789');
        expect(envelope).to.have.property('messageType', 'response');
        expect(envelope).to.have.property('context', 'test');
        expect(envelope).to.have.property('operation', 'test');
        expect(envelope).to.have.property('timestamp');
        expect(envelope).to.have.property('service', 'unknown');
        expect(envelope).to.have.property('status', 'succeeded');

        expect(envelope).to.have.property('payload');
        expect(envelope.payload).to.have.property('test', true);
    });
});

// ---------------------------------------------------------------------------------------------------------------------
