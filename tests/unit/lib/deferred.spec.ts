//----------------------------------------------------------------------------------------------------------------------
// Deferred Unit Tests
//----------------------------------------------------------------------------------------------------------------------

import { expect } from 'chai';

// Lib
import { Deferred, DeferredRequest } from '../../../src/lib/deferred.js';

//----------------------------------------------------------------------------------------------------------------------

describe('Deferred', () =>
{
    describe('constructor()', () =>
    {
        it('creates a new Deferred object', () =>
        {
            const deferred = new Deferred();

            expect(deferred).to.be.an.instanceOf(Deferred);
            expect(deferred).to.have.property('promise');
            expect(deferred).to.have.property('resolve');
            expect(deferred).to.have.property('reject');
        });
    });

    describe('resolve()', () =>
    {
        it('resolves the deferred', async() =>
        {
            const deferred = new Deferred();

            deferred.resolve('test');

            const result = await deferred.promise;

            expect(result).to.equal('test');
        });
    });

    describe('reject()', () =>
    {
        it('rejects the deferred', async() =>
        {
            const deferred = new Deferred();

            deferred.reject('test');

            try
            {
                await deferred.promise;
                expect.fail('Deferred did not reject.');
            }
            catch (err)
            {
                expect(err).to.equal('test');
            }
        });
    });
});

describe('DeferredRequest', () =>
{
    describe('constructor()', () =>
    {
        it('creates a new DeferredRequest object', () =>
        {
            const deferred = new DeferredRequest('test');

            expect(deferred).to.be.an.instanceOf(DeferredRequest);
            expect(deferred).to.have.property('request', 'test');
            expect(deferred).to.have.property('promise');
            expect(deferred).to.have.property('resolve');
            expect(deferred).to.have.property('reject');
        });
    });

    describe('resolve()', () =>
    {
        it('resolves the deferred', async() =>
        {
            const deferred = new DeferredRequest('test');

            deferred.resolve('test');

            const result = await deferred.promise;

            expect(result).to.equal('test');
        });
    });

    describe('reject()', () =>
    {
        it('rejects the deferred', async() =>
        {
            const deferred = new DeferredRequest('test');

            deferred.reject('test');

            try
            {
                await deferred.promise;
                expect.fail('Deferred did not reject.');
            }
            catch (err)
            {
                expect(err).to.equal('test');
            }
        });
    });
});

//----------------------------------------------------------------------------------------------------------------------
