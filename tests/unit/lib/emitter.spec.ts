//----------------------------------------------------------------------------------------------------------------------
// TypedEmitter Unit Tests
//----------------------------------------------------------------------------------------------------------------------

import { expect } from 'chai';

// Lib
import { TypedEmitter } from '../../../src/lib/emitter.js';

//----------------------------------------------------------------------------------------------------------------------

interface TestEvents
{
    test : () => void;
    testArg : (arg1 : string) => void;
    testArgs : (arg1 : string, arg2 : number) => void;
}

class TestEmitter extends TypedEmitter<TestEvents> {}

//----------------------------------------------------------------------------------------------------------------------

describe('TypedEmitter', () =>
{
    let emitter : TestEmitter;
    beforeEach(() =>
    {
        emitter = new TestEmitter();
    });

    it('emits an event', (done) =>
    {
        emitter.on('test', () =>
        {
            done();
        });

        emitter.emit('test');
    });

    it('emits an event with no arguments', (done) =>
    {
        emitter.on('test', () =>
        {
            done();
        });

        emitter.emit('test');
    });

    it('emits an event with a single argument', (done) =>
    {
        emitter.on('testArg', (arg1 : string) =>
        {
            expect(arg1).to.equal('test');

            done();
        });

        emitter.emit('testArg', 'test');
    });

    it('emits an event with multiple arguments', (done) =>
    {
        emitter.on('testArgs', (arg1 : string, arg2 : number) =>
        {
            expect(arg1).to.equal('test');
            expect(arg2).to.equal(1234);

            done();
        });

        emitter.emit('testArgs', 'test', 1234);
    });

    it('emits an event with multiple listeners', (done) =>
    {
        let count = 0;

        emitter.on('test', () =>
        {
            count++;

            if(count === 2)
            {
                done();
            }
        });

        emitter.on('test', () =>
        {
            count++;

            if(count === 2)
            {
                done();
            }
        });

        emitter.emit('test');
    });
});

//----------------------------------------------------------------------------------------------------------------------
