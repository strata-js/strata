// ---------------------------------------------------------------------------------------------------------------------
// Context Unit Tests
// ---------------------------------------------------------------------------------------------------------------------

import { expect } from 'chai';

// Lib
import { StrataContext } from '../../../src/lib/context.js';
import { StrataRequest } from '../../../src/lib/request.js';

// Utils
import { TestMiddleware } from '../../utils/middleware.js';

// ---------------------------------------------------------------------------------------------------------------------

describe('Context', () =>
{
    describe('$callOperation()', () =>
    {
        let request;
        let context;
        let testMiddlewareInst;

        beforeEach(() =>
        {
            context = new StrataContext();

            request = new StrataRequest({
                id: '1234567890',
                context: 'test',
                operation: 'foo',
                messageType: 'request',
                payload: {},
                metadata: {},
            });

            testMiddlewareInst = new TestMiddleware();
        });

        it('executes the specified operation name', async() =>
        {
            let opCalled = false;
            context.registerOperation('foo', async() =>
            {
                opCalled = true;
            });

            // Call operation
            await context.$callOperation('foo', request);

            // Tests
            expect(opCalled).to.equal(true);
        });

        it('it calls the `beforeRequest` function on all registered middleware', async() =>
        {
            let opCalled = false;
            context.useMiddleware(testMiddlewareInst);
            context.registerOperation('foo', async() =>
            {
                opCalled = true;
            });

            // Call operation
            await context.$callOperation('foo', request);

            // Tests
            expect(opCalled).to.equal(true);
            expect(testMiddlewareInst.beforeCalled).to.equal(1);
        });

        describe('Middleware', () =>
        {
            it('it stop calling `beforeRequest` functions if any middleware throw a error', async() =>
            {
                let opCalled = false;
                const mw1 = new TestMiddleware('ErrorMiddleware', {
                    beforeFn: () =>
                    {
                        throw new Error('Fail!');
                    },
                });

                const mw2 = new TestMiddleware('SecondMiddleware');

                context.useMiddleware(mw1);
                context.useMiddleware(mw2);

                context.registerOperation('foo', async() =>
                {
                    opCalled = true;
                });

                try
                {
                // Call operation
                    await context.$callOperation('foo', request);
                }
                catch (_ex) { /* do nothing */ }

                // Tests
                expect(opCalled).to.equal(false);
                expect(mw1.beforeCalled).to.equal(1);
                expect(mw2.beforeCalled).to.equal(0);
            });

            it('it stops calling `beforeRequest` functions if any middleware succeed or fail a request', async() =>
            {
                let opCalled = false;
                const mw1 = new TestMiddleware('ErrorMiddleware', {
                    beforeFn: () =>
                    {
                        request.succeed({});
                    },
                });

                const mw2 = new TestMiddleware('SecondMiddleware');

                context.useMiddleware(mw1);
                context.useMiddleware(mw2);

                context.registerOperation('foo', async() =>
                {
                    opCalled = true;
                });

                // Call operation
                await context.$callOperation('foo', request);

                // Tests
                expect(opCalled).to.equal(false);
                expect(mw1.beforeCalled).to.equal(1);
                expect(mw2.beforeCalled).to.equal(0);
            });

            // eslint-disable-next-line @stylistic/max-len
            it('it still calls `succeed` or `failed` on all middleware if a request is succeeded or failed in `beforeRequest`', async() =>
            {
                let opCalled = false;
                const mw1 = new TestMiddleware('ErrorMiddleware', {
                    beforeFn: () =>
                    {
                        request.succeed({});
                    },
                });

                const mw2 = new TestMiddleware('SecondMiddleware');

                context.useMiddleware(mw1);
                context.useMiddleware(mw2);

                context.registerOperation('foo', async() =>
                {
                    opCalled = true;
                });

                // Call operation
                await context.$callOperation('foo', request);

                // Tests
                expect(opCalled).to.equal(false);
                expect(mw1.beforeCalled).to.equal(1);
                expect(mw2.beforeCalled).to.equal(0);
                expect(mw1.successCalled).to.equal(1);
                expect(mw2.successCalled).to.equal(1);
            });

            it('succeeds the request if the operation did not explicitly succeed the request.', async() =>
            {
                let opCalled = false;
                context.useMiddleware(testMiddlewareInst);
                context.registerOperation('foo', async() =>
                {
                    opCalled = true;
                });

                // Call operation
                await context.$callOperation('foo', request);

                // Tests
                expect(opCalled).to.equal(true);
                expect(request.status).to.equal('succeeded');
            });

            it('fails the request if the operation throws.', async() =>
            {
                let opCalled = false;
                context.useMiddleware(testMiddlewareInst);
                context.registerOperation('foo', async() =>
                {
                    opCalled = true;
                    throw new Error('Failure!');
                });

                try
                {
                // Call operation
                    await context.$callOperation('foo', request);
                }
                catch (_ex) { /* do nothing */ }

                // Tests
                expect(opCalled).to.equal(true);
                expect(request.status).to.equal('failed');
            });

            // eslint-disable-next-line @stylistic/max-len
            it('calls the `success` function on all registered middleware if the operation succeeds the request', async() =>
            {
                const mw1 = new TestMiddleware('FirstMiddleware');
                const mw2 = new TestMiddleware('SecondMiddleware');

                context.useMiddleware(mw1);
                context.useMiddleware(mw2);

                context.registerOperation('foo', async() =>
                {
                    request.succeed({});
                });

                // Call operation
                await context.$callOperation('foo', request);

                // Tests
                expect(mw1.successCalled).to.equal(1, 'FirstMiddleware');
                expect(mw2.successCalled).to.equal(1, 'SecondMiddleware');
            });

            // eslint-disable-next-line @stylistic/max-len
            it('calls the `failure` function on all registered middleware if the operation fails the request', async() =>
            {
                const mw1 = new TestMiddleware('FirstMiddleware');
                const mw2 = new TestMiddleware('SecondMiddleware');

                context.useMiddleware(mw1);
                context.useMiddleware(mw2);

                context.registerOperation('foo', async() =>
                {
                    request.failure({});
                });

                // Call operation
                await context.$callOperation('foo', request);

                // Tests
                expect(mw1.failureCalled).to.equal(1, 'FirstMiddleware');
                expect(mw2.failureCalled).to.equal(1, 'SecondMiddleware');
            });

            it('calls context middleware `beforeRequest` before operation middleware `beforeRequest`', async() =>
            {
                let mw1TS;
                let mw2TS;

                const mw1 = new TestMiddleware('FirstMiddleware', {
                    beforeFn()
                    {
                        mw1TS = process.hrtime.bigint();
                    },
                });
                const mw2 = new TestMiddleware('SecondMiddleware', {
                    beforeFn()
                    {
                        mw2TS = process.hrtime.bigint();
                    },
                });

                context.useMiddleware(mw1);

                context.registerOperation('foo', async() =>
                {
                    return {};
                }, [ 'payload' ], [ mw2 ]);

                // Call operation
                await context.$callOperation('foo', request);

                // Tests
                expect(Number(mw1TS)).to.be.lessThan(Number(mw2TS));
            });

            // eslint-disable-next-line @stylistic/max-len
            it('calls operation middleware `success` or `failure before context middleware `success` or `failure`', async() =>
            {
                let mw1TS;
                let mw2TS;

                const mw1 = new TestMiddleware('FirstMiddleware', {
                    successFn()
                    {
                        mw1TS = process.hrtime.bigint();
                    },
                });
                const mw2 = new TestMiddleware('SecondMiddleware', {
                    successFn()
                    {
                        mw2TS = process.hrtime.bigint();
                    },
                });

                context.useMiddleware(mw1);

                context.registerOperation('foo', async() =>
                {
                    return {};
                }, [ 'payload' ], [ mw2 ]);

                // Call operation
                await context.$callOperation('foo', request);

                // Tests
                expect(Number(mw2TS)).to.be.lessThan(Number(mw1TS));
            });

            it('calls middleware in the order they were registered', async() =>
            {
                let mw1TS;
                let mw2TS;

                const mw1 = new TestMiddleware('FirstMiddleware', {
                    successFn()
                    {
                        mw1TS = process.hrtime.bigint();
                    },
                });
                const mw2 = new TestMiddleware('SecondMiddleware', {
                    successFn()
                    {
                        mw2TS = process.hrtime.bigint();
                    },
                });

                context.registerOperation('foo', async() =>
                {
                    return {};
                }, [ 'payload' ], [ mw2, mw1 ]);

                // Call operation
                await context.$callOperation('foo', request);

                // Tests
                expect(Number(mw2TS)).to.be.lessThan(Number(mw1TS));
            });
        });
    });

    describe('registerOperation()', () =>
    {
        let context;

        beforeEach(() =>
        {
            context = new StrataContext();
        });

        it('allows registering an operation', () =>
        {
            function registrationTest() : void
            {
                context.registerOperation('foo', async() =>
                {
                    /* Do nothing */
                });
            }

            expect(registrationTest).to.not.throw();
            expect(context.getOperationNames()).to.include('foo');
        });

        it('does not allow overwriting a previously registered operation', () =>
        {
            context.registerOperation('foo', async() =>
            {
                /* Do nothing */
            });

            function registrationTest() : void
            {
                context.registerOperation('foo', async() =>
                {
                    /* Do nothing, again! */
                });
            }

            expect(registrationTest).to.throw();
            expect(context.getOperationNames()).to.include('foo');
        });

        it('supports registering the same operation under multiple names', () =>
        {
            async function op1() : Promise<void>
            {
                /* Do nothing */
            }

            context.registerOperation('foo', op1);
            context.registerOperation('bar', op1);

            expect(context.getOperationNames()).to.include('foo');
            expect(context.getOperationNames()).to.include('bar');
        });

        it('allows registering middleware for just this operation', async() =>
        {
            const mw1 = new TestMiddleware('FirstMiddleware');

            async function op1() : Promise<void>
            {
                /* Do nothing */
            }

            context.registerOperation('foo', op1, [ 'payload' ], [ mw1 ]);
            context.registerOperation('bar', op1);

            const request = new StrataRequest({
                id: '1234567890',
                context: 'test',
                operation: 'foo',
                messageType: 'request',
                payload: {},
                metadata: {},
            });

            // Call operation
            await context.$callOperation('foo', request);
            await context.$callOperation('bar', request);

            expect(mw1.beforeCalled).to.equal(1);
        });

        describe('Operation Parameters', () =>
        {
            it('allows specifying properties to extract from the passed in request', async() =>
            {
                let arg;
                context.registerOperation('foo', async(payload) =>
                {
                    arg = payload;
                }, [ 'payload' ]);

                const request = new StrataRequest({
                    id: '1234567890',
                    context: 'test',
                    operation: 'foo',
                    messageType: 'request',
                    payload: { foobar: '123' },
                    metadata: {},
                });

                // Call operation
                await context.$callOperation('foo', request);

                // Tests
                expect(arg).to.deep.equal(request.payload);
            });

            it('if empty, the full request is passed to the operation', async() =>
            {
                let arg;
                context.registerOperation('foo', async(payload) =>
                {
                    arg = payload;
                });

                const request = new StrataRequest({
                    id: '1234567890',
                    context: 'test',
                    operation: 'foo',
                    messageType: 'request',
                    payload: { foobar: '123' },
                    metadata: {},
                });

                // Call operation
                await context.$callOperation('foo', request);

                // Tests
                expect(arg).to.deep.equal(request);
            });

            it('if multiple properties are specified, multiple arguments are passed to the operation.', async() =>
            {
                let arg1;
                let arg2;
                context.registerOperation('foo', async(payload1, payload2) =>
                {
                    arg1 = payload1;
                    arg2 = payload2;
                }, [ 'payload.foobar', 'payload.bar' ]);

                const request = new StrataRequest({
                    id: '1234567890',
                    context: 'test',
                    operation: 'foo',
                    messageType: 'request',
                    payload: { foobar: '123', bar: { baz: { omg: 123, zzy: true } } },
                    metadata: {},
                });

                // Call operation
                await context.$callOperation('foo', request);

                // Tests
                expect(arg1).to.deep.equal(request.payload.foobar);
                expect(arg2).to.deep.equal(request.payload.bar);
            });
        });
    });

    describe('registerOperationsForInstance()', () =>
    {
        let context;

        beforeEach(() =>
        {
            context = new StrataContext();
        });

        it('registers all functions on an object as operations', () =>
        {
            const manager = {
                test1() { /* Do nothing */ },
                test2() { /* Do nothing */ },
            };

            context.registerOperationsForInstance(manager);

            expect(context.getOperationNames()).to.have.members([ 'test2', 'test1' ]);
        });

        it('supports class instances or objects', () =>
        {
            class TestManager
            {
                test1() : void { /* Do nothing */ }
                test2() : void { /* Do nothing */ }
            }

            const manager = new TestManager();

            context.registerOperationsForInstance(manager);

            expect(context.getOperationNames()).to.have.members([ 'test2', 'test1' ]);
        });

        it('supports a whitelist by specifying function names in the `includes` list', () =>
        {
            const manager = {
                test1() { /* Do nothing */ },
                test2() { /* Do nothing */ },
            };

            context.registerOperationsForInstance(manager, [], [], { include: [ 'test2' ] });

            expect(context.getOperationNames()).to.have.members([ 'test2' ]);
        });

        it('supports a blacklist by specifying function names in the `excludes` list', () =>
        {
            const manager = {
                test1() { /* Do nothing */ },
                test2() { /* Do nothing */ },
            };

            context.registerOperationsForInstance(manager, [], [], { exclude: [ 'test1' ] });

            expect(context.getOperationNames()).to.have.members([ 'test2' ]);
        });

        it('exclude overrides if `includes` and `excludes` are specified and overlap', () =>
        {
            const manager = {
                test1() { /* Do nothing */ },
                test2() { /* Do nothing */ },
                test3() { /* Do nothing */ },
                test4() { /* Do nothing */ },
            };

            context.registerOperationsForInstance(manager, [], [], {
                include: [ 'test2', 'test3' ],
                exclude: [ 'test1', 'test2' ],
            });
            expect(context.getOperationNames()).to.have.members([ 'test3' ]);
        });

        describe('Operation Parameters', () =>
        {
            it('allows specifying properties to extract from the passed in request', async() =>
            {
                let arg;
                const manager = {
                    foo(payload)
                    {
                        arg = payload;
                    },
                };

                context.registerOperationsForInstance(manager, [ 'payload' ]);

                const request = new StrataRequest({
                    id: '1234567890',
                    context: 'test',
                    operation: 'foo',
                    messageType: 'request',
                    payload: { foobar: '123' },
                    metadata: {},
                });

                // Call operation
                await context.$callOperation('foo', request);

                // Tests
                expect(arg).to.deep.equal(request.payload);
            });

            it('if empty, the full request is passed to the operation', async() =>
            {
                let arg;
                const manager = {
                    foo(payload)
                    {
                        arg = payload;
                    },
                };

                context.registerOperationsForInstance(manager);

                const request = new StrataRequest({
                    id: '1234567890',
                    context: 'test',
                    operation: 'foo',
                    messageType: 'request',
                    payload: { foobar: '123' },
                    metadata: {},
                });

                // Call operation
                await context.$callOperation('foo', request);

                // Tests
                expect(arg).to.deep.equal(request);
            });

            it('if multiple properties are specified, multiple arguments are passed to the operation.', async() =>
            {
                let arg1;
                let arg2;
                const manager = {
                    foo(payload1, payload2)
                    {
                        arg1 = payload1;
                        arg2 = payload2;
                    },
                };

                context.registerOperationsForInstance(manager, [ 'payload.foobar', 'payload.bar' ]);

                const request = new StrataRequest({
                    id: '1234567890',
                    context: 'test',
                    operation: 'foo',
                    messageType: 'request',
                    payload: { foobar: '123', bar: { baz: { omg: 123, zzy: true } } },
                    metadata: {},
                });

                // Call operation
                await context.$callOperation('foo', request);

                // Tests
                expect(arg1).to.deep.equal(request.payload.foobar);
                expect(arg2).to.deep.equal(request.payload.bar);
            });
        });
    });

    describe('useMiddleware()', () =>
    {
        let context;

        beforeEach(() =>
        {
            context = new StrataContext();
        });

        it('adds the specified middleware to the list of context middleware', async() =>
        {
            const mw1 = new TestMiddleware();

            context.useMiddleware(mw1);

            context.registerOperation('foo', async() =>
            {
                return {};
            });

            const request = new StrataRequest({
                id: '1234567890',
                context: 'test',
                operation: 'foo',
                messageType: 'request',
                payload: {},
                metadata: {},
            });

            // Call operation
            await context.$callOperation('foo', request);

            // Tests
            expect(mw1.beforeCalled).to.equal(1);
        });

        it('supports adding an array of middleware', async() =>
        {
            const mw1 = new TestMiddleware('FirstMiddleware');
            const mw2 = new TestMiddleware('SecondMiddleware');

            context.useMiddleware([ mw1, mw2 ]);

            context.registerOperation('foo', async() =>
            {
                return {};
            });

            const request = new StrataRequest({
                id: '1234567890',
                context: 'test',
                operation: 'foo',
                messageType: 'request',
                payload: {},
                metadata: {},
            });

            // Call operation
            await context.$callOperation('foo', request);

            // Tests
            expect(mw1.beforeCalled).to.equal(1);
            expect(mw2.beforeCalled).to.equal(1);
        });
    });

    describe('getOperationNames()', () =>
    {
        it('returns a list of the operation names registered.', () =>
        {
            const context = new StrataContext();
            context.registerOperation('foo', () => Promise.resolve({}));
            context.registerOperation('bar', () => Promise.resolve({}));

            const operationsNames = context.getOperationNames();

            expect(operationsNames).to.be.an.instanceOf(Array);
            expect(operationsNames.length).to.equal(2);
            expect(operationsNames).to.deep.equal([ 'foo', 'bar' ]);
        });

        it('returns an empty array if no operations are registered.', () =>
        {
            const context = new StrataContext();
            const operationsNames = context.getOperationNames();

            expect(operationsNames).to.be.an.instanceOf(Array);
            expect(operationsNames.length).to.equal(0);
        });
    });
});

// ---------------------------------------------------------------------------------------------------------------------
