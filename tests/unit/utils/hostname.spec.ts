//----------------------------------------------------------------------------------------------------------------------
// Hostname Utils Tests
//----------------------------------------------------------------------------------------------------------------------

import { hostname } from 'os';
import { expect } from 'chai';

// Utils
import { getHostname } from '../../../src/utils/hostname.js';

//----------------------------------------------------------------------------------------------------------------------

describe('Hostname Utils', () =>
{
    describe('getHostname()', () =>
    {
        it('returns the HOSTNAME environment variable, if set', () =>
        {
            process.env.HOSTNAME = 'test';
            const host = getHostname();

            expect(host).to.equal(process.env.HOSTNAME);
        });

        it('returns the host, if HOSTNAME is not set', () =>
        {
            delete process.env.HOSTNAME;
            const host = getHostname();

            expect(host).to.not.be.undefined;
            expect(host).to.equal(hostname());
        });
    });
});

//----------------------------------------------------------------------------------------------------------------------
