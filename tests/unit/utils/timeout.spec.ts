//----------------------------------------------------------------------------------------------------------------------
// Promise Timeout Util Tests
//----------------------------------------------------------------------------------------------------------------------

import { expect } from 'chai';

// Utils
import { promiseTimeout } from '../../../src/utils/timeout.js';
import { InternalTimeoutError } from '../../../src/lib/errors.js';

//----------------------------------------------------------------------------------------------------------------------

describe('Promise Timeout Utils', () =>
{
    describe('promiseTimeout()', () =>
    {
        it('resolves the promise, if it resolves before the timeout', async() =>
        {
            const promise = new Promise((resolve) =>
            {
                setTimeout(() => resolve('test'), 10);
            });

            const result = await promiseTimeout(promise, 20);

            expect(result).to.equal('test');
        });

        it('rejects the promise, if it rejects before the timeout', async() =>
        {
            const promise = new Promise((_resolve, reject) =>
            {
                setTimeout(() => reject(new Error('test')), 10);
            });

            try
            {
                await promiseTimeout(promise, 20);
                expect.fail('Promise did not reject.');
            }
            catch (err)
            {
                if(err instanceof Error)
                {
                    expect(err.message).to.equal('test');
                }
                else
                {
                    expect.fail('Promise did not reject with an error.');
                }
            }
        });

        it('rejects the promise, if it does not resolve before the timeout', async() =>
        {
            const promise = new Promise((resolve) =>
            {
                setTimeout(() => resolve('test'), 20);
            });

            try
            {
                await promiseTimeout(promise, 10);
                expect.fail('Promise did not reject.');
            }
            catch (err)
            {
                if(err instanceof Error)
                {
                    expect(err).to.be.instanceOf(InternalTimeoutError);
                    expect(err.message).to.equal('Operation timed out after 10ms.');
                }
                else
                {
                    expect.fail('Promise did not reject with an error.');
                }
            }
        });
    });
});

//----------------------------------------------------------------------------------------------------------------------
