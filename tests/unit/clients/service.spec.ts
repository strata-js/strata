// ---------------------------------------------------------------------------------------------------------------------
// Service Unit Tests
// ---------------------------------------------------------------------------------------------------------------------

import toobusy from 'node-toobusy';
import { expect } from 'chai';
import { StrataRequest } from '../../../src/lib/request.js';

// Interfaces
import { OperationMiddleware } from '../../../src/interfaces/middleware.js';

// Clients
import { StrataService } from '../../../src/clients/service.js';

// Resource Access
import { NullBackend } from '../../../src/resource-access/backends/null.js';
import { StrataContext } from '../../../src/lib/context.js';
import { beforeEach } from 'mocha';

// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------

describe('Service', () =>
{
    let service : StrataService;

    beforeEach(() =>
    {
        service = new StrataService({
            service: { serviceGroup: 'TestService' },
            backend: { type: 'null' },
        });
    });

    it('should be able to be constructed', () =>
    {
        expect(service).to.be.instanceOf(StrataService);
    });

    it('should be able to be initialized', async() =>
    {
        await service.start();
        expect(service.initialized).to.be.true;
    });

    it('automatically listens for requests when initialized', async() =>
    {
        await service.start();
        expect((service.queueEngine.backend as NullBackend).listeningForRequests).to.be.true;
    });

    it('automatically listens for commands when initialized', async() =>
    {
        await service.start();
        expect((service.queueEngine.backend as NullBackend).listeningForCommands).to.be.true;
    });

    describe('registerTeardown()', () =>
    {
        it('registers a tear down function', async() =>
        {
            let called = false;
            service.registerTeardown(async() => { called = true; });

            await service.teardown();
            expect(called).to.be.true;
        });
    });

    describe('registerContext()', () =>
    {
        it('registers a context', () =>
        {
            const context = new StrataContext();
            context.registerOperation('test', () => Promise.resolve());
            service.registerContext('test', context);

            expect(service.contexts).to.have.deep.property('test', [ 'test' ]);
        });

        it('throws an error if a context with the same name is already registered', () =>
        {
            const context = new StrataContext();
            context.registerOperation('test', () => Promise.resolve());

            // Register the first time
            service.registerContext('test', context);

            expect(() => service.registerContext('test', context))
                .to.throw('The context \'test\' has already been registered.');
        });
    });

    describe('getContext()', () =>
    {
        it('returns a context', () =>
        {
            const context = new StrataContext();
            context.registerOperation('test', () => Promise.resolve());
            service.registerContext('test', context);

            expect(service.getContext('test')).to.equal(context);
        });

        it('throws an error if the context does not exist', () =>
        {
            expect(() => service.getContext('test'))
                .to.throw('Missing or unrecognized context \'test\'.');
        });
    });

    describe('useMiddleware()', () =>
    {
        it('registers middleware', () =>
        {
            const mw : OperationMiddleware = {
                async beforeRequest(request : StrataRequest)
                {
                    return request;
                },
            };

            service.useMiddleware(mw);
            expect(service.middleware).to.have.lengthOf(1);
            expect(service.middleware).to.include(mw);
        });

        it('registers multiple middleware', () =>
        {
            const mw1 : OperationMiddleware = {
                async beforeRequest(request : StrataRequest)
                {
                    return request;
                },
            };

            const mw2 : OperationMiddleware = {
                async beforeRequest(request : StrataRequest)
                {
                    return request;
                },
            };

            service.useMiddleware([ mw1, mw2 ]);
            expect(service.middleware).to.have.lengthOf(2);
            expect(service.middleware).to.include(mw1);
            expect(service.middleware).to.include(mw2);
        });
    });

    describe('setConcurrency()', () =>
    {
        it('sets the concurrency', () =>
        {
            service.setConcurrency(5);
            expect(service.concurrency).to.equal(5);
        });
    });

    describe('setTooBusy()', () =>
    {
        it('sets the too busy config', () =>
        {
            service.setTooBusy({ interval: 1234 });
            expect(toobusy.interval).to.equal(1234);
        });
    });
});

// ---------------------------------------------------------------------------------------------------------------------
