// ---------------------------------------------------------------------------------------------------------------------
// Unit Tests for the client.spec.ts module.
// ---------------------------------------------------------------------------------------------------------------------

import { hostname } from 'os';
import { expect } from 'chai';

import { AlreadyInitializedError } from '../../../src/lib/errors.js';

// Clients
import { StrataClient } from '../../../src/clients/client.js';

// Resource Access
import { NullBackend } from '../../../src/resource-access/backends/null.js';
import { beforeEach } from 'mocha';

// ---------------------------------------------------------------------------------------------------------------------

describe('Client', () =>
{
    let client : StrataClient;

    beforeEach(() =>
    {
        client = new StrataClient({
            client: { name: 'Test Client' },
            backend: { type: 'null' },
        });
    });

    it('should be able to be constructed', () =>
    {
        expect(client).to.be.an.instanceOf(StrataClient);
    });

    it('should be able to be initialized', async() =>
    {
        await client.start();
        expect(client.initialized).to.be.true;
    });

    it('automatically listens for command responses when initialized', async() =>
    {
        await client.start();
        const backend = client.queueEngine.backend as NullBackend;
        expect(backend.listeningForCommandResponses).to.be.true;
    });

    it('automatically listens for responses when initialized', async() =>
    {
        await client.start();
        const backend = client.queueEngine.backend as NullBackend;
        expect(backend.listeningForResponses).to.be.true;
    });

    describe('constructor()', () =>
    {
        it('should set the client name to the passed in name', () =>
        {
            expect(client.name).to.equal('Test Client');
        });

        it('should set the client name to the service group name if no name is passed in', () =>
        {
            client = new StrataClient({
                service: {
                    serviceGroup: 'Test Service Group',
                },
                backend: {
                    type: 'null',
                },
            });

            expect(client.name).to.equal('Client:Test Service Group');
        });

        it('should set the client name to the default name if no name or service group is passed in', () =>
        {
            client = new StrataClient({
                backend: {
                    type: 'null',
                },
            });

            expect(client.name).to.equal(`StrataClient.${ hostname() }`);
        });

        it('should set the client id to the passed in id', () =>
        {
            client = new StrataClient({
                client: {
                    id: 'test-id',
                },
                backend: {
                    type: 'null',
                },
            });

            expect(client.id).to.equal('test-id');
        });

        it('should set the client id to the default id if no id is passed in', () =>
        {
            expect(client.id).to.match(/^[a-zA-Z0-9]{20}$/);
        });
    });

    describe('start()', () =>
    {
        it('should throw an error if the client is already initialized', async() =>
        {
            await client.start();

            try
            {
                await client.start();
                expect.fail('Expected an error to be thrown.');
            }
            catch (err)
            {
                expect(err).to.be.an.instanceOf(AlreadyInitializedError);
            }
        });
    });

    describe('command()', () =>
    {
        it('should call the backend command() method', async() =>
        {
            await client.start();
            const backend = client.queueEngine.backend as NullBackend;
            await client.command('concurrency', 'test', { concurrency: 1 });
            expect(backend.command?.payload).to.have.property('concurrency', 1);
        });

        it('sets the response channel if the command requires a reply', async() =>
        {
            await client.start();
            const backend = client.queueEngine.backend as NullBackend;
            client.command('info', 'test', undefined);
            expect(backend.command).to.have.property('responseChannel', `Clients:${ client.id }`);
        });
    });

    describe('post()', () =>
    {
        it('posts a request', async() =>
        {
            await client.start();
            await client.post('TestService', 'test', 'test', { test: 'test' });

            const backend = client.queueEngine.backend as NullBackend;

            expect(backend.request).to.not.be.undefined;
            expect(backend.request).to.have.property('context', 'test');
            expect(backend.request).to.have.property('operation', 'test');
            expect(backend.request).to.have.property('payload');
            expect(backend.request?.payload).to.have.property('test', 'test');
        });

        it('throws an error if the client is not initialized', async() =>
        {
            try
            {
                await client.post('TestService', 'test', 'test', { test: 'test' });
                expect.fail('Expected an error to be thrown.');
            }
            catch (err)
            {
                expect(err).to.be.an.instanceOf(Error);
            }
        });
    });

    describe('request()', () =>
    {
        it('sends a request', async() =>
        {
            await client.start();
            const responsePromise = client.request('TestService', 'test', 'test', { test: 'test' });

            const backend = client.queueEngine.backend as NullBackend;

            expect(backend.request).to.not.be.undefined;
            expect(backend.request).to.have.property('context', 'test');
            expect(backend.request).to.have.property('operation', 'test');
            expect(backend.request).to.have.property('payload');
            expect(backend.request?.payload).to.have.property('test', 'test');

            // Simulate the response
            backend.emit('incomingResponse', {
                id: backend.request?.id ?? '12345',
                payload: { test: 'test' },
                messageType: 'response',
                context: backend.request?.context ?? 'test',
                operation: backend.request?.operation ?? 'test',
                timestamp: (new Date()).toISOString(),
                status: 'succeeded',
                messages: [],
                service: 'TestService',
            });

            const response = await responsePromise;

            expect(response).to.not.be.undefined;
            expect(response).to.have.property('payload');
            expect(response?.payload).to.have.property('test', 'test');
        });

        it('throws an error if the client is not initialized', async() =>
        {
            try
            {
                await client.request('TestService', 'test', 'test', { test: 'test' });
                expect.fail('Expected an error to be thrown.');
            }
            catch (err)
            {
                expect(err).to.be.an.instanceOf(Error);
            }
        });
    });

    describe('discoverServiceGroups()', () =>
    {
        it('returns an object of service groups', async() =>
        {
            await client.start();
            const clientBackend = client.queueEngine.backend as NullBackend;
            clientBackend.discoveredServices = {
                TestService: {
                    123456789: {
                        id: '123456789',
                        name: 'Test Service',
                        serviceGroup: 'TestService',
                        serviceName: 'TestService',
                        environment: 'test',
                        hostname: 'localhost',
                        version: '0.0.0',
                        strataVersion: '2.0.0',
                        concurrency: 1,
                        outstanding: 0,
                        queue: 'Requests:TestService.test',
                        backend: { type: 'null' },
                        lastBusy: { time: new Date(), lag: 0 },
                        contexts: {
                            test: [ 'echo', 'echoAsync', 'fail' ],
                            service: [ 'info' ],
                        },
                        lastSeen: (new Date()).toISOString(),
                    },
                },
            };

            const serviceGroups = await client.discoverServices();

            expect(clientBackend.listServicesCalled).to.be.true;
            expect(serviceGroups).to.have.property('TestService');
            expect(serviceGroups.TestService).to.have.property('123456789');
            expect(serviceGroups.TestService['123456789']).to.have.property('id', '123456789');
            expect(serviceGroups.TestService['123456789']).to.have.property('name', 'Test Service');
            expect(serviceGroups.TestService['123456789']).to.have.property('serviceGroup', 'TestService');
            expect(serviceGroups.TestService['123456789']).to.have.property('serviceName', 'TestService');
            expect(serviceGroups.TestService['123456789']).to.have.property('environment', 'test');
            expect(serviceGroups.TestService['123456789']).to.have.property('hostname', 'localhost');
            expect(serviceGroups.TestService['123456789']).to.have.property('version', '0.0.0');
            expect(serviceGroups.TestService['123456789']).to.have.property('strataVersion', '2.0.0');
            expect(serviceGroups.TestService['123456789']).to.have.property('concurrency', 1);
            expect(serviceGroups.TestService['123456789']).to.have.property('outstanding', 0);
            expect(serviceGroups.TestService['123456789']).to.have.property('queue', 'Requests:TestService.test');
            expect(serviceGroups.TestService['123456789']).to.have.property('lastBusy');
            expect(serviceGroups.TestService['123456789']).to.have.property('contexts');
            expect(serviceGroups.TestService['123456789'].contexts).to.have.property('test');
            expect(serviceGroups.TestService['123456789'].contexts.test).to.be.an('array');
            expect(serviceGroups.TestService['123456789'].contexts.test).to.have.lengthOf(3);
            expect(serviceGroups.TestService['123456789'].contexts.test).to.include('echo');
            expect(serviceGroups.TestService['123456789'].contexts.test).to.include('echoAsync');
            expect(serviceGroups.TestService['123456789'].contexts.test).to.include('fail');
            expect(serviceGroups.TestService['123456789'].contexts).to.have.property('service');
            expect(serviceGroups.TestService['123456789'].contexts.service).to.be.an('array');
            expect(serviceGroups.TestService['123456789'].contexts.service).to.have.lengthOf(1);
            expect(serviceGroups.TestService['123456789'].contexts.service).to.include('info');
            expect(serviceGroups.TestService['123456789']).to.have.property('lastSeen');
        });

        it('throws an error if the client is not initialized', async() =>
        {
            try
            {
                await client.discoverServices();
                expect.fail('Expected an error to be thrown.');
            }
            catch (err)
            {
                expect(err).to.be.an.instanceOf(Error);
            }
        });
    });
});

// ---------------------------------------------------------------------------------------------------------------------
