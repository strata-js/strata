// ---------------------------------------------------------------------------------------------------------------------
// Mock StrataRequest
// ---------------------------------------------------------------------------------------------------------------------

import { ServiceError } from '../../../src/lib/errors.js';

// Interfaces
import { RequestEnvelope, ResponseEnvelope } from '../../../src/interfaces/messages.js';

// Clients
import { StrataService } from '../../../src/index.js';

// Engines
import messageFormatEng from '../../../src/engines/envelope.js';

// Utils
import { genID } from '../../../src/utils/id.js';

// ---------------------------------------------------------------------------------------------------------------------

export class MockStrataRequest
{
    id : string = genID();
    status : 'succeeded' | 'failed' | 'pending' = 'pending';
    messageType : 'request' | 'post' = 'request';
    responseQueue : string | undefined;
    response : Record<string, unknown> | undefined;
    completedTimestamp : number | undefined;
    service : StrataService | undefined;

    constructor(service : StrataService)
    {
        this.service = service;
    }

    fail(reason : string | Error) : void
    {
        if(reason instanceof ServiceError)
        {
            this.response = reason.toJSON();
        }
        else if(reason instanceof Error)
        {
            this.response = {
                message: reason.message,
                name: 'FailedRequestError',
                stack: reason.stack,
                code: reason['code'] ?? 'FAILED_REQUEST',
                details: reason['details'],
                isSafeMessage: reason['isSafeMessage'] ?? false,
            };
        }
        else if(typeof reason === 'string')
        {
            this.response = {
                message: reason,
                name: 'FailedRequestError',
                code: 'FAILED_REQUEST',
            };
        }
        else
        {
            this.response = reason;
        }

        this.status = 'failed';
        this.completedTimestamp = Date.now();
    }

    renderRequest() : RequestEnvelope
    {
        const timestamp = new Date().toISOString();

        return {
            id: this.id,
            messageType: 'request',
            context: 'test',
            operation: 'test',
            responseQueue: 'responseQueue',
            timestamp,
            payload: {},
            metadata: {},
        };
    }

    renderResponse() : ResponseEnvelope
    {
        return messageFormatEng.buildResponse({
            id: this.id,
            context: 'test',
            operation: 'test',
            timestamp: this.completedTimestamp,
            payload: this.response,
            status: this.status,
            messages: [],
            service: this.service?.name,
        });
    }
}

// ---------------------------------------------------------------------------------------------------------------------
