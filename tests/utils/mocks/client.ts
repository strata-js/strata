//----------------------------------------------------------------------------------------------------------------------
// MockStrataClient
//----------------------------------------------------------------------------------------------------------------------

import { StrataRequest } from '../../../src/lib/request.js';
import { AlreadyInitializedError } from '../../../src/lib/errors.js';

// Interfaces
import { StrataClientConfig } from '../../../src/interfaces/config.js';
import {
    ServiceCommandResponse,
    ValidCommandName,
} from '../../../src/interfaces/command.js';
import { DiscoveredServiceGroups } from '../../../src/interfaces/discovery.js';
import { ResponseEnvelope } from '../../../src/interfaces/messages.js';
import { CommandArguments, PostArguments, RequestArguments } from './interfaces/arguments.js';

// Engines
import { ClientQueueEngine } from '../../../src/engines/queue.js';

// Utils
import { genID } from '../../../src/utils/id.js';
import { getHostname } from '../../../src/utils/hostname.js';

//----------------------------------------------------------------------------------------------------------------------

export class MockStrataClient
{
    id = genID();
    config : StrataClientConfig;
    name = `StrataClient-${ getHostname() }`;
    queueEngine : ClientQueueEngine;
    version : string;
    initialized = false;

    commandCalls : CommandArguments[] = [];
    postCalls : PostArguments[] = [];
    requestCalls : RequestArguments[] = [];
    teardownCalled = false;

    //------------------------------------------------------------------------------------------------------------------

    get outstandingRequests() : Map<string, StrataRequest>
    {
        return this.queueEngine.outstandingRequests;
    }

    //------------------------------------------------------------------------------------------------------------------

    constructor(config : StrataClientConfig, queueEngine ?: ClientQueueEngine)
    {
        this.config = config;
        this.version = '0.0.0';

        // Set our name to either what was passed in, what was configured, or the default.
        this.name = config.client?.name
            ?? (config.service?.serviceGroup ? `${ config.service.serviceGroup } Client` : this.name);

        // Set our id to either what was passed in, or the default autogenerated id.
        this.id = config.client?.id ?? this.id;

        if(!queueEngine)
        {
            // TODO: This should be a mock service queue engine
            queueEngine = new ClientQueueEngine(this as any);
        }
        this.queueEngine = queueEngine;
    }

    public async start() : Promise<void>
    {
        if(!this.initialized)
        {
            this.initialized = true;
        }
        else
        {
            throw new AlreadyInitializedError('Initialization', 'client');
        }
    }

    public async command(
        command : ValidCommandName,
        target = 'Services',
        payload ?: Record<string, unknown>
    ) : Promise<ServiceCommandResponse | undefined>
    {
        this.commandCalls.push({ command, target, payload });

        return undefined;
    }

    public async post<MetadataType extends Record<string, unknown> = Record<string, unknown>>(
        serviceGroupOrAlias : string,
        context : string,
        operation : string,
        payload : Record<string, unknown>,
        metadata ?: MetadataType,
        auth ?: string
    ) : Promise<void>
    {
        this.postCalls.push({
            serviceGroupOrAlias,
            context,
            operation,
            payload,
            metadata,
            auth,
        });
    }

    public async request<
        ReturnPayloadType = Record<string, unknown>,
        MetadataType extends Record<string, unknown> = Record<string, unknown>>(
        serviceGroupOrAlias : string,
        context : string,
        operation : string,
        payload : Record<string, unknown>,
        metadata ?: MetadataType,
        auth ?: string,
        timeout ?: number
    ) : Promise<ResponseEnvelope<ReturnPayloadType>>
    {
        this.requestCalls.push({
            serviceGroupOrAlias,
            context,
            operation,
            payload,
            metadata,
            auth,
            timeout,
        });

        return {
            id: genID(),
            service: this.name,
            messageType: 'response',
            context,
            operation,
            timestamp: (new Date()).toISOString(),
            payload: { test: true } as ReturnPayloadType,
            status: 'succeeded',
            messages: [],
        };
    }

    public async discoverServiceGroups() : Promise<DiscoveredServiceGroups>
    {
        return {};
    }

    async teardown() : Promise<void>
    {
        this.teardownCalled = false;
    }
}

//----------------------------------------------------------------------------------------------------------------------
