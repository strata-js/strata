//----------------------------------------------------------------------------------------------------------------------
// MockStrataContext
//----------------------------------------------------------------------------------------------------------------------

import { Deferred } from '../../../src/lib/deferred.js';
import { StrataContext } from '../../../src/lib/context.js';

// Interfaces
import { OperationHandler } from '../../../src/interfaces/context.js';
import { OperationMiddleware } from '../../../src/interfaces/middleware.js';

//----------------------------------------------------------------------------------------------------------------------

export class MockStrataContext extends StrataContext
{
    testOperations : Record<string, Deferred> = {};

    public registerTestOperation<ReturnPayloadType>(
        opName : string,
        opFunc : OperationHandler<ReturnPayloadType>,
        opFuncParams : string[] = [],
        middleware : OperationMiddleware[] = []
    ) : void
    {
        const newOpFunc = async(...args : unknown[]) : Promise<ReturnPayloadType> =>
        {
            const deferred = new Deferred();
            this.testOperations[opName] = deferred;

            // Wait for deferred to be resolved
            await deferred.promise;

            return opFunc(...args) as any;
        };

        super.registerOperation(opName, newOpFunc as OperationHandler<ReturnPayloadType>, opFuncParams, middleware);
    }
}

//----------------------------------------------------------------------------------------------------------------------
