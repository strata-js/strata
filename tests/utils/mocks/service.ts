//----------------------------------------------------------------------------------------------------------------------
// MockStrataService
//----------------------------------------------------------------------------------------------------------------------

import { StrataRequest } from '../../../src/lib/request.js';
import { StrataContext } from '../../../src/lib/context.js';
import { UnknownContextError } from '../../../src/lib/errors.js';

// Clients
import { StrataClient } from '../../../src/clients/client.js';
import { MockStrataClient } from './client.js';

// Interfaces
import { PostArguments, RequestArguments } from './interfaces/arguments.js';
import { ServiceInfo } from '../../../src/interfaces/service.js';
import { StrataConfig, TooBusyConfig } from '../../../src/interfaces/config.js';
import { DiscoveredServiceGroups } from '../../../src/interfaces/discovery.js';
import { OperationMiddleware } from '../../../src/interfaces/middleware.js';
import { ResponseEnvelope } from '../../../src/interfaces/messages.js';

// Engines
import { ServiceQueueEngine } from '../../../src/engines/queue.js';

// Utils
import { getHostname } from '../../../src/utils/hostname.js';
import { genID } from '../../../src/utils/id.js';

//----------------------------------------------------------------------------------------------------------------------

export class MockStrataService
{
    id = genID();
    initialized = false;
    config : StrataConfig;
    name : string;
    displayName : string;
    serviceGroup : string;
    client : StrataClient;
    version : string;
    concurrency : number;
    lastBusy : { time : Date, lag : number };

    queueEngine : ServiceQueueEngine;
    middleware = new Set<OperationMiddleware>();
    #contexts = new Map<string, StrataContext>();

    postCalls : PostArguments[] = [];
    requestCalls : RequestArguments[] = [];
    teardownCalled = false;

    //------------------------------------------------------------------------------------------------------------------

    get outstandingRequests() : Map<string, StrataRequest>
    {
        return this.queueEngine.outstandingRequests;
    }

    get contexts() : Record<string, string[]>
    {
        return Array.from(this.#contexts.keys())
            .reduce((accum, contextName) =>
            {
                const contextInst = this.#contexts.get(contextName);
                if(contextInst)
                {
                    accum[contextName] = contextInst.getOperationNames();
                }
                return accum;
            }, {});
    }

    get info() : ServiceInfo
    {
        return {
            queue: 'TestService',
            serviceGroup: 'TestService',
            serviceName: this.name,
            id: this.id,
            name: this.name,
            version: '0.0.0',
            strataVersion: this.version,
            environment: process.env.ENVIRONMENT ?? 'local',
            concurrency: this.concurrency,
            outstanding: this.outstandingRequests.size,
            hostname: getHostname(),
            lastBusy: this.lastBusy,
            contexts: this.contexts,
            backend: { type: this.config.backend.type },
        };
    }

    //------------------------------------------------------------------------------------------------------------------

    constructor(config : StrataConfig)
    {
        this.config = config;
        this.name = 'mock-service';
        this.displayName = 'Mock Service';
        this.serviceGroup = config.service?.serviceGroup ?? 'MockService';
        this.version = '0.0.0';
        this.concurrency = 16;
        this.lastBusy = { time: new Date(), lag: 0 };

        // TODO: This should be a mock service queue engine
        this.queueEngine = new ServiceQueueEngine(this as any);

        this.client = new MockStrataClient(config) as any;
    }

    //------------------------------------------------------------------------------------------------------------------

    async start() : Promise<void>
    {
        this.initialized = true;
    }

    public registerClient(client : StrataClient) : void
    {
        this.client = client;
    }

    public registerContext(contextName : string, context : StrataContext) : void
    {
        this.#contexts.set(contextName, context);
    }

    public getContext(contextName : string) : StrataContext
    {
        const context = this.#contexts.get(contextName);
        if(!context)
        {
            throw new UnknownContextError(contextName);
        }

        return context;
    }

    public useMiddleware(middleware : OperationMiddleware | OperationMiddleware[]) : void
    {
        if(!Array.isArray(middleware))
        {
            middleware = [ middleware ];
        }

        middleware.forEach((mw) =>
        {
            this.middleware.add(mw);
        });
    }

    public setConcurrency(concurrency : number) : void
    {
        this.concurrency = concurrency;
    }

    public setTooBusy(busyOptions : TooBusyConfig) : void
    {
        if(!this.config.service)
        {
            this.config.service = { serviceGroup: 'MockService' };
        }
        this.config.service.toobusy = busyOptions;
        this.lastBusy = { time: new Date(), lag: 0 };
    }

    public async post<MetadataType extends Record<string, unknown> = Record<string, unknown>>(
        serviceGroupOrAlias : string,
        context : string,
        operation : string,
        payload : Record<string, unknown>,
        metadata ?: MetadataType,
        auth ?: string
    ) : Promise<void>
    {
        this.postCalls.push({
            serviceGroupOrAlias,
            context,
            operation,
            payload,
            metadata,
            auth,
        });
    }

    public async request<
        ReturnPayloadType = Record<string, unknown>,
        MetadataType extends Record<string, unknown> = Record<string, unknown>>(
        serviceGroupOrAlias : string,
        context : string,
        operation : string,
        payload : Record<string, unknown>,
        metadata ?: MetadataType,
        auth ?: string,
        timeout ?: number
    ) : Promise<ResponseEnvelope<ReturnPayloadType>>
    {
        this.requestCalls.push({
            serviceGroupOrAlias,
            context,
            operation,
            payload,
            metadata,
            auth,
            timeout,
        });

        return {
            id: genID(),
            service: this.name,
            messageType: 'response',
            context,
            operation,
            timestamp: (new Date()).toISOString(),
            payload: { test: true } as ReturnPayloadType,
            status: 'succeeded',
            messages: [],
        };
    }

    public async discoverServiceGroups() : Promise<DiscoveredServiceGroups>
    {
        return {};
    }

    async teardown() : Promise<void>
    {
        this.teardownCalled = false;
    }
}

//----------------------------------------------------------------------------------------------------------------------
