// ---------------------------------------------------------------------------------------------------------------------
// Mock a function
// ---------------------------------------------------------------------------------------------------------------------

export function mockMember(obj : any, property : string, value : any) : void
{
    obj.$$mocked = obj.$$mocked ?? {};
    obj.$$mocked[property] = obj[property];

    obj[property] = value;
}

export function unmockMember(obj : any, property : string) : void
{
    if(obj.$$mocked && obj.$$mocked[property])
    {
        obj[property] = obj.$$mocked[property];
        // eslint-disable-next-line @typescript-eslint/no-dynamic-delete
        delete obj.$$mocked[property];
    }

    if(obj.$$mocked && Object.keys(obj.$$mocked).length === 0)
    {
        delete obj.$$mocked;
    }
}

// ---------------------------------------------------------------------------------------------------------------------
