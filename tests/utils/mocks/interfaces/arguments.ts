//----------------------------------------------------------------------------------------------------------------------
// Argument Mock Interfaces
//----------------------------------------------------------------------------------------------------------------------

import { ValidCommandName } from '../../../../src/interfaces/command.js';

//----------------------------------------------------------------------------------------------------------------------

export interface CommandArguments
{
    command : ValidCommandName;
    target : string;
    payload ?: Record<string, unknown>;
}

export interface PostArguments
{
    serviceGroupOrAlias : string;
    context : string;
    operation : string;
    payload : Record<string, unknown>;
    metadata ?: Record<string, unknown>;
    auth ?: string;
}

export interface RequestArguments
{
    serviceGroupOrAlias : string;
    context : string;
    operation : string;
    payload : Record<string, unknown>;
    metadata ?: Record<string, unknown>;
    auth ?: string;
    timeout ?: number;
}

//----------------------------------------------------------------------------------------------------------------------
