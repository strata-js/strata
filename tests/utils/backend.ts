//----------------------------------------------------------------------------------------------------------------------
// Test Backend
//----------------------------------------------------------------------------------------------------------------------

import { logging } from '@strata-js/util-logging';

// Interface
import { BackendConfig } from '../../src/interfaces/config.js';

// Resource-Access
import { BaseStrataBackend } from '../../src/resource-access/backends/base.js';
import {
    ConcurrencyCheck,
    DiscoveredServices,
    KnownServiceCommand,
    PostEnvelope,
    RequestEnvelope,
    ResponseEnvelope,
    ServiceCommandResponse,
    StrataService,
} from '../../src/index.js';
import { AlreadyInitializedError } from '../../src/lib/errors.js';
import { StrataRequest } from '../../src/lib/request.js';

//----------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('nullBackend');

//----------------------------------------------------------------------------------------------------------------------

export interface TestBackendOptions extends BackendConfig
{
    type : 'test';
}

//----------------------------------------------------------------------------------------------------------------------

export class TestBackend extends BaseStrataBackend<TestBackendOptions>
{
    #config : TestBackendOptions = { type: 'test' };

    #initialized = false;
    listeningForCommands = false;
    listeningForCommandResponses = false;
    listeningForRequests = false;
    listeningForResponses = false;
    discoveryEnabled = false;
    listServicesCalled = false;
    teardownCalled = false;

    // Set this to control the return value of listServices().
    discoveredServices : DiscoveredServices = {};

    command : KnownServiceCommand | undefined;
    commandResponse : ServiceCommandResponse | undefined;
    request : RequestEnvelope | PostEnvelope | undefined;
    response : ResponseEnvelope | undefined;

    get initialized() : boolean { return this.#initialized; }

    async init(config : TestBackendOptions | undefined) : Promise<void>
    {
        if(!this.initialized)
        {
            this.#config = config ?? this.#config;
            this.listeningForCommands = false;
            this.listeningForCommandResponses = false;
            this.listeningForRequests = false;
            this.listeningForResponses = false;
            this.discoveryEnabled = false;
            this.listServicesCalled = false;
            this.teardownCalled = false;
            this.#initialized = true;

            logger.info('Test Backend initialized.');
        }
        else
        {
            throw new AlreadyInitializedError('TestBackend');
        }
    }

    async disableDiscovery() : Promise<void>
    {
        this.discoveryEnabled = false;
        logger.trace('disableDiscovery() called.');
    }

    async enableDiscovery(service : StrataService) : Promise<void>
    {
        this.discoveryEnabled = true;
        logger.trace(`enableDiscovery('<Service:${ service.serviceGroup }', '${ service.id }') called.`);
    }

    async listServices() : Promise<DiscoveredServices>
    {
        this.listServicesCalled = true;
        logger.trace('listServices() called.');
        return this.discoveredServices;
    }

    async listenForCommands(serviceGroup : string, serviceID : string) : Promise<void>
    {
        this.listeningForCommands = true;
        logger.trace(`listenForCommands('${ serviceGroup }', '${ serviceID }') called.`);
    }

    async listenForRequests(
        serviceGroup : string,
        serviceID : string,
        _waterMarkCheck : ConcurrencyCheck
    ) : Promise<void>
    {
        this.listeningForRequests = true;
        logger.trace(`listenForRequests('${ serviceGroup }', '${ serviceID }', <waterMarkCheck>)`);
    }

    async listenForResponses(clientID : string) : Promise<void>
    {
        this.listeningForResponses = true;
        logger.trace(`listenForResponses('${ clientID }', <handler>)`);
    }

    async listenForCommandResponses(clientID : string) : Promise<void>
    {
        this.listeningForCommandResponses = true;
        logger.trace(`listenForCommandResponses('${ clientID }') called.`);
    }

    async sendCommand(target : string, command : KnownServiceCommand) : Promise<void>
    {
        this.command = command;
        logger.trace(`sendCommand('${ target }', '${ JSON.stringify(command, null, 4) }') called.`);
    }

    async sendCommandResponse(_target : string, envelope : ServiceCommandResponse) : Promise<void>
    {
        this.commandResponse = envelope;
        logger.trace(`sendCommandResponse('${ JSON.stringify(envelope, null, 4) }') called.`);
    }

    async sendRequest(serviceGroup : string, envelope : RequestEnvelope | PostEnvelope) : Promise<void>
    {
        this.request = envelope;
        logger.trace(`sendRequest('${ serviceGroup }', '${ JSON.stringify(envelope, null, 4) }') called.`);
    }

    async sendResponse(request : StrataRequest) : Promise<void>
    {
        this.response = request.response ? request.renderResponse() : undefined;
        logger.trace(`sendResponse('${ request.responseQueue }', `
            + `${ JSON.stringify(request.renderRequest(), null, 4) }') called.`);
    }

    async stopListeningForCommands() : Promise<void>
    {
        this.listeningForCommands = false;
        logger.trace('stopListeningForCommands() called.');
    }

    async stopListeningForCommandResponses() : Promise<void>
    {
        this.listeningForCommandResponses = false;
        logger.trace('stopListeningForCommandResponses() called.');
    }

    async stopListeningForRequests(serviceGroup : string) : Promise<void>
    {
        this.listeningForRequests = false;
        logger.trace(`stopListeningForRequests('${ serviceGroup }') called.`);
    }

    async stopListeningForResponses() : Promise<void>
    {
        this.listeningForResponses = false;
        logger.trace('stopListeningForResponses() called.');
    }

    async teardown() : Promise<void>
    {
        this.listeningForCommands = false;
        this.listeningForRequests = false;
        this.listeningForResponses = false;
        this.discoveryEnabled = false;

        this.teardownCalled = true;

        logger.trace('teardown() called.');
    }
}

//----------------------------------------------------------------------------------------------------------------------
