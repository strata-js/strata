// ---------------------------------------------------------------------------------------------------------------------
// Middleware
// ---------------------------------------------------------------------------------------------------------------------

import { OperationMiddleware } from '../../src/index.js';
import { StrataRequest } from '../../src/lib/request.js';

// ---------------------------------------------------------------------------------------------------------------------

export interface TestMiddlewareOptions 
{
    beforeFn ?: () => any;
    successFn ?: () => any;
    failureFn ?: () => any;
}

// ---------------------------------------------------------------------------------------------------------------------

export class TestMiddleware implements OperationMiddleware
{
    #name = 'TestMiddleware';
    #beforeCalled = 0;
    #successCalled = 0;
    #failureCalled = 0;
    #beforeFn ?: () => any;
    #successFn ?: () => any;
    #failureFn ?: () => any;

    get beforeCalled() : number { return this.#beforeCalled; }
    get successCalled() : number { return this.#successCalled; }
    get failureCalled() : number { return this.#failureCalled; }

    constructor(name ?: string, options : TestMiddlewareOptions = {})
    {
        this.#name = name ?? this.#name;
        this.#beforeFn = options.beforeFn;
        this.#successFn = options.successFn;
        this.#failureFn = options.failureFn;
    }

    async beforeRequest(req : StrataRequest) : Promise<StrataRequest | undefined>
    {
        // Increment before called.
        this.#beforeCalled++;

        // Run function, if exists
        if(this.#beforeFn)
        {
            await this.#beforeFn();
        }

        return req;
    }

    async success(req : StrataRequest) : Promise<StrataRequest | undefined>
    {
        // Increment before called.
        this.#successCalled++;

        // Run function, if exists
        if(this.#successFn)
        {
            await this.#successFn();
        }

        return req;
    }

    async failure(req : StrataRequest) : Promise<StrataRequest | undefined>
    {
        // Increment before called.
        this.#failureCalled++;

        // Run function, if exists
        if(this.#failureFn)
        {
            await this.#failureFn();
        }

        return req;
    }
}

// ---------------------------------------------------------------------------------------------------------------------
