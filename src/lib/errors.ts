// ---------------------------------------------------------------------------------------------------------------------
// Custom Errors
// ---------------------------------------------------------------------------------------------------------------------

import { StrataRequest } from './request.js';
import { ResponseEnvelope } from '../interfaces/messages.js';

// ---------------------------------------------------------------------------------------------------------------------

export class ServiceError extends Error
{
    public code = 'SERVICE_ERROR';
    public isSafeMessage = true;

    constructor(message : string, code ?: string)
    {
        super(message);
        if(code)
        {
            this.code = code;
        }
    }

    public toJSON() : Record<string, unknown>
    {
        return {
            name: this.constructor.name,
            isSafeMessage: this.isSafeMessage,
            message: this.message,
            code: this.code,
            stack: this.stack,
        };
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export class UnknownConstructorError extends ServiceError
{
    public code = 'UNKNOWN_CONSTRUCTOR_ERROR';

    constructor(name ?: string)
    {
        super(`Missing or unregistered constructor${ name ? ` '${ name }'` : '' }.`);
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export class UnknownContextError extends ServiceError
{
    public code = 'UNKNOWN_CONTEXT_ERROR';

    constructor(context ?: string)
    {
        super(`Missing or unrecognized context${ context ? ` '${ context }'` : '' }.`);
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export class UnknownOperationError extends ServiceError
{
    public code = 'UNKNOWN_OPERATION_ERROR';

    constructor(operation ?: string)
    {
        super(`Missing or unrecognized operation${ operation ? ` '${ operation }'` : '' }.`);
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export class UnknownBackendError extends ServiceError
{
    public code = 'UNKNOWN_BACKEND_ERROR';

    constructor(backend ?: string)
    {
        super(`Missing or unrecognized backend${ backend ? ` '${ backend }'` : '' }.`);
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export class AlreadyRegisteredError extends ServiceError
{
    public code = 'ALREADY_REGISTERED_ERROR';

    constructor(thing : string, type : string)
    {
        super(`The ${ type } '${ thing }' has already been registered.`);
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export class FailedRequestError<P, M, R> extends ServiceError
{
    public code = 'FAILED_REQUEST';
    public details ?: unknown;

    #name ?: unknown;
    #response : ResponseEnvelope<R>;

    constructor(request : StrataRequest<P, M, R>)
    {
        super((request.response && (request.response['message'] as string)) ?? 'Request failed.');
        this.#response = request.renderResponse();

        if(request.response)
        {
            // Populate details, if it exists.
            this.details = (request.response as any).details;
            // Populate code if it exists.
            this.code = (request.response as any).code ?? this.code;
            // Populate isSafeMessage if it exists, otherwise set to false.
            this.isSafeMessage = (request.response as any).isSafeMessage ?? false;
            // Set name locally if it exists instead of setting from constructor.
            this.#name = (request.response as any).name;
        }
    }

    public toJSON() : Record<string, unknown>
    {
        return {
            ...super.toJSON(),
            name: this.#name ?? this.constructor.name,
            response: this.#response,
            details: this.details,
        };
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export class InternalTimeoutError extends ServiceError
{
    public code = 'INTERNAL_TIMEOUT_ERROR';

    constructor(maxTime : number)
    {
        super(`Operation timed out after ${ maxTime }ms.`);
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export class RequestTimeoutError<P, M, R> extends ServiceError
{
    public code = 'REQUEST_TIMEOUT_ERROR';
    readonly request : StrataRequest<P, M, R>;

    constructor(request : StrataRequest<P, M, R>)
    {
        super(`Request '${ request.id }' timed out.`);
        this.request = request;
    }

    public toJSON() : Record<string, unknown>
    {
        return {
            ...super.toJSON(),
            request: this.request.renderRequest(),
        };
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export class AlreadyInitializedError extends ServiceError
{
    public code = 'ALREADY_INITIALIZED_ERROR';

    constructor(attemptedAction = 'Initialization', type : 'service' | 'client' = 'service')
    {
        super(`${ attemptedAction } failed: ${ type } already initialized!`);
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export class UninitializedError extends ServiceError
{
    public code = 'UNINITIALIZED_ERROR';

    constructor(attemptedAction : string, type = 'service')
    {
        super(`Attempted to ${ attemptedAction } without initializing the ${ type }!`);
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export class MissingRequiredConfigError extends ServiceError
{
    public code = 'MISSING_REQUIRED_CONFIG_ERROR';

    constructor(missingKey : string)
    {
        super(`Missing required configuration key '${ missingKey }'!`);
    }
}

// ---------------------------------------------------------------------------------------------------------------------
