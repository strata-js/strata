//----------------------------------------------------------------------------------------------------------------------
// StrataRequest
//----------------------------------------------------------------------------------------------------------------------

import { logging } from '@strata-js/util-logging';

// Interfaces
import {
    RequestEnvelope,
    RequestOrPostEnvelope,
    ResponseEnvelope,
    ResponseMessage,
} from '../interfaces/messages.js';
import { IsValidPayloadType } from '../interfaces/context.js';

// Engines
import msgFormatEngine from '../engines/envelope.js';

// Utils
import { genID } from '../utils/id.js';
import { Optional } from '../utils/types.js';

// Errors
import { ServiceError } from './errors.js';

//----------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('strataRequest');

type RequestConstructor<P, M> = Optional<RequestOrPostEnvelope<P, M>, 'id' | 'timestamp'>;

//----------------------------------------------------------------------------------------------------------------------

export class StrataRequest<
    PayloadType = Record<string, unknown>,
    MetadataType = Record<string, unknown>,
    ResponsePayloadType = Record<string, unknown>>
{
    // Shared Properties
    readonly id : string;
    readonly context : string;
    readonly operation : string;

    // Request Properties
    readonly responseQueue ?: string;
    readonly receivedTimestamp = Date.now();
    readonly timestamp : string;
    readonly timeout ?: number;
    readonly payload : IsValidPayloadType<PayloadType>;
    readonly metadata : MetadataType;
    readonly auth ?: string;
    readonly client : string;
    readonly priorRequest ?: string;
    readonly requestChain : string[] = [];

    // Response Properties
    response ?: IsValidPayloadType<ResponsePayloadType> | Record<string, unknown>;
    completedTimestamp ?: number;
    status : 'succeeded' | 'failed' | 'pending' = 'pending';
    service ?: string;
    readonly messages : ResponseMessage[] = [];

    // Internal Properties
    readonly messageType : 'request' | 'post';
    readonly promise : Promise<void>;
    #resolve : (value ?: void | PromiseLike<void>) => void = () => undefined;

    //------------------------------------------------------------------------------------------------------------------

    constructor(request : RequestConstructor<IsValidPayloadType<PayloadType>, MetadataType>, serviceString ?: string)
    {
        this.id = request.id ?? genID();
        this.context = request.context;
        this.operation = request.operation;
        this.timestamp = request.timestamp ?? (new Date()).toISOString();
        this.payload = request.payload;
        this.metadata = request.metadata;
        this.auth = request.auth;
        this.client = request.client ?? 'unknown';
        this.priorRequest = request.priorRequest;
        this.messageType = request.messageType;

        if(request.messageType === 'request')
        {
            this.responseQueue = request.responseQueue;
            this.timeout = request.timeout;
        }

        // requestChain must be cloned, or it will continue to be added to for every request that a service/client makes
        this.requestChain = [ ...(request.requestChain ?? []) ];

        // check if this request is already there, because the request gets constructed on both sides of the wire
        if(this.requestChain[this.requestChain.length - 1] !== this.id)
        {
            this.requestChain.push(this.id);
        }

        // Specifying this allows us to name the service, without depending on the service code.
        this.service = serviceString;

        // Build a new promise to track when this is succeeded or failed.
        this.promise = new Promise((resolve) =>
        {
            this.#resolve = resolve;
        });
    }

    //------------------------------------------------------------------------------------------------------------------

    public parseResponse(response : ResponseEnvelope<IsValidPayloadType<ResponsePayloadType>>) : void
    {
        this.completedTimestamp = (new Date(response.timestamp)).getTime();
        this.service = response.service;

        // Populate messages array
        response.messages.forEach((msg) => this.messages?.push(msg));

        if(response.status === 'failed')
        {
            this.fail(response.payload);
        }
        else
        {
            this.succeed(response.payload);
        }
    }

    /**
     * Renders the on-the-wire format for the request envelope.
     *
     * @returns Returns a plain object version of the request envelope.
     */
    public renderRequest() : RequestEnvelope<PayloadType, MetadataType>
    {
        return msgFormatEngine.buildRequest<PayloadType, MetadataType>({
            id: this.id,
            context: this.context,
            operation: this.operation,
            responseQueue: this.responseQueue,
            timestamp: this.receivedTimestamp,
            payload: this.payload as PayloadType,
            metadata: this.metadata as MetadataType,
            auth: this.auth,
            priorRequest: this.priorRequest,
            requestChain: this.requestChain,
            client: this.client,
        });
    }

    /**
     * Renders the on-the-wire format for the response envelope.
     *
     * @returns Returns a plain object version of the response envelope.
     */
    public renderResponse() : ResponseEnvelope<ResponsePayloadType>
    {
        return msgFormatEngine.buildResponse({
            id: this.id,
            context: this.context,
            operation: this.operation,
            timestamp: this.completedTimestamp,
            payload: this.response as ResponsePayloadType | undefined,
            status: this.status,
            messages: this.messages,
            service: this.service,
        });
    }

    /**
     * Succeeds this request.
     *
     * @param payload
     */
    public succeed(payload : IsValidPayloadType<ResponsePayloadType>) : void
    {
        this.response = payload;
        this.status = 'succeeded';
        this.completedTimestamp = this.completedTimestamp ?? Date.now();

        // Resolve promise
        this.#resolve();
    }

    public fail(reason : string | Error | IsValidPayloadType<ResponsePayloadType>) : void
    {
        if(reason instanceof ServiceError)
        {
            this.response = reason.toJSON();
        }
        else if(reason instanceof Error)
        {
            this.response = {
                message: reason.message,
                name: 'FailedRequestError',
                stack: reason.stack,
                code: reason['code'] ?? 'FAILED_REQUEST',
                details: reason['details'],
                isSafeMessage: reason['isSafeMessage'] ?? false,
            };
        }
        else if(typeof reason === 'string')
        {
            this.response = {
                message: reason,
                name: 'FailedRequestError',
                code: 'FAILED_REQUEST',
            };
        }
        else
        {
            this.response = reason;
        }

        if(this.messageType === 'post')
        {
            logger.error(`Failed post request '${ this.id }':`, JSON.stringify(this.response));
        }

        this.status = 'failed';
        this.completedTimestamp = this.completedTimestamp ?? Date.now();

        // Resolve promise
        this.#resolve();
    }
}

//----------------------------------------------------------------------------------------------------------------------
