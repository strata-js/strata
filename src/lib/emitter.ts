//----------------------------------------------------------------------------------------------------------------------
// TypedEmitter
//----------------------------------------------------------------------------------------------------------------------

import { EventEmitter } from 'events';

//----------------------------------------------------------------------------------------------------------------------

type ListenerFn = (...args : any) => void;

type ExtractEventParams<T> = T extends (...args : infer Params) => void ? Params : [T];

/**
 * This is a specialized TypedEmitter that is a stripped down to just the essentials, and also allows the specification
 * of an "event map" that limits the events and their listeners to just the types specified. This allows us to define
 * events as part of the class, and it gives proper Typescript autocomplete and compilation errors if misused.
 *
 * A basic usage example is as follows:
 *
 * @example
 * interface MyEvents {
 *   userLoggedIn: (username: string) => void;
 *   messageReceived: string;
 * }
 *
 * // Create an instance of TypedEmitter
 * const myEventEmitter = new TypedEmitter<MyEvents>();
 *
 * // Define event listeners
 * myEventEmitter.on('userLoggedIn', (username) => {
 *   console.log(`User logged in: ${username}`);
 * });
 *
 * myEventEmitter.on('messageReceived', (content) => {
 *   console.log(`Message received: ${content}`);
 * });
 *
 * // Emit events with correct parameters
 * myEventEmitter.emit('userLoggedIn', 'john_doe');
 * myEventEmitter.emit('messageReceived', 'Hello');
 *
 * // TypeScript enforces correct parameter types
 * myEventEmitter.emit('userLoggedIn', 42); // This will result in a TypeScript error
 * myEventEmitter.emit('messageReceived', 123); // This will also result in a TypeScript error
 *
 */
export class TypedEmitter<Events extends Record<string, any>>
{
    readonly #eventEmitter = new EventEmitter();

    constructor()
    {
        this.#eventEmitter.setMaxListeners(Infinity);
    }

    on<K extends keyof Events>(eventName : K, listener : (...args : ExtractEventParams<Events[K]>) => void) : void
    {
        this.#eventEmitter.on(eventName as string, listener as ListenerFn);
    }

    off<K extends keyof Events>(eventName : K, listener : (...args : ExtractEventParams<Events[K]>) => void) : void
    {
        this.#eventEmitter.off(eventName as string, listener as ListenerFn);
    }

    emit<K extends keyof Events>(eventName : K, ...args : Parameters<Events[K]>) : void
    {
        this.#eventEmitter.emit(eventName as string, ...args);
    }
}
//----------------------------------------------------------------------------------------------------------------------
