//----------------------------------------------------------------------------------------------------------------------
// Deferred Request
//----------------------------------------------------------------------------------------------------------------------

/**
 * A deferred is basically a promise that can be resolved or rejected from outside the promise. This is useful for
 * situations like unit testings, where you need to make something wait until you're ready to continue the promise
 * chain.
 */
export class Deferred
{
    #resolve : (value : any) => void;
    #reject : (reason : any) => void;

    promise : Promise<any>;

    constructor()
    {
        this.#resolve = () =>
        {
            throw new Error('Deferred resolved before constructor completed.');
        };

        this.#reject = () =>
        {
            throw new Error('Deferred rejected before constructor completed.');
        };

        this.promise = new Promise((resolve, reject) =>
        {
            this.#resolve = resolve;
            this.#reject = reject;
        });
    }

    /**
     * Resolve the request with the given response.
     */
    resolve(response : any) : void
    {
        this.#resolve(response);
    }

    /**
     * Reject the request with the given error.
     */
    reject(error : any) : void
    {
        this.#reject(error);
    }
}

/**
 * Represents a request that has a deferred response. This object holds on to the initial request and once a response
 * comes in, it completes the request, resolving with the response.
 */
export class DeferredRequest<RequestObject = any, ResponseObject = any> extends Deferred
{
    #resolve : (value : ResponseObject) => void;
    #reject : (reason : any) => void;

    request : RequestObject;
    declare promise : Promise<ResponseObject>;

    constructor(request : RequestObject)
    {
        super();

        this.#resolve = () =>
        {
            throw new Error('DeferredRequest resolved before constructor completed.');
        };

        this.#reject = () =>
        {
            throw new Error('DeferredRequest rejected before constructor completed.');
        };

        this.request = request;
        this.promise = new Promise((resolve, reject) =>
        {
            this.#resolve = resolve;
            this.#reject = reject;
        });
    }

    /**
     * Resolve the request with the given response.
     */
    resolve(response : ResponseObject) : void
    {
        this.#resolve(response);
    }

    /**
     * Reject the request with the given error.
     */
    reject(error : any) : void
    {
        this.#reject(error);
    }
}

//----------------------------------------------------------------------------------------------------------------------
