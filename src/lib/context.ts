//----------------------------------------------------------------------------------------------------------------------
// StrataContext
//----------------------------------------------------------------------------------------------------------------------

import ld from 'lodash';

// Interfaces
import { OperationHandler } from '../interfaces/context.js';
import { OperationMiddleware } from '../interfaces/middleware.js';

// Message
import { StrataRequest } from './request.js';

// Errors
import { AlreadyRegisteredError, UnknownOperationError } from './errors.js';

//----------------------------------------------------------------------------------------------------------------------

interface OperationConfig
{
    opFunc : OperationHandler<any>,
    opFuncParams : string[],
    middleware : OperationMiddleware[]
}

type OperationMiddlewareIterable = Set<OperationMiddleware> | OperationMiddleware[];

//----------------------------------------------------------------------------------------------------------------------

export class StrataContext
{
    readonly #operations = new Map<string, OperationConfig>();
    readonly #middleware = new Set<OperationMiddleware>();

    //------------------------------------------------------------------------------------------------------------------

    private async $callBeforeMiddleware(
        middlewareList : OperationMiddlewareIterable,
        request : StrataRequest
    ) : Promise<StrataRequest>
    {
        for(const middleware of middlewareList)
        {
            // Support ending calls early
            if(request.status !== 'pending')
            {
                break;
            }

            // eslint-disable-next-line no-await-in-loop
            request = (await middleware?.beforeRequest(request)) ?? request;
        }

        return request;
    }

    private async $callSuccessMiddleware(
        middlewareList : OperationMiddlewareIterable,
        request : StrataRequest
    ) : Promise<StrataRequest>
    {
        for(const middleware of middlewareList)
        {
            if(typeof middleware.success === 'function')
            {
                // eslint-disable-next-line no-await-in-loop
                request = (await middleware.success(request)) ?? request;
            }
        }

        return request;
    }

    private async $callFailureMiddleware(
        middlewareList : OperationMiddlewareIterable,
        request : StrataRequest
    ) : Promise<StrataRequest>
    {
        for(const middleware of middlewareList)
        {
            if(typeof middleware.failure === 'function')
            {
                // eslint-disable-next-line no-await-in-loop
                request = (await middleware.failure(request)) ?? request;
            }
        }

        return request;
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Calls an operation, and any middleware configured for the operation.
     *
     * The logic for this function is as follows:
     *
     * 1. Iterated over every configured piece of middleware, and call `middleware.beforeRequest`.
     *   * If the middleware throws an error, or succeeds/fails the request, we break out and return the request.
     * 2. Call the operation function
     *   * If the operation function completes, we check to see if it's succeeded or failed the request.
     *     * If it succeeds the request, we call `middleware.success` for all configured middleware.
     *     * If it fails the request, we call `middleware.failure` for all configured middleware.
     *     * If it hasn't done either, we succeed the request with the return value of the operation function, and
     *          then call `middleware.success` for all configured middleware.
     *   * If the operation throws we fail the request and then call `middleware.failure` for all configured middleware.
     *
     * _Note: Middleware is always called in the order of context configured middleware, and then operation configured
     * middleware._
     *
     * @param opName - The name of the operation to call.
     * @param request - The service request received.
     *
     * @returns Returns a succeeded or failed request. (In the event of an error, it may throw, in which case the
     * caller is expected to handle succeeding or failing the request.)
     */
    public async $callOperation(opName : string, request : StrataRequest) : Promise<StrataRequest>
    {
        const opConfig = this.#operations.get(opName);
        if(!opConfig)
        {
            throw new UnknownOperationError(opName);
        }

        // Call the context level before middleware
        request = await this.$callBeforeMiddleware(this.#middleware, request);

        // Call the operation configured middleware
        request = await this.$callBeforeMiddleware(opConfig.middleware, request);

        //--------------------------------------------------------------------------------------------------------------

        const { opFuncParams } = opConfig;

        // Extract the properties requested by the opFuncParams and pass those through to the opFunc method
        let opFuncParam : any[] = [ request ];
        if(opFuncParams.length > 0)
        {
            opFuncParam = opFuncParams.map((param) =>
            {
                return ld.get(request, param);
            });
        }

        // Middleware is allowed to succeed or fail the request early, without calling the operation function.
        if(request.status === 'pending')
        {
            // Call operation
            const response = await opConfig.opFunc(...opFuncParam)
                .catch(async(error) =>
                {
                    request.fail(error);
                });

            if(request.status === 'pending')
            {
                request.succeed(response);
            }
        }

        if(request.status === 'succeeded')
        {
            request = await this.$callSuccessMiddleware(opConfig.middleware, request);
            request = await this.$callSuccessMiddleware(this.#middleware, request);
        }
        else if(request.status === 'failed')
        {
            request = await this.$callFailureMiddleware(opConfig.middleware, request);
            request = await this.$callFailureMiddleware(this.#middleware, request);
        }

        return request;
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Declares a function to be an exposed operation. The function will be called with a [[StrataRequest]] representing
     * the pending request, and is expected to return a response, or to call [[StrataRequest.success]] or
     * [[StrataRequest.fail]] itself.
     *
     * @param opName - The name we want the function to be exposed under.
     * @param opFunc - The actual function to expose.
     * @param opFuncParams - An optional array of property names to extract from the request object when making the call
     * to the underlying method. The default is an empty array; if so the entire request object is passed through.
     * ie. [ 'payload' ] or [ 'payload.someProp1', 'payload.someProp2', 'auth.user' ]
     *   * each property value will be passed through as a separate parameter to the opFunc
     * @param middleware - An optional list of middleware to configure for this function.
     */
    public registerOperation<ReturnPayloadType>(
        opName : string,
        opFunc : OperationHandler<ReturnPayloadType>,
        opFuncParams : string[] = [],
        middleware : OperationMiddleware[] = []
    ) : void
    {
        if(this.#operations.has(opName))
        {
            throw new AlreadyRegisteredError(opName, 'operation');
        }

        this.#operations.set(opName, { opFunc, opFuncParams, middleware });
    }

    /**
     * Exposes all methods of an instance, that meet the specified criteria, as operations (all methods that begin
     * with underscore (_) or dollar sign ($) will automatically be excluded). Operations that are exposed with this
     * method will be simple pass-through methods. The operation names will match the method names of the instance.
     * Middleware should be used to handle any required functionality before and/or after the method call.
     *
     * @param instance - An instance that will have the methods exposed as operations.
     * @param opFuncParams - An optional array of property names to extract from the request object when making the call
     * to the underlying method. The default is an empty array, if so the entire request object is passed through.
     * ie. [ 'payload' ] or [ 'payload.someProp1', 'payload.someProp2', 'auth.user' ]
     *   * each property value will be passed through as a separate parameter to the opFunc
     *   (which is the instance method in this case)
     * @param middleware - An optional list of middleware to configure for all methods exposed as operations.
     * @param opModifiers - An optional object to include/exclude specific methods of the instance. Items in the
     * exclude list will take precedence over the include list.
     */
    public registerOperationsForInstance<T>(
        instance : T,
        opFuncParams : string[] = [],
        middleware : OperationMiddleware[] = [],
        opModifiers : { include ?: string[], exclude ?: string[] } = { include: [], exclude: [] }
    ) : void
    {
        let instancePrototype = Object.getPrototypeOf(instance);
        if(instancePrototype === Object.prototype)
        {
            instancePrototype = instance;
        }

        // Loop over all properties of the class instance
        Object.getOwnPropertyNames(instancePrototype)
            .forEach((name) =>
            {
                const descriptor = Object.getOwnPropertyDescriptor(instancePrototype, name);
                if(!descriptor) { return; }

                // Only handle the instance property if it is a function, is not the constructor, and is not 'private'.
                if(typeof descriptor.value != 'function'
                    || name == 'constructor'
                    || name.startsWith('_')
                    || name.startsWith('$')
                )
                {
                    return;
                }

                // Map the method to an operation if it meets the specified criteria.
                if(((opModifiers.include || []).length === 0 || opModifiers.include?.includes(name))
                    && ((opModifiers.exclude || []).length === 0 || !opModifiers.exclude?.includes(name)))
                {
                    this.registerOperation(name, async(...params) =>
                    {
                        return instance[name](...params);
                    }, opFuncParams, middleware);
                }
            });
    }

    /**
     * Registers a piece (or pieces) of middleware to be used for all calls on this context.
     *
     * @param middleware - The middleware to register.
     */
    public useMiddleware(middleware : OperationMiddleware | OperationMiddleware[]) : void
    {
        if(!Array.isArray(middleware))
        {
            middleware = [ middleware ];
        }

        middleware.forEach((mw) =>
        {
            this.#middleware.add(mw);
        });
    }

    /**
     * Gets a list of valid operation names.
     *
     * @returns Returns a list of all the valid operations, by name.
     */
    public getOperationNames() : string[]
    {
        return Array.from(this.#operations.keys());
    }

    /**
     * Gets a list of registered middlewares for the context.
     *
     * @returns Returns a list of all the middlewares for the context.
     */
    public get contextMiddlewareList() : OperationMiddleware[]
    {
        return [ ...this.#middleware.values() ];
    }

    /**
     * Gets a list of registered middlewares for the operations in this context.
     *
     * @returns Returns a list of all the middlewares for the operations.
     */
    public get operationsMiddlewareList() : OperationMiddleware[]
    {
        return ld.uniq([ ...this.#operations.values() ].flatMap((op) => op.middleware));
    }
}

//----------------------------------------------------------------------------------------------------------------------
