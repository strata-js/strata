//----------------------------------------------------------------------------------------------------------------------
// Request Engine
//----------------------------------------------------------------------------------------------------------------------

import { StrataService } from '../clients/service.js';
import { StrataRequest } from '../lib/request.js';

//----------------------------------------------------------------------------------------------------------------------

/* eslint-disable no-await-in-loop */

export class RequestEngine
{
    async callBeforeMiddleware(service : StrataService, request : StrataRequest) : Promise<StrataRequest>
    {
        for(const middleware of service.middleware)
        {
            // Support ending calls early
            if(request.status !== 'pending')
            {
                break;
            }

            if(typeof middleware.beforeRequest === 'function')
            {
                request = (await middleware.beforeRequest(request)) ?? request;
            }
        }

        return request;
    }

    async callSuccessMiddleware(service : StrataService, request : StrataRequest) : Promise<StrataRequest>
    {
        for(const middleware of service.middleware)
        {
            if(typeof middleware.success === 'function')
            {
                request = (await middleware.success(request)) ?? request;
            }
        }

        return request;
    }

    async callFailureMiddleware(service : StrataService, request : StrataRequest) : Promise<StrataRequest>
    {
        for(const middleware of service.middleware)
        {
            if(typeof middleware.failure === 'function')
            {
                request = (await middleware.failure(request)) ?? request;
            }
        }

        return request;
    }
}

/* eslint-enable no-await-in-loop */

//----------------------------------------------------------------------------------------------------------------------
