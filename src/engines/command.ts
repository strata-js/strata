//----------------------------------------------------------------------------------------------------------------------
// CommandEngine
//----------------------------------------------------------------------------------------------------------------------

import { logging } from '@strata-js/util-logging';

// Interfaces
import {
    KnownServiceCommand,
    ServiceCommandResponse,
    StrataConcurrencyCommandPayload,
    StrataShutdownCommandPayload,
    StrataTooBusyCommandPayload,
} from '../interfaces/command.js';

// Utils
import shutdownUtil from '../utils/shutdown.js';
import { ServiceInfo } from '../interfaces/service.js';
import { StrataService } from '../clients/service.js';

//----------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('commandEngine');

//----------------------------------------------------------------------------------------------------------------------

export class CommandEngine
{
    private async _handleInfoCommand(service : StrataService) : Promise<ServiceInfo>
    {
        // Generate info payload
        return service.info;
    }

    private async _handleConcurrencyCommand(
        service : StrataService,
        payload : StrataConcurrencyCommandPayload
    ) : Promise<void>
    {
        // `concurrency` must be defined, must be a number and must be greater than -1.
        if(!payload.concurrency || !Number.isFinite(payload.concurrency) || payload.concurrency <= -1)
        {
            logger.warn('Service received concurrency command with malformed payload:', payload);
            return;
        }

        service.setConcurrency(payload.concurrency);
    }

    private async _handleTooBusyCommand(service : StrataService, payload : StrataTooBusyCommandPayload) : Promise<void>
    {
        service.setTooBusy(payload);
    }

    private async _handleShutdownCommand(
        _service : StrataService,
        payload ?: StrataShutdownCommandPayload
    ) : Promise<void>
    {
        // `exitCode`, if defined, must be a number and must be greater than -1.
        if(payload?.exitCode && (!Number.isFinite(payload.exitCode) || payload?.exitCode < 0))
        {
            logger.warn('Service received shutdown command with malformed payload:', payload);
            return;
        }

        if(payload?.graceful === false)
        {
            logger.warn('Exiting without graceful shutdown.');
            process.exit(payload?.exitCode ?? 0);
        }
        else
        {
            shutdownUtil.gracefulShutdown(payload?.exitCode ?? 0);
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    // Public
    //------------------------------------------------------------------------------------------------------------------

    public async handleCommand(
        service : StrataService,
        envelope : KnownServiceCommand
    ) : Promise<ServiceCommandResponse | undefined>
    {
        let response : any;
        switch (envelope.command)
        {
            case 'info':
                response = await this._handleInfoCommand(service);
                break;

            case 'concurrency':
                response = await this._handleConcurrencyCommand(service, envelope.payload);
                break;

            case 'shutdown':
                response = await this._handleShutdownCommand(service, envelope.payload);
                break;

            case 'toobusy':
                response = await this._handleTooBusyCommand(service, envelope.payload);
                break;

            default:
                logger.warn(`Service '${ service.displayName }(${ service.id })' received unknown command:`, envelope);
                return;
        }

        if(response)
        {
            return {
                id: envelope.id,
                payload: response,
            } satisfies ServiceCommandResponse;
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------
