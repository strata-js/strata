//----------------------------------------------------------------------------------------------------------------------
// Queue Engine
//----------------------------------------------------------------------------------------------------------------------

import { logging } from '@strata-js/util-logging';
import { DeferredRequest } from '../lib/deferred.js';
import { StrataRequest } from '../lib/request.js';
import { FailedRequestError, InternalTimeoutError, RequestTimeoutError } from '../lib/errors.js';

// Interfaces
import { KnownServiceCommand, ServiceCommandResponse, StrataInfoCommand } from '../interfaces/command.js';
import {
    ConcurrencyCheck, PostEnvelope,
    RequestEnvelope,
    ResponseEnvelope,
} from '../interfaces/messages.js';
import { LocalStorage } from '../interfaces/localStorage.js';
import { DiscoveredServices } from '../interfaces/discovery.js';

// Clients
import { StrataClient } from '../clients/client.js';
import { StrataService } from '../clients/service.js';

// Managers
import backendMan from '../managers/backend.js';

// Engines
import { CommandEngine } from './command.js';
import { RequestEngine } from './request.js';
import msgFormatEngine from './envelope.js';

// Resource Access
import { BaseStrataBackend } from '../resource-access/backends/base.js';
import { NullBackend } from '../resource-access/backends/null.js';

// Utils
import localStorageUtil from '../utils/localStorage.js';
import { promiseTimeout } from '../utils/timeout.js';

//----------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('queueEngine');

type OutstandingCommand = DeferredRequest<KnownServiceCommand, ServiceCommandResponse>;

//----------------------------------------------------------------------------------------------------------------------

export class ServiceQueueEngine
{
    readonly #outstandingRequests = new Map<string, StrataRequest<any, any, any>>();
    readonly #service : StrataService;
    readonly #commandEng : CommandEngine;
    readonly #requestEng : RequestEngine;

    // This is a bit of a hack, but it's the easiest way to get the backend to be set in the constructor.
    #backend : BaseStrataBackend<any> = new NullBackend();

    //------------------------------------------------------------------------------------------------------------------
    // Properties
    //------------------------------------------------------------------------------------------------------------------

    get backend() : BaseStrataBackend<any>
    {
        return this.#backend;
    }

    get outstandingRequests() : Map<string, StrataRequest<any, any, any>>
    {
        return this.#outstandingRequests;
    }

    //------------------------------------------------------------------------------------------------------------------

    constructor(service : StrataService)
    {
        this.#service = service;
        this.#commandEng = new CommandEngine();
        this.#requestEng = new RequestEngine();
    }

    //------------------------------------------------------------------------------------------------------------------
    // Event Handlers
    //------------------------------------------------------------------------------------------------------------------

    private async _handleIncomingCommand(envelope : KnownServiceCommand) : Promise<void>
    {
        if(envelope && envelope.command)
        {
            const response = await this.#commandEng.handleCommand(this.#service, envelope);
            if(response)
            {
                const responseChannel = (envelope as StrataInfoCommand).responseChannel;

                // Send the response
                await this.#backend.sendCommandResponse(responseChannel, response);
            }
        }
    }

    private async _handleIncomingRequest(envelope : RequestEnvelope | PostEnvelope) : Promise<void>
    {
        let request = new StrataRequest(envelope, this.#service.serviceGroup);
        const store : LocalStorage = {
            currentRequestID: request.id,
            currentRequest: request,
            messages: [],
        };

        // Track this outstanding request.
        this.#outstandingRequests.set(request.id, request);

        // Run inside the local storage context.
        await localStorageUtil.run(store, async() =>
        {
            try
            {
                // Get the context, and if it's not found, this will throw with an `UnknownContextError`.
                const context = this.#service.getContext(request.context);

                // Handle global before middleware
                request = await this.#requestEng.callBeforeMiddleware(this.#service, request);

                // Call the context handler
                request = await promiseTimeout(
                    context.$callOperation(request.operation, request),
                    request.timeout ?? this.#service.config.service?.defaultRequestTimeout
                );
            }
            catch (error)
            {
                if(error instanceof Error)
                {
                    if(error instanceof InternalTimeoutError)
                    {
                        logger.warn(`Request (${ request.id }) timed out. Failing request.`);
                        const newError = new RequestTimeoutError(request);
                        request.fail(newError);
                    }
                    else
                    {
                        request.fail(error);
                    }
                }
                else
                {
                    request.fail(new Error(`Unknown error: ${ error }`));
                }
            }

            if(request.status === 'succeeded')
            {
                // Handle global middleware success
                request = await this.#requestEng.callSuccessMiddleware(this.#service, request);
            }
            else if(request.status === 'failed')
            {
                // Handle global middleware failure
                request = await this.#requestEng.callFailureMiddleware(this.#service, request);
            }
        })
            .catch((error) =>
            {
                request.fail(error);
                logger.error('Unhandled exception running request handler. Error:', error.stack);
            });

        // Remove this request from our tracking. This needs to be done before we respond, to ensure our tracking is as
        // accurate as possible; since responding is async, it can be dozens or hundreds of milliseconds before this
        // delete is done if we have to wait on the response and ack.
        this.#outstandingRequests.delete(request.id);

        // Response to the request
        await this.respond(request);
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Initialize the queue engine.
     */
    async init() : Promise<void>
    {
        // Get the backend
        this.#backend = backendMan.getInstance(this.#service.config.backend?.type);

        // Bind event handlers
        this.#backend.on('incomingCommand', this._handleIncomingCommand.bind(this));
        this.#backend.on('incomingRequest', this._handleIncomingRequest.bind(this));

        // Initialize the backend
        if(!this.#backend.initialized)
        {
            await this.#backend.init(this.#service.config.backend);
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    // Listening
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Start listening for commands as a member of this service group.
     *
     * @param serviceGroup - The service group this service is a member of.
     * @param serviceID - The unique ID of this service.
     */
    async listenForCommands(serviceGroup : string, serviceID : string) : Promise<void>
    {
        await this.#backend.listenForCommands(serviceGroup, serviceID);
    }

    /**
     * Start listening for incoming requests. The service will process these requests and respond to them, if they
     * require a response.
     *
     * @param serviceGroup - The service group this service is a member of.
     * @param serviceID - The unique ID of this service.
     * @param waterMarkCheck - A function that tells the service if we need to pause pulling requests or not.
     */
    async listenForRequests(
        serviceGroup : string,
        serviceID : string,
        waterMarkCheck : ConcurrencyCheck
    ) : Promise<void>
    {
        await this.#backend.listenForRequests(serviceGroup, serviceID, waterMarkCheck);
    }

    /**
     * Stop listening for commands.
     */
    async stopListeningForCommands() : Promise<void>
    {
        await this.#backend.stopListeningForCommands();
    }

    /**
     * Stop listening for requests.
     *
     * @param serviceGroup - The service group this service is a member of.
     */
    async stopListeningForRequests(serviceGroup : string) : Promise<void>
    {
        await this.#backend.stopListeningForRequests(serviceGroup);
    }

    //------------------------------------------------------------------------------------------------------------------
    // Sending
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Send a response to a request to the client who made the request.
     *
     * @param request - The request to respond to.
     *
     */
    async respond(request : StrataRequest) : Promise<void>
    {
        if(request.status === 'pending')
        {
            request.fail('Service error, attempted to respond while request still pending.');
            logger.warn('Attempting to respond with a pending status! Failing request.');
        }

        // Send the response
        await this.#backend.sendResponse(request);
    }

    //------------------------------------------------------------------------------------------------------------------
    // Discovery
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Allow this service to be discoverable.
     */
    async enableDiscovery() : Promise<void>
    {
        logger.trace(`Enabling discovery for service '${ this.#service.id }'.`);
        await this.#backend.enableDiscovery(this.#service);
    }

    /**
     * Disable discovery for this service.
     */
    async disableDiscovery() : Promise<void>
    {
        logger.trace(`Disabling discovery for service '${ this.#service.id }'.`);
        await this.#backend.disableDiscovery();
    }

    //------------------------------------------------------------------------------------------------------------------
    // Cleanup
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Tear down the queue engine, and clean anything up.
     */
    async teardown() : Promise<void>
    {
        logger.debug(`Tearing down ServiceQueueEngine for service '${ this.#service.id }'.`);
        await this.#backend.teardown();
    }
}

//----------------------------------------------------------------------------------------------------------------------

export class ClientQueueEngine
{
    readonly #outstandingRequests = new Map<string, StrataRequest<any, any, any>>();
    readonly #outstandingCommands = new Map<string, OutstandingCommand>();

    #client : StrataClient;
    #backend : BaseStrataBackend<any> = new NullBackend();

    //------------------------------------------------------------------------------------------------------------------
    // Properties
    //------------------------------------------------------------------------------------------------------------------

    get backend() : BaseStrataBackend<any>
    {
        return this.#backend;
    }

    get outstandingRequests() : Map<string, StrataRequest<any, any, any>>
    {
        return this.#outstandingRequests;
    }

    get outstandingCommands() : Map<string, OutstandingCommand>
    {
        return this.#outstandingCommands;
    }

    //------------------------------------------------------------------------------------------------------------------

    constructor(client : StrataClient)
    {
        this.#client = client;
    }

    //------------------------------------------------------------------------------------------------------------------
    // Event Handlers
    //------------------------------------------------------------------------------------------------------------------

    private _handleIncomingCommandResponse(envelope : ServiceCommandResponse) : void
    {
        // Match the response to it's command
        const command = this.#outstandingCommands.get(envelope.id);
        if(command)
        {
            this.#outstandingCommands.delete(envelope.id);
            command.resolve(envelope);
        }
        else
        {
            logger.warn(`Got response for unknown command '${ envelope.id }'.`);
        }
    }

    private _handleIncomingResponse(envelope : ResponseEnvelope) : void
    {
        // Match the response to it's request
        const request = this.#outstandingRequests.get(envelope.id);
        if(request)
        {
            request.parseResponse(envelope);
        }
        else
        {
            logger.warn(`Got response for unknown request '${ envelope.id }'.`);
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Initialize the queue engine.
     */
    async init() : Promise<void>
    {
        // Get the backend
        this.#backend = backendMan.getInstance(this.#client.config.backend?.type);

        // Bind event handlers
        this.#backend.on('incomingResponse', this._handleIncomingResponse.bind(this));
        this.#backend.on('incomingCommandResponse', this._handleIncomingCommandResponse.bind(this));

        // Initialize the backend
        if(!this.#backend.initialized)
        {
            await this.#backend.init(this.#client.config.backend);
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    // Listening
    //------------------------------------------------------------------------------------------------------------------
    /**
     * Start listening for command responses.
     * @param clientID - The unique ID of the client making the request.
     */
    async listenForCommandResponses(clientID : string) : Promise<void>
    {
        await this.#backend.listenForCommandResponses(clientID);
    }

    /**
     * Start listening for incoming responses. The client will process these and match them up to requests.
     *
     * @param clientID - The unique ID of this service.
     * @param clientName - The name of this client.
     */
    async listenForResponses(clientID : string, clientName ?: string) : Promise<void>
    {
        await this.#backend.listenForResponses(clientID, clientName);
    }

    /**
     * Stop listening for command responses.
     */
    async stopListeningForCommandResponses() : Promise<void>
    {
        await this.#backend.stopListeningForCommandResponses();
    }

    /**
     * Stop listening for responses.
     */
    async stopListeningForResponses() : Promise<void>
    {
        await this.#backend.stopListeningForResponses();
    }

    //------------------------------------------------------------------------------------------------------------------
    // Sending
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Send a service command. The target for the command can be all services (i.e. `Services`), a service group
     * (i.e. `Services:<ServiceGroup>`) or an individual service (i.e. `Services:<ServiceGroup>:<ServiceID>`).
     *
     * @param target - The target for the command. (Of the form `Services:<ServiceGroup>:<ServiceID>`.)
     * @param command - The service command to send.
     */
    async command(target : string, command : KnownServiceCommand) : Promise<ServiceCommandResponse | undefined>
    {
        if(command.command === 'info' && command.responseChannel)
        {
            // Make a new deferred command
            const deferredCommand = new DeferredRequest(command);

            // Track this outstanding command.
            this.#outstandingCommands.set(command.id, deferredCommand);

            // Send the command
            await this.#backend.sendCommand(target, command);

            // Return the deferred command's promise
            return deferredCommand.promise;
        }
        else
        {
            // If we don't have a response channel, we don't need to track this command.
            await this.#backend.sendCommand(target, command);
        }
    }

    /**
     * Sends a service request that does not require a response.
     *
     * @param serviceGroupOrAlias - Attempts to look up this string as an alias name in the config, otherwise passes
     * this * as the serviceGroup.
     * @param context - The context for your new request.
     * @param operation - The operation for your new request.
     * @param payload - The payload of your new request.
     * @param metadata - Any metadata for the request. (This is useful for tracking and statistics.)
     * @param auth - The security token for your request, if required.
     * @param clientString - A string to identify the client making the request.
     *
     */
    public async post<MetadataType extends Record<string, unknown> = Record<string, unknown>>(
        serviceGroupOrAlias : string,
        context : string,
        operation : string,
        payload : Record<string, unknown>,
        metadata ?: MetadataType,
        auth ?: string,
        clientString ?: string
    ) : Promise<void>
    {
        // Resolve the serviceGroup
        const aliasConfig = this.#client.config.aliases ?? [];
        const alias = aliasConfig[serviceGroupOrAlias];
        const queue = alias ?? serviceGroupOrAlias;

        const store = localStorageUtil.getStore();
        metadata = (metadata ?? {}) as MetadataType;

        const post = msgFormatEngine.buildPost<Record<string, unknown>, MetadataType>({
            context,
            operation,
            payload,
            metadata,
            auth: auth ?? store?.currentRequest.auth,
            client: clientString,
            requestChain: store?.currentRequest.requestChain,
        });

        // Send the message to the backend.
        await this.#backend.sendRequest(queue, post);
    }

    /**
     * Sends a service request, and waits for a response.
     *
     * @param clientID - The unique ID of the client making the request.
     * @param serviceGroupOrAlias - Attempts to look up this string as an alias name in the config, otherwise passes
     * this * as the serviceGroup.
     * @param context - The context for your new request.
     * @param operation - The operation for your new request.
     * @param payload - The payload of your new request.
     * @param metadata - Any metadata for the request. (This is useful for tracking and statistics.)
     * @param auth - The security token for your request, if required.
     * @param timeout - A desired timeout for the request. (Defaults to no timeout.)
     * @param clientString - A string to identify the client making the request.
     *
     * @returns Returns a completed request instance.
     */
    public async request<
        ReturnPayloadType = Record<string, unknown>,
        MetadataType extends Record<string, unknown> = Record<string, unknown>>(
        clientID : string,
        serviceGroupOrAlias : string,
        context : string,
        operation : string,
        payload : Record<string, unknown>,
        metadata ?: MetadataType,
        auth ?: string,
        timeout ?: number,
        clientString ?: string
    ) : Promise<ResponseEnvelope<ReturnPayloadType>>
    {
        // Resolve the serviceGroup
        const aliasConfig = this.#client.config.aliases ?? [];
        const alias = aliasConfig[serviceGroupOrAlias];
        const queue = alias ?? serviceGroupOrAlias;

        const store = localStorageUtil.getStore();
        metadata = (metadata ?? {}) as MetadataType;

        const request = new StrataRequest<Record<string, unknown>, MetadataType, ReturnPayloadType>({
            messageType: 'request',
            context,
            operation,
            payload,
            metadata,
            auth: auth ?? store?.currentRequest.auth,
            timeout: timeout ?? this.#client.config.service?.defaultRequestTimeout,
            client: clientString ?? clientID,
            priorRequest: store ? store.currentRequestID : undefined,
            requestChain: store?.currentRequest.requestChain,
        });

        // Track this outstanding request.
        this.#outstandingRequests.set(request.id, request);

        // Send the message to the backend.
        await this.#backend.sendRequest(queue, request.renderRequest(), clientID);

        // Wait for the promise to be fulfilled.
        await promiseTimeout(request.promise, request.timeout)
            .catch((error) =>
            {
                // An error occurred, so we're about to throw, which means we need to remove the tracking.
                this.#outstandingRequests.delete(request.id);

                if(error?.code === 'INTERNAL_TIMEOUT_ERROR')
                {
                    throw new RequestTimeoutError(request);
                }

                throw error;
            });

        // Remove tracking of the request
        this.#outstandingRequests.delete(request.id);

        if(request.status === 'failed')
        {
            throw new FailedRequestError(request);
        }

        // Return the response
        return request.renderResponse();
    }

    //------------------------------------------------------------------------------------------------------------------
    // Discovery
    //------------------------------------------------------------------------------------------------------------------

    /**
     * List known/discoverable services.
     */
    async listServices() : Promise<DiscoveredServices>
    {
        return this.#backend.listServices();
    }

    //------------------------------------------------------------------------------------------------------------------
    // Cleanup
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Tear down the queue engine, and clean anything up.
     */
    async teardown() : Promise<void>
    {
        logger.debug(`Tearing down ClientQueueEngine for client '${ this.#client.id }'.`);
        await this.#backend.teardown();
    }
}

//----------------------------------------------------------------------------------------------------------------------
