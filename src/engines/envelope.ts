//----------------------------------------------------------------------------------------------------------------------
// Envelope Engine
//----------------------------------------------------------------------------------------------------------------------

// Interfaces
import { PostEnvelope, RequestEnvelope, ResponseEnvelope } from '../interfaces/messages.js';

// Utils
import { genID } from '../utils/id.js';
import { Optional } from '../utils/types.js';

//----------------------------------------------------------------------------------------------------------------------

type PostConstructor<Payload, Metadata> =
    Omit<Optional<PostEnvelope<Payload, Metadata>, 'id' | 'timestamp'>, 'messageType' | 'responseQueue' | 'timeout'>;
type RequestConstructor<Payload, Metadata> = Omit<
    RequestEnvelope<Payload, Metadata>, 'timestamp' | 'messageType'
> & { timestamp ?: string | number };
type ResponseConstructor<Payload> = Omit<
    Optional<ResponseEnvelope<Payload>, 'payload' | 'messages' | 'service'>, 'timestamp' | 'messageType'
> & { timestamp ?: string | number };

//----------------------------------------------------------------------------------------------------------------------

export function buildPost<P, M>(post : PostConstructor<P, M>) : PostEnvelope<P, M>
{
    return {
        id: post.id ?? genID(),
        messageType: 'post',
        context: post.context,
        operation: post.operation,
        timestamp: post.timestamp ?? (new Date()).toISOString(),
        payload: post.payload,
        metadata: post.metadata,
        auth: post.auth,
        client: post.client ?? 'unknown',
        priorRequest: post.priorRequest,
        requestChain: post.requestChain,
    };
}

export function buildRequest<P, M>(request : RequestConstructor<P, M>) : RequestEnvelope<P, M>
{
    // Typescript's `Date` type doesn't take undefined, even though Date _does_ take undefined. So we cheat.
    const timestamp = request.timestamp ? (new Date(request.timestamp)).toISOString() : (new Date()).toISOString();

    return {
        id: request.id,
        messageType: 'request',
        context: request.context,
        operation: request.operation,
        responseQueue: request.responseQueue,
        timestamp,
        payload: request.payload,
        metadata: request.metadata,
        auth: request.auth,
        priorRequest: request.priorRequest,
        requestChain: request.requestChain,
        client: request.client ?? 'unknown',
        timeout: request.timeout,
    };
}

export function buildResponse<P>(response : ResponseConstructor<P>) : ResponseEnvelope<P>
{
    // Typescript's `Date` type doesn't take undefined, even though Date _does_ take undefined. So we cheat.
    const timestamp = response.timestamp ? (new Date(response.timestamp)).toISOString() : (new Date()).toISOString();

    return {
        id: response.id,
        messageType: 'response',
        context: response.context,
        operation: response.operation,
        timestamp,
        payload: (response.payload ?? {}) as P,
        status: response.status,
        messages: response.messages ?? [],
        service: response.service ?? 'unknown',
    };
}

//----------------------------------------------------------------------------------------------------------------------

export default {
    buildPost,
    buildRequest,
    buildResponse,
};

//----------------------------------------------------------------------------------------------------------------------
