//----------------------------------------------------------------------------------------------------------------------
// OperationLocalStorage
//----------------------------------------------------------------------------------------------------------------------

import { AsyncLocalStorage } from 'async_hooks';

// Interfaces
import { LocalStorage, RunFunction } from '../interfaces/localStorage.js';

//----------------------------------------------------------------------------------------------------------------------

const _asyncLocalStorage = new AsyncLocalStorage();

//------------------------------------------------------------------------------------------------------------------

/**
 * Runs a function inside an AsyncLocalStorage context. The `store` parameter is set as the store for this run
 * context.
 *
 * _Note: A "run context" is simply the chain of function calls (synchronous or asynchronous) triggered in the
 * execution of `func`. I.e. everything called inside `func` will be inside the "run context"._
 *
 * @param store - The store for this run context.
 * @param func - The function to run in the context.
 */
export async function run(store : LocalStorage, func : RunFunction) : Promise<void>
{
    await _asyncLocalStorage.run(store, () =>
    {
        return func();
    });
}

/**
 * Retrieves the current store for this run context.
 *
 * @returns Returns either the store, or undefined if no store has been set, or we're outside a run context.
 */
export function getStore() : LocalStorage | undefined
{
    return _asyncLocalStorage.getStore() as (LocalStorage | undefined);
}

//----------------------------------------------------------------------------------------------------------------------

export default {
    run,
    getStore,
};

//----------------------------------------------------------------------------------------------------------------------
