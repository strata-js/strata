// ---------------------------------------------------------------------------------------------------------------------
// Request Utils
// ---------------------------------------------------------------------------------------------------------------------

import localStorage from './localStorage.js';
import { ResponseMessage } from '../interfaces/messages.js';
import { LocalStorage } from '../interfaces/localStorage.js';

// ---------------------------------------------------------------------------------------------------------------------

/**
 * Retrieves the current request context from asyncLocalStorage. If there is no context, it returns undefined.
 *
 * @returns Returns either the context, or undefined if no context has been set.
 */
export function getRequestContext() : LocalStorage | undefined
{
    return localStorage.getStore();
}

/**
 * Adds a message to the current request context. This will be rendered out in the response.
 *
 * @param message - The message to add.
 */
export function addMessage(message : ResponseMessage) : void
{
    const store = localStorage.getStore();
    if(store)
    {
        store.currentRequest.messages.push(message);
    }
}

/**
 * Adds an error message to the current request context. This will be rendered out in the response.
 *
 * @param message - The message to add.
 * @param code - A machine-readable code for this message.
 * @param type - A human-readable type for this message.
 * @param details - Additional details about this message.
 * @param stack - The stack trace for this message.
 */
export function addError(
    message : string,
    code ?: string,
    type ?: string,
    details ?: Record<string, unknown>,
    stack ?: string
) : void
{
    addMessage({ severity: 'error', message, code, type, details, stack });
}

/**
 * Adds a warning message to the current request context. This will be rendered out in the response.
 *
 * @param message - The message to add.
 * @param code - A machine-readable code for this message.
 * @param type - A human-readable type for this message.
 * @param details - Additional details about this message.
 * @param stack - The stack trace for this message.
 */
export function addWarning(
    message : string,
    code ?: string,
    type ?: string,
    details ?: Record<string, unknown>,
    stack ?: string
) : void
{
    addMessage({ severity: 'warning', message, code, type, details, stack });
}

/**
 * Adds an info message to the current request context. This will be rendered out in the response.
 *
 * @param message - The message to add.
 * @param code - A machine-readable code for this message.
 * @param type - A human-readable type for this message.
 * @param details - Additional details about this message.
 * @param stack - The stack trace for this message.
 */
export function addInfo(
    message : string,
    code ?: string,
    type ?: string,
    details ?: Record<string, unknown>,
    stack ?: string
) : void
{
    addMessage({ severity: 'info', message, code, type, details, stack });
}

/**
 * Adds a debug message to the current request context. This will be rendered out in the response.
 *
 * @param message - The message to add.
 * @param code - A machine-readable code for this message.
 * @param type - A human-readable type for this message.
 * @param details - Additional details about this message.
 * @param stack - The stack trace for this message.
 */
export function addDebug(
    message : string,
    code ?: string,
    type ?: string,
    details ?: Record<string, unknown>,
    stack ?: string
) : void
{
    addMessage({ severity: 'debug', message, code, type, details, stack });
}

// ---------------------------------------------------------------------------------------------------------------------
