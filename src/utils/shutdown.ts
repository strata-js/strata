// ---------------------------------------------------------------------------------------------------------------------
// Signal Handling Util
// ---------------------------------------------------------------------------------------------------------------------

import { logging } from '@strata-js/util-logging';

// Clients
import { StrataClient } from '../clients/client.js';
import { StrataService } from '../clients/service.js';

// Utils
import { promiseTimeout } from './timeout.js';

// ---------------------------------------------------------------------------------------------------------------------

type Signal = 'SIGTERM' | 'SIGINT' | 'SIGBREAK' | string;

const logger = logging.getLogger('shutdownUtil');

// ---------------------------------------------------------------------------------------------------------------------

class ShutdownUtil
{
    #initialized = false;
    #isShuttingDown = false;
    #interruptsToForceShutdown = 3;
    #shutdownTimeout = 30 * 1000;
    #clients = new Set<StrataClient>();
    #services = new Set<StrataService>();

    constructor()
    {
        this.$handleKeyInterrupt('SIGBREAK');
        this.$handleKeyInterrupt('SIGINT');

        // Also listen for signals to start shutdown, but not force it.
        this.$gracefulShutdownOnSignal('SIGTERM');
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Properties
    // -----------------------------------------------------------------------------------------------------------------

    get isShuttingDown() : boolean { return this.#isShuttingDown; }

    // -----------------------------------------------------------------------------------------------------------------
    // Helpers
    // -----------------------------------------------------------------------------------------------------------------

    private $gracefulShutdownOnSignal(signal : Signal) : void
    {
        process.on(signal, () =>
        {
            logger.info(`Got ${ signal }; shutting down gracefully...`);
            this.gracefulShutdown();
        });
    }

    private $handleKeyInterrupt(signal : Signal) : void
    {
        process.on(signal, () =>
        {
            this.#interruptsToForceShutdown--;

            if(this.#isShuttingDown)
            {
                if(this.#interruptsToForceShutdown > 0)
                {
                    logger.warn(`Key Interrupt (${ signal }): Already shutting down; interrupt`
                        + ` ${ this.#interruptsToForceShutdown } more time(s) to force shutdown.`);
                }
                else
                {
                    logger.warn(`Key Interrupt (${ signal }): Forcing shutdown.`);
                    process.exit(1);
                }
            }
            else
            {
                logger.warn(`Key Interrupt (${ signal }): Shutting down; interrupt`
                    + ` ${ this.#interruptsToForceShutdown } more time(s) to force shutdown.`);
                this.gracefulShutdown();
            }
        });
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Public
    // -----------------------------------------------------------------------------------------------------------------

    public init(shutdownTimeout ?: number, interruptsToForceShutdown ?: number) : void
    {
        if(!this.#initialized)
        {
            this.#initialized = true;
            this.#shutdownTimeout = shutdownTimeout ?? this.#shutdownTimeout;
            this.#interruptsToForceShutdown = interruptsToForceShutdown ?? this.#interruptsToForceShutdown;
        }
    }

    /**
     * Initiates a graceful shutdown (i.e. it waits for incoming messages to be processed, and outgoing messages to be
     * completed as well.)
     *
     * Under normal circumstances, this shouldn't be called by a developer's code, instead, it should be called by
     * internal signal handlers.
     *
     * @param exitCode - The exit code to exit with. (Defaults to `0`.)
     */
    public async gracefulShutdown(exitCode = 0) : Promise<void>
    {
        if(!this.#isShuttingDown)
        {
            this.#isShuttingDown = true;

            logger.info('Shutdown started.');

            // ---------------------------------------------------------------------------------------------------------
            // Shutdown Services
            // ---------------------------------------------------------------------------------------------------------

            logger.info(`Shutting down ${ this.#services.size } services with outstanding requests.`);

            // We shut down all clients at the same time, but give each of them their own timeout.
            await Promise.all([ ...this.#services ].map(async(service) =>
            {
                // Shutdown service
                if(service.initialized)
                {
                    logger.info(`Shutting down incoming request listening with`
                        + ` ${ service.outstandingRequests.size } messages outstanding.`);

                    // We wait for messages to process, but with a (possible) timeout.
                    await promiseTimeout(() => service.teardown(), this.#shutdownTimeout)
                        .then(() =>
                        {
                            logger.info('Successfully shut down incoming request listening.');
                        })
                        .catch((error) =>
                        {
                            if(error.message.includes('Timeout'))
                            {
                                logger.warn(`Timeout expired with`
                                    + ` ${ service.outstandingRequests.size } messages outstanding.`);
                            }
                            else
                            {
                                logger.error('Error encountered while shutting down incoming requests:', error.stack);
                            }
                        });
                }
            }));

            // ---------------------------------------------------------------------------------------------------------
            // Shutdown Clients
            // ---------------------------------------------------------------------------------------------------------

            logger.info(`Shutting down ${ this.#clients.size } clients.`);

            // We shut down all clients at the same time, but give each of them their own timeout.
            await Promise.all([ ...this.#clients ].map(async(client) =>
            {
                if(client.initialized)
                {
                    logger.info(`Shutting down client ${ client.name }(${ client.id }) with`
                        + ` ${ client.outstandingRequests.size } outstanding requests.`);
                    await promiseTimeout(() => client.teardown(), this.#shutdownTimeout)
                        .then(() =>
                        {
                            logger.info(`Successfully shut down client ${ client.name }(${ client.id }).`);
                        })
                        .catch((error) =>
                        {
                            if(error.message.includes('Timeout'))
                            {
                                logger.warn(`Timeout expired for client ${ client.name }(${ client.id }) with`
                                    + ` ${ client.outstandingRequests.size } messages outstanding.`);
                            }
                            else
                            {
                                logger.error('Error encountered while shutting down incoming requests:', error.stack);
                            }
                        });
                }
            }));

            // ---------------------------------------------------------------------------------------------------------

            logger.info('Graceful shutdown complete. Exiting.');

            process.exit(exitCode);
        }
        else
        {
            logger.warn(`Already shutting down; gracefulShutdown() called multiple times. Ignoring.`);
        }
    }

    /**
     * Registers services with the shutdown handler, so we can wait for them to finish cleaning up when performing a
     * graceful shutdown.
     *
     * @param service - The service to register.
     */
    registerService(service : StrataService) : void
    {
        this.#services.add(service);
    }

    /**
     * Registers clients with the shutdown handler, so we can wait for them to finish cleaning up when performing a
     * graceful shutdown.
     *
     * @param client - The client to register.
     */
    registerClient(client : StrataClient) : void
    {
        this.#clients.add(client);
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export default new ShutdownUtil();

// ---------------------------------------------------------------------------------------------------------------------
