//----------------------------------------------------------------------------------------------------------------------
// Timeout Utilities
//----------------------------------------------------------------------------------------------------------------------

import { InternalTimeoutError } from '../lib/errors.js';

//----------------------------------------------------------------------------------------------------------------------

type PromiseFn<T> = (...args : any[]) => Promise<T>;

//----------------------------------------------------------------------------------------------------------------------

/**
 * Allows a promise to run for a specified amount of time before throwing an error. _(The promise is not canceled, and
 * may still complete, but the chain is broken and there is no way to recover the late value.)_ If `maxTime` is
 * undefined, less than 1, or infinite, the wrapper will resolve the promise and not initiate timeout logic.
 *
 * @param promiseOrFn - A function that returns a promise, or a promise. This promise is waited on for completion.
 * @param maxTime - The maximum number of milliseconds to wait before throwing an [[InternalTimeoutError]].
 *
 * @returns Resolves to the same value as `promiseOrFn`, or throws an [[InternalTimeoutError]].
 */
export function promiseTimeout<T>(promiseOrFn : PromiseFn<T> | Promise<T>, maxTime ?: number) : Promise<T>
{
    return new Promise((resolve, reject) =>
    {
        let alreadyRejected = false;

        if(typeof promiseOrFn === 'function')
        {
            promiseOrFn = promiseOrFn();
        }

        if(maxTime && maxTime > 0 && Number.isFinite(maxTime))
        {
            const timeoutHandle = setTimeout(() =>
            {
                alreadyRejected = true;
                reject(new InternalTimeoutError(maxTime));
            }, maxTime);

            // Wrap a promise, but handle the fact that we might have timed out already.
            Promise.resolve(promiseOrFn)
                .then((result : T) =>
                {
                    if(!alreadyRejected)
                    {
                        clearTimeout(timeoutHandle);
                        resolve(result);
                    }
                })
                .catch((err) =>
                {
                    if(!alreadyRejected)
                    {
                        clearTimeout(timeoutHandle);
                        reject(err);
                    }
                });
        }
        else
        {
            resolve(promiseOrFn);
        }
    });
}

//----------------------------------------------------------------------------------------------------------------------
