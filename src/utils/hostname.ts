//----------------------------------------------------------------------------------------------------------------------
// Hostname Util
//----------------------------------------------------------------------------------------------------------------------

import { hostname } from 'os';

//----------------------------------------------------------------------------------------------------------------------

export function getHostname() : string
{
    // Note: this assumes we're either setting a `HOSTNAME` variable, or otherwise setting up docker to return the
    // correct hostname. For more information, please see this:
    // eslint-disable-next-line @stylistic/max-len
    // https://stackoverflow.com/questions/29808920/how-to-get-the-hostname-of-the-docker-host-from-inside-a-docker-container-on-tha
    return process.env.HOSTNAME ?? hostname();
}

//----------------------------------------------------------------------------------------------------------------------
