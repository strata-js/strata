// ---------------------------------------------------------------------------------------------------------------------
// VersionUtil
// ---------------------------------------------------------------------------------------------------------------------

import path from 'path';
import pkgDir from 'pkg-dir';
import { readFile } from 'node:fs/promises';

// ---------------------------------------------------------------------------------------------------------------------

let _initialized = false;
let _appVersion = '0.0.0';
let _libVersion = '0.0.0';
let _appName = '';

// ---------------------------------------------------------------------------------------------------------------------

export function appVersion() : string
{
    return _appVersion;
}

export function libVersion() : string
{
    return _libVersion;
}

export function appName() : string
{
    return _appName;
}

// ---------------------------------------------------------------------------------------------------------------------

export async function init() : Promise<void>
{
    if(!_initialized)
    {
        // Set root paths
        const appRoot = (await pkgDir()) ?? '.';
        const libRoot = (await pkgDir(import.meta.dirname)) ?? '.';

        // Import package.json files
        const appPkgStr = await readFile(path.join(appRoot, 'package.json'), { encoding: 'utf-8' });
        const libPkgStr = await readFile(path.join(libRoot, 'package.json'), { encoding: 'utf-8' });

        const appPkg = JSON.parse(appPkgStr);
        const libPkg = JSON.parse(libPkgStr);

        _appVersion = process.env.APP_VERSION ?? appPkg.version;
        _libVersion = libPkg.version;

        _appName = process.env.npm_package_name ?? process.env.APP_NAME ?? appPkg.name;

        _initialized = true;
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export default {
    init,
    get libVersion() { return _libVersion; },
    get appVersion() { return _appVersion; },
    get appName() { return _appName; },
};

// ---------------------------------------------------------------------------------------------------------------------
