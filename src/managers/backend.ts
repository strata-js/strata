//----------------------------------------------------------------------------------------------------------------------
// Backend Manager
//----------------------------------------------------------------------------------------------------------------------

import { logging } from '@strata-js/util-logging';
import { BaseStrataBackend } from '../resource-access/backends/base.js';
import { AlreadyRegisteredError, UnknownBackendError } from '../lib/errors.js';

// Internal Backends
import { NullBackend } from '../resource-access/backends/null.js';
import { RedisBackend } from '../resource-access/backends/redis/index.js';
import { RedisStreamsBackend } from '../resource-access/backends/redisStreams/index.js';

//----------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('backendMan');

//----------------------------------------------------------------------------------------------------------------------

type BackendConstructor = new () => BaseStrataBackend<any>;

//----------------------------------------------------------------------------------------------------------------------

class BackendManager
{
    readonly #backends = new Map<string, BackendConstructor>();

    //------------------------------------------------------------------------------------------------------------------

    constructor()
    {
        this.registerInternalBackends();
    }

    /**
     * Registers a backend constructor under a given name. This allows the backend to be used with Strata under the
     * given name. (All our built-in backends use this registration internally.)
     *
     * @param backendName - The name of the backend. (This should match the `name` parameter in the configuration.)
     * @param backendClass - A constructor for the backend instance.
     */
    register(backendName : string, backendClass : BackendConstructor) : void
    {
        if(this.#backends.has(backendName))
        {
            throw new AlreadyRegisteredError(backendName, 'backend');
        }

        this.#backends.set(backendName, backendClass);
        logger.debug(`Successfully registered '${ backendName }' backend.`);
    }

    /**
     * Get a new instance of the given backend.
     *
     * @param backendName - The name of the backend. (This should match the `name` parameter in the configuration.)
     */
    getInstance<T extends BaseStrataBackend<any>>(backendName : string) : T
    {
        const BackendClass = this.#backends.get(backendName);
        if(!BackendClass)
        {
            throw new UnknownBackendError(backendName);
        }

        // We always need to return a new instance, so that the backend can track things like the generated queue name,
        // or other client/service specific information.
        return (new BackendClass()) as T;
    }

    /**
     * Gets a list of valid backend names.
     *
     * @returns Returns a list of all the valid backends, by name.
     */
    public getNames() : string[]
    {
        return Array.from(this.#backends.keys());
    }

    /**
     * Registers all the internal backends.
     *
     * _Note: This is primarily for unit testing. _
     */
    public registerInternalBackends() : void
    {
        this.register('null', NullBackend);
        this.register('redis', RedisBackend);
        this.register('redis-streams', RedisStreamsBackend);
    }

    /**
     * Clears all registered backends.
     *
     * _Note: This is primarily for unit testing. _
     */
    public clear() : void
    {
        this.#backends.clear();
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new BackendManager();

//----------------------------------------------------------------------------------------------------------------------
