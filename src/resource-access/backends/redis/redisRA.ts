//----------------------------------------------------------------------------------------------------------------------
// Redis Queues Resource Access
//----------------------------------------------------------------------------------------------------------------------

import { Redis, RedisOptions } from 'ioredis';
import { logging } from '@strata-js/util-logging';

// Resource Access
import { RedisResourceAccess } from '../common/redis.js';

//----------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('redisRA');

//----------------------------------------------------------------------------------------------------------------------

class RedisQueueResourceAccess
{
    #redisRA = new RedisResourceAccess();

    //------------------------------------------------------------------------------------------------------------------

    public getConnection(connectionConfig : RedisOptions, connectionName ?: string) : Redis
    {
        return this.#redisRA.getConnection(connectionConfig, connectionName);
    }

    public disconnect(conn ?: Redis) : void
    {
        return this.#redisRA.disconnect(conn);
    }

    public async blockingRead(
        conn : Redis,
        key : string,
        blockTimeout = 2000
    ) : Promise<Record<string, unknown> | undefined>
    {
        logger.trace('Starting a blocking read...');

        // Redis specifies the timeout in seconds.
        const timeout = blockTimeout / 1000;

        // Wait for a message to be pushed to the queue.
        const results = await conn.blpop(key, timeout);

        logger.trace('Blocking read finished.');

        if(results)
        {
            // Returns the key that was popped, and the message, which should be a JSON string.
            const [ , msg ] = results;
            return JSON.parse(msg);
        }
    }

    public async insertMessage(conn : Redis, key : string, message : Record<string, unknown>) : Promise<void>
    {
        const messageJSON = JSON.stringify(message);
        await conn.rpush(key, messageJSON);
    }

    async getClientID(conn : Redis) : Promise<string>
    {
        return (await conn.client('ID')).toString();
    }

    public async unblock(conn : Redis, name : string) : Promise<void>
    {
        await this.#redisRA.unblock(conn, name);
    }

    public async publish(conn : Redis, channel : string, message : string) : Promise<void>
    {
        await this.#redisRA.publish(conn, channel, message);
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new RedisQueueResourceAccess();

//----------------------------------------------------------------------------------------------------------------------
