//----------------------------------------------------------------------------------------------------------------------
// RedisQueuesBackend
//----------------------------------------------------------------------------------------------------------------------

import { Redis } from 'ioredis';
import { logging } from '@strata-js/util-logging';

import { BaseStrataBackend } from '../base.js';

// Interfaces
import { RedisBackendConfig } from '../../../interfaces/config.js';
import { DiscoveredServices } from '../../../interfaces/discovery.js';
import {
    ConcurrencyCheck,
    PostEnvelope,
    RequestEnvelope,
    ResponseEnvelope,
} from '../../../interfaces/messages.js';
import { KnownServiceCommand, ServiceCommandResponse } from '../../../interfaces/command.js';
import { AlreadyInitializedError } from '../../../lib/errors.js';
import { StrataService } from '../../../clients/service.js';
import { RedisDiscovery } from '../common/redisDiscovery.js';

// Resource Access
import redisRA from './redisRA.js';
import { setTimeout } from 'node:timers/promises';
import { BackendInfo } from '../../../interfaces/service.js';
import { StrataRequest } from '../../../lib/request.js';

// ---------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('redisBackend');

//----------------------------------------------------------------------------------------------------------------------

type Handler<T> = (message : T) => void;
type ListenCheck = () => Promise<boolean>;

export interface QueueConfig
{
    blockingTimeout ?: number | {
        request ?: number;
        response ?: number;
    };
}

export interface RedisBackendOptions extends RedisBackendConfig
{
    type : 'redis';
    queue ?: QueueConfig;
}

export interface RedisBackendInfo extends BackendInfo
{
    type : 'redis';
    redis : RedisBackendConfig['redis'];
}

// ---------------------------------------------------------------------------------------------------------------------

export class RedisBackend extends BaseStrataBackend<RedisBackendOptions>
{
    // Main redis connections, used for blocking on our listen queues.
    #requestConn ?: Redis;
    #responseConn ?: Redis;

    // This connection should remain unblocked and never subscribe to any channels. It used for sending commands and
    // responses.
    #sendConn ?: Redis;

    // Per the redis docs, a subscribed client should only be used for pub/sub. This client will be used to subscribe
    // to all pubsub channels. We will use `#sendConn` for publishing to channels.
    #commConn ?: Redis;

    #initialized = false;
    #listeningForCommands = false;
    #listeningForCommandResponses = false;
    #listeningForRequests = false;
    #listeningForResponses = false;

    // Queues
    #requestQueue ?: string;
    #responseQueue ?: string;

    // Configuration
    #config : RedisBackendOptions = { type: 'redis', redis: {}, isolateCommands: true };

    // Discovery Instance
    #discovery ?: RedisDiscovery;

    // Redis ClientIDs
    #requestClientID ?: string;
    #responseClientID ?: string;

    //------------------------------------------------------------------------------------------------------------------
    // Properties
    //------------------------------------------------------------------------------------------------------------------

    get initialized() : boolean
    {
        return this.#initialized;
    }

    get blockingTimeouts() : { request ?: number, response ?: number }
    {
        const blockingTimeout = this.#config.queue?.blockingTimeout;
        if(typeof blockingTimeout === 'object')
        {
            return {
                request: blockingTimeout.request,
                response: blockingTimeout.response,
            };
        }
        else
        {
            return { request: blockingTimeout, response: blockingTimeout };
        }
    }

    get info() : RedisBackendInfo
    {
        return {
            type: 'redis',
            redis: this.#config.redis,
        };
    }

    //------------------------------------------------------------------------------------------------------------------
    // Internal helpers
    //------------------------------------------------------------------------------------------------------------------

    private _buildQueueName(prefix : string, serviceGroup : string) : string
    {
        return `${ prefix }:${ serviceGroup }`;
    }

    private _parseJSON<T>(message : string) : T | undefined
    {
        try
        {
            return JSON.parse(message);
        }
        catch (ex)
        {
            if(ex instanceof SyntaxError)
            {
                logger.warn('Failed to parse message:', message);
                logger.debug('Parse error:', ex.stack);
            }
            else if(ex instanceof Error)
            {
                logger.warn('Unexpected error parsing message:', ex.message);
                logger.debug('Parse error:', ex.stack);
            }
            else
            {
                logger.error('Unexpected error parsing message:', ex);
            }
        }
    }

    private _handleCommand(_channel : string, message : string) : void
    {
        const parsed = this._parseJSON<KnownServiceCommand>(message);
        if(parsed)
        {
            this.emit('incomingCommand', parsed);
        }
    }

    private _handleCommandResponse(_channel : string, message : string) : void
    {
        const parsed = this._parseJSON<ServiceCommandResponse>(message);
        if(parsed)
        {
            this.emit('incomingCommandResponse', parsed);
        }
    }

    private _handleIncomingRequest(envelope : RequestEnvelope | PostEnvelope) : void
    {
        this.emit('incomingRequest', envelope);
    }

    private _handleIncomingResponse(envelope : ResponseEnvelope) : void
    {
        this.emit('incomingResponse', envelope);
    }

    private async _listenLoop<Envelope extends RequestEnvelope | PostEnvelope | ResponseEnvelope>(
        redisConn : Redis,
        queue : string,
        handler : Handler<Envelope>,
        blockingTimeout ?: number,
        listenCheck : ListenCheck = async() => true,
        concurrencyCheck : ConcurrencyCheck = async() => true
    ) : Promise<void>
    {
        if(redisConn)
        {
            // Call the engine's loop
            if(redisConn.status === 'ready')
            {
                logger.trace('listenLoop called.');
                if(await concurrencyCheck())
                {
                    logger.trace('`concurrencyCheck()` check passed.');

                    const message = await redisRA.blockingRead(redisConn, queue, blockingTimeout);

                    if(message)
                    {
                        // TODO: There's no enforcement here! We should have some validation the envelope is correctly
                        //  formatted! This should be AVJ based.
                        const envelope : Envelope = {
                            ...message as Envelope,
                        };

                        // Call the handler
                        handler(envelope);
                    }
                }
                else
                {
                    logger.trace('`concurrencyCheck()` check failed. Skipping pulling messages.');

                    // If we're too busy to work messages, let's wait for 50ms, and then try again.
                    await setTimeout(50);
                }
            }
            else
            {
                // If redis connection is not ready, slow down the listen loop
                await setTimeout(200);
            }

            // If we're still listening, we call ourselves recursively. We do _not_ await the promise.
            if(await listenCheck())
            {
                logger.trace('Calling _listenLoop again.');
                this._listenLoop<Envelope>(
                    redisConn,
                    queue,
                    handler,
                    blockingTimeout,
                    listenCheck,
                    concurrencyCheck
                );
            }
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    async init(config : RedisBackendOptions) : Promise<void>
    {
        if(this.#initialized)
        {
            throw new AlreadyInitializedError('RedisStreams Backend Initialization');
        }

        this.#config = {
            ...config,

            // Default to isolating commands
            isolateCommands: config.isolateCommands ?? true,
        };

        this.#sendConn = redisRA.getConnection(this.#config.redis, 'Send Connection');

        this.#listeningForCommands = false;
        this.#listeningForCommandResponses = false;
        this.#listeningForRequests = false;
        this.#listeningForResponses = false;

        this.#initialized = true;

        logger.info('Redis backend initialized.');
    }

    async enableDiscovery(service : StrataService) : Promise<void>
    {
        logger.trace(`enableDiscovery('<Service:${ service.serviceGroup }', '${ service.id }>') called.`);

        this.#discovery = new RedisDiscovery(this.#config, service);

        // Start discovery heartbeat
        await this.#discovery.startDiscoveryHeartbeat();
    }

    async disableDiscovery() : Promise<void>
    {
        logger.trace('disableDiscovery() called.');

        if(this.#discovery)
        {
            // Stop discovery heartbeat
            await this.#discovery.teardown();
            this.#discovery = undefined;
        }
    }

    async listServices() : Promise<DiscoveredServices>
    {
        logger.trace('listServices() called.');
        if(!this.#discovery)
        {
            this.#discovery = new RedisDiscovery({ redis: this.#config.redis });
        }

        return this.#discovery.listServices();
    }

    async listenForCommands(serviceGroup : string, serviceID : string) : Promise<void>
    {
        logger.trace(`listenForCommands('${ serviceGroup }', '${ serviceID }') called.`);

        let servicesPrefix = 'Services';
        if(this.#config.isolateCommands)
        {
            const redisDB = this.#config.redis.db ?? 0;
            servicesPrefix = `Services-DB${ redisDB }`;
        }

        if(!this.#listeningForCommands)
        {
            const commandChannels = [
                `${ servicesPrefix }`,
                `${ servicesPrefix }:${ serviceGroup }`,
                `${ servicesPrefix }:${ serviceGroup }:${ serviceID }`,
            ];

            // Build Connection
            this.#commConn = redisRA.getConnection(this.#config.redis, 'Command Connection');

            // Prepare message handler
            this.#commConn.on('message', this._handleCommand.bind(this));

            // Subscribe to control commands
            await this.#commConn.subscribe(...commandChannels);

            // Set listening flag
            this.#listeningForCommands = true;

            logger.debug(`listenForCommands('${ serviceGroup }', '${ serviceID }') successfully called.`);
        }
        else
        {
            logger.warn(
                'Attempted to listen for commands, but was already listening. This is likely a (harmless) bug.'
            );
        }
    }

    async listenForCommandResponses(clientID : string) : Promise<void>
    {
        if(!this.#listeningForCommandResponses)
        {
            // Build Connection
            this.#commConn = redisRA.getConnection(this.#config.redis, 'Command Response Connection');

            // Prepare message handler
            this.#commConn.on('message', this._handleCommandResponse.bind(this));

            // Subscribe to control commands
            await this.#commConn.subscribe(`Clients:${ clientID }`);

            // Set listening flag
            this.#listeningForCommandResponses = true;

            logger.debug(`listenForCommandResponses('${ clientID }') successfully called.`);
        }
        else
        {
            logger.warn(
                'Attempted to listen for command responses, but was already listening. This is likely a (harmless) bug.'
            );
        }

        logger.trace(`listenForCommandResponses('${ clientID }') called.`);
    }

    async listenForRequests(
        serviceGroup : string,
        serviceID : string,
        waterMarkCheck : ConcurrencyCheck
    ) : Promise<void>
    {
        logger.trace(`listenForRequests('${ serviceGroup }', '${ serviceID }', <waterMarkCheck>) called.`);

        if(!this.#listeningForRequests)
        {
            // Build the queue name
            const queue = this._buildQueueName('Requests', serviceGroup);
            this.#requestQueue = queue;

            // Build Connection
            this.#requestConn = redisRA.getConnection(this.#config.redis, 'Request Connection');
            this.#requestClientID = await redisRA.getClientID(this.#requestConn);

            // Set listening flag
            this.#listeningForRequests = true;

            logger.trace('Listening for requests on queue:', this.#requestQueue);

            // Start the loop!
            this._listenLoop<RequestEnvelope | PostEnvelope>(
                this.#requestConn,
                queue,
                this._handleIncomingRequest.bind(this),
                this.blockingTimeouts.request,
                async() => this.#listeningForRequests,
                waterMarkCheck
            );

            logger.debug(
                `listenForRequests('${ serviceGroup }', '${ serviceID }', <waterMarkCheck>) successfully called.`
            );
        }
        else
        {
            logger.warn(
                'Attempted to listen for requests, but was already listening. This is likely a (harmless) bug.'
            );
        }
    }

    async listenForResponses(clientID : string, clientName ?: string) : Promise<void>
    {
        logger.trace(`listenForResponses('${ clientID }', '${ clientName }') called.`);

        if(!this.#listeningForResponses)
        {
            // Build our response queue name
            const clientText = `${ clientName ? `${ clientName }:` : '' }${ clientID }`;
            const queue = this._buildQueueName('Responses', clientText);
            this.#responseQueue = queue;

            // Build Connection
            this.#responseConn = redisRA.getConnection(this.#config.redis, 'Response Connection');
            this.#responseClientID = await redisRA.getClientID(this.#responseConn);

            // Set listening flag
            this.#listeningForResponses = true;

            logger.trace('Listening for responses on queue:', this.#responseQueue);

            // Start the loop!
            this._listenLoop<ResponseEnvelope>(
                this.#responseConn,
                queue,
                this._handleIncomingResponse.bind(this),
                this.blockingTimeouts.response,
                async() => this.#listeningForResponses
            );

            logger.debug(`listenForResponses('${ clientID }', '${ clientName }') successfully called.`);
        }
        else
        {
            logger.warn(
                'Attempted to listen for responses, but was already listening. This is likely a (harmless) bug.'
            );
        }
    }

    async sendCommand(target : string, command : KnownServiceCommand) : Promise<void>
    {
        logger.trace(`sendCommand('${ target }', '${ JSON.stringify(command, null, 4) }') called.`);

        // If we're isolating commands, we need to rewrite the 'Services' portion of the target to include the
        // DB number.
        if(this.#config.isolateCommands)
        {
            const redisDB = this.#config.redis.db ?? 0;

            // Make sure the user didn't already do this substitution
            if(!/^Services-DB\d/.test(target))
            {
                target = target.replace(/^Services/, `Services-DB${ redisDB }`);

                logger.trace(`Rewrote target to include DB number: '${ target }'.`);
            }
        }

        if(this.#sendConn)
        {
            await redisRA.publish(this.#sendConn, target, JSON.stringify(command));
        }
        else
        {
            logger.warn('Attempting to send command, but control connection is missing.');
        }
    }

    async sendCommandResponse(target : string, envelope : ServiceCommandResponse) : Promise<void>
    {
        logger.trace(`sendCommandResponse('${ JSON.stringify(envelope, null, 4) }') called.`);

        if(this.#sendConn)
        {
            await redisRA.publish(this.#sendConn, target, JSON.stringify(envelope));
        }
        else
        {
            logger.warn('Attempting to send command, but control connection is missing.');
        }
    }

    async sendRequest(serviceGroup : string, envelope : RequestEnvelope | PostEnvelope) : Promise<void>
    {
        logger.trace(`sendRequest('${ serviceGroup }', '${ JSON.stringify(envelope, null, 4) }') called.`);

        if(this.#sendConn)
        {
            const sendQueue = this._buildQueueName('Requests', serviceGroup);

            // Add responseQueue
            if(envelope.messageType === 'request')
            {
                envelope.responseQueue = this.#responseQueue;
            }

            // Send the request
            await redisRA.insertMessage(this.#sendConn, sendQueue, { ...envelope });
        }
        else
        {
            logger.error(`Attempted to send request, but there is no connection.`);
        }
    }

    async sendResponse(request : StrataRequest) : Promise<void>
    {
        logger.trace(`sendResponse('${ JSON.stringify(request.renderRequest(), null, 4) }') called.`);
        if(request.messageType === 'request')
        {
            if(!request.responseQueue)
            {
                logger.warn(`Ignoring attempt to respond to request '${ request.id }'; no response queue.`);
            }
            else if(this.#sendConn)
            {
                // Send the response
                await redisRA.insertMessage(this.#sendConn, request.responseQueue, { ...request.renderResponse() });
            }
            else
            {
                logger.error(`Attempted to send response, but there is no connection, or we're not listening.`);
            }
        }
    }

    async stopListeningForCommands() : Promise<void>
    {
        logger.trace('stopListeningForCommands() called.');

        if(this.#commConn)
        {
            this.#commConn.punsubscribe('Services*');
        }

        this.#listeningForCommands = false;

        logger.debug('stopListeningForCommands() complete.');
    }

    async stopListeningForCommandResponses() : Promise<void>
    {
        logger.trace('stopListeningForCommands() called.');
        if(this.#commConn)
        {
            this.#commConn.punsubscribe('Clients*');
        }

        this.#listeningForCommands = false;

        logger.debug('stopListeningForCommands() complete.');
    }

    async stopListeningForRequests(serviceGroup : string) : Promise<void>
    {
        logger.trace(`stopListeningForRequests('${ serviceGroup }') called.`);

        // Stop listening
        this.#listeningForRequests = false;

        // If we have listen details, we attempt to clean up.
        if(this.#sendConn && this.#requestClientID)
        {
            // Unblock the listen connection
            await redisRA.unblock(this.#sendConn, this.#requestClientID);
        }

        logger.debug('stopListeningForRequests() complete.');
    }

    async stopListeningForResponses() : Promise<void>
    {
        logger.trace('stopListeningForResponses() called.');

        // Stop listening
        this.#listeningForResponses = false;

        // If we have listen details, we attempt to clean up.
        if(this.#sendConn && this.#responseClientID)
        {
            // Unblock the listen connection
            await redisRA.unblock(this.#sendConn, this.#responseClientID);
        }

        logger.debug('stopListeningForResponses() complete.');
    }

    async teardown() : Promise<void>
    {
        logger.trace('teardown() called.');

        if(this.#listeningForRequests)
        {
            // The parameters don't matter here, as we don't use them to stop listening.
            await this.stopListeningForRequests('');
        }

        if(this.#listeningForResponses)
        {
            await this.stopListeningForResponses();
        }

        if(this.#listeningForCommands)
        {
            await this.stopListeningForCommands();
        }

        if(this.#listeningForCommandResponses)
        {
            await this.stopListeningForCommandResponses();
        }

        if(this.#discovery)
        {
            await this.disableDiscovery();
        }

        // Disconnect from Redis
        redisRA.disconnect(this.#requestConn);
        redisRA.disconnect(this.#responseConn);
        redisRA.disconnect(this.#sendConn);
        redisRA.disconnect(this.#commConn);

        // Reset state
        this.#requestConn = undefined;
        this.#responseConn = undefined;
        this.#sendConn = undefined;
        this.#commConn = undefined;
        this.#requestQueue = undefined;
        this.#responseQueue = undefined;
        this.#initialized = false;

        this.#config = { type: 'redis', redis: {} };

        logger.debug('teardown() complete.');
    }
}

//----------------------------------------------------------------------------------------------------------------------
