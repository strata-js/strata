// ---------------------------------------------------------------------------------------------------------------------
// NullBackend
// ---------------------------------------------------------------------------------------------------------------------

import { logging } from '@strata-js/util-logging';
import { BaseStrataBackend } from './base.js';
import { DiscoveredServices } from '../../interfaces/discovery.js';
import {
    ConcurrencyCheck,
    PostEnvelope,
    RequestEnvelope,
    ResponseEnvelope,
} from '../../interfaces/messages.js';
import { KnownServiceCommand, ServiceCommandResponse } from '../../interfaces/command.js';
import { BackendConfig } from '../../interfaces/config.js';
import { StrataService } from '../../clients/service.js';
import { StrataRequest } from '../../lib/request.js';

// ---------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('nullBackend');

// ---------------------------------------------------------------------------------------------------------------------

export interface NullBackendOptions extends BackendConfig
{
    type : 'null';
}

// ---------------------------------------------------------------------------------------------------------------------

export class NullBackend extends BaseStrataBackend<NullBackendOptions>
{
    #initialized = false;

    listeningForCommands = false;
    listeningForCommandResponses = false;
    listeningForRequests = false;
    listeningForResponses = false;
    discoveryEnabled = false;
    listServicesCalled = false;
    teardownCalled = false;

    // Set this to control the return value of listServices().
    discoveredServices : DiscoveredServices = {};

    command : KnownServiceCommand | undefined;
    commandResponse : ServiceCommandResponse | undefined;
    request : RequestEnvelope | PostEnvelope | undefined;
    response : ResponseEnvelope | undefined;

    get initialized() : boolean { return this.#initialized; }

    async init() : Promise<void>
    {
        this.listeningForCommands = false;
        this.listeningForCommandResponses = false;
        this.listeningForRequests = false;
        this.listeningForResponses = false;
        this.discoveryEnabled = false;
        this.listServicesCalled = false;
        this.teardownCalled = false;

        logger.info('Null backend initialized.');
        logger.warn('Using this backend is most likely not intentional.');

        this.#initialized = true;
    }

    async disableDiscovery() : Promise<void>
    {
        this.discoveryEnabled = false;
        logger.trace('disableDiscovery() called.');
    }

    async enableDiscovery(service : StrataService) : Promise<void>
    {
        this.discoveryEnabled = true;
        logger.trace(`enableDiscovery('<Service:${ service.serviceGroup }', '${ service.id }') called.`);
    }

    async listServices() : Promise<DiscoveredServices>
    {
        this.listServicesCalled = true;
        logger.trace('listServices() called.');
        return this.discoveredServices;
    }

    async listenForCommands(serviceGroup : string, serviceID : string) : Promise<void>
    {
        this.listeningForCommands = true;
        logger.trace(`listenForCommands('${ serviceGroup }', '${ serviceID }') called.`);
    }

    async listenForRequests(
        serviceGroup : string,
        serviceID : string,
        _waterMarkCheck : ConcurrencyCheck
    ) : Promise<void>
    {
        this.listeningForRequests = true;
        logger.trace(`listenForRequests('${ serviceGroup }', '${ serviceID }', <waterMarkCheck>)`);
    }

    async listenForResponses(clientID : string, clientName ?: string) : Promise<void>
    {
        this.listeningForResponses = true;
        logger.trace(`listenForResponses('${ clientID }', '${ clientName }')`);
    }

    async listenForCommandResponses(clientID : string) : Promise<void>
    {
        this.listeningForCommandResponses = true;
        logger.trace(`listenForCommandResponses('${ clientID }') called.`);
    }

    async sendCommand(target : string, command : KnownServiceCommand) : Promise<void>
    {
        this.command = command;
        logger.trace(`sendCommand('${ target }', '${ JSON.stringify(command, null, 4) }') called.`);
    }

    async sendCommandResponse(target : string, envelope : ServiceCommandResponse) : Promise<void>
    {
        this.commandResponse = envelope;
        logger.trace(`sendCommandResponse('${ target }', '${ JSON.stringify(envelope, null, 4) }') called.`);
    }

    async sendRequest(serviceGroup : string, envelope : RequestEnvelope | PostEnvelope) : Promise<void>
    {
        this.request = envelope;
        logger.trace(`sendRequest('${ serviceGroup }', '${ JSON.stringify(envelope, null, 4) }') called.`);
    }

    async sendResponse(request : StrataRequest) : Promise<void>
    {
        this.response = request.response ? request.renderResponse() : undefined;
        logger.trace(`sendResponse('${ request.responseQueue }', `
            + `${ JSON.stringify(request.renderRequest(), null, 4) }') called.`);
    }

    async stopListeningForCommands() : Promise<void>
    {
        this.listeningForCommands = false;
        logger.trace('stopListeningForCommands() called.');
    }

    async stopListeningForCommandResponses() : Promise<void>
    {
        this.listeningForCommandResponses = false;
        logger.trace('stopListeningForCommandResponses() called.');
    }

    async stopListeningForRequests(serviceGroup : string) : Promise<void>
    {
        this.listeningForRequests = false;
        logger.trace(`stopListeningForRequests('${ serviceGroup }') called.`);
    }

    async stopListeningForResponses() : Promise<void>
    {
        this.listeningForResponses = false;
        logger.trace('stopListeningForResponses() called.');
    }

    async teardown() : Promise<void>
    {
        this.listeningForCommands = false;
        this.listeningForRequests = false;
        this.listeningForResponses = false;
        this.discoveryEnabled = false;

        this.teardownCalled = true;

        logger.trace('teardown() called.');
    }
}

// ---------------------------------------------------------------------------------------------------------------------
