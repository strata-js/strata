// ---------------------------------------------------------------------------------------------------------------------
// RedisStreamsBackend
// ---------------------------------------------------------------------------------------------------------------------

import ld from 'lodash';
import { setTimeout } from 'node:timers/promises';
import { Redis } from 'ioredis';
import { logging } from '@strata-js/util-logging';

import redisRA from './redisRA.js';
import { BaseStrataBackend } from '../base.js';
import { AlreadyInitializedError } from '../../../lib/errors.js';
import { RedisDiscovery } from '../common/redisDiscovery.js';
import { StrataService } from '../../../clients/service.js';

// Interfaces
import { RedisBackendConfig } from '../../../interfaces/config.js';
import { DiscoveredServices } from '../../../interfaces/discovery.js';
import {
    ConcurrencyCheck,
    PostEnvelope,
    RequestEnvelope,
    ResponseEnvelope,
} from '../../../interfaces/messages.js';
import { KnownServiceCommand, ServiceCommandResponse } from '../../../interfaces/command.js';
import { BackendInfo } from '../../../interfaces/service.js';
import { StrataRequest } from '../../../lib/request.js';

// ---------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('redisStreamsBackend');

// ---------------------------------------------------------------------------------------------------------------------

type Handler<T> = (message : T) => void;
type ListenCheck = () => Promise<boolean>;
type EnforceQueueLengthFunc = () => void;

interface ListenDetails
{
    queue : string;
    redisClientID : string;
    consumerGroup : string;
    consumer : string;
}

export interface QueueConfig
{
    maxLength ?: number | {
        request ?: number,
        response ?: number
    };
    deleteOnAck ?: boolean | {
        request ?: boolean,
        response ?: boolean
    };
    trimWait ?: number;
    maxTrimWait ?: number;
}

export interface RedisStreamsBackendOptions extends RedisBackendConfig
{
    type : 'redis-streams';
    queue ?: QueueConfig;
}

export interface RedisStreamsBackendInfo extends BackendInfo
{
    type : 'redis-streams';
    redis : RedisBackendConfig['redis']
}

// ---------------------------------------------------------------------------------------------------------------------

export class RedisStreamsBackend extends BaseStrataBackend<RedisStreamsBackendOptions>
{
    // Redis connections
    #controlConn ?: Redis;
    #requestConn ?: Redis;
    #responseConn ?: Redis;
    #sendConn ?: Redis;
    #commConn ?: Redis;
    #commRespConn ?: Redis;

    #discovery ?: RedisDiscovery;

    #requestQueue ?: string;
    #responseQueue ?: string;

    // Store the details we need to stop listening properly
    #requestListenDetails ?: ListenDetails;
    #responseListenDetails ?: ListenDetails;

    // Configuration
    #config : RedisStreamsBackendOptions = { type: 'redis-streams', redis: {}, isolateCommands: true };

    #initialized = false;
    #listeningForCommands = false;
    #listeningForCommandResponses = false;
    #listeningForRequests = false;
    #listeningForResponses = false;
    #enforceRequestQueueLimits : EnforceQueueLengthFunc = () => { /* nothing */ };
    #enforceResponseQueueLimits : EnforceQueueLengthFunc = () => { /* nothing */ };

    #requestIDs = new Map<string, string>();
    #responseIDs = new Map<string, string>();

    //------------------------------------------------------------------------------------------------------------------
    // Properties
    //------------------------------------------------------------------------------------------------------------------

    get initialized() : boolean
    {
        return this.#initialized;
    }

    get info() : RedisStreamsBackendInfo
    {
        return {
            type: 'redis-streams',
            redis: this.#config.redis,
        };
    }

    get queueLimits() : { request ?: number, response ?: number }
    {
        const maxLength = this.#config.queue?.maxLength;
        if(typeof maxLength === 'object')
        {
            return {
                request: maxLength.request,
                response: maxLength.response,
            };
        }
        else
        {
            return { request: maxLength, response: maxLength };
        }
    }

    get deleteOnAck() : { request : boolean, response : boolean }
    {
        const deleteOnAck = this.#config.queue?.deleteOnAck;
        if(typeof deleteOnAck === 'object')
        {
            return {
                request: deleteOnAck.request ?? true,
                response: deleteOnAck.response ?? true,
            };
        }
        else
        {
            return { request: deleteOnAck ?? true, response: deleteOnAck ?? true };
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    // Internal helpers
    //------------------------------------------------------------------------------------------------------------------

    private _buildQueueName(prefix : string, serviceGroup : string) : string
    {
        return `${ prefix }:${ serviceGroup }`;
    }

    private async _enforceQueueLength(queue : string, conn ?: Redis, queueLimit = -1) : Promise<void>
    {
        if(conn && queueLimit >= 0)
        {
            return redisRA.trimQueue(conn, queue, queueLimit);
        }
    }

    private _parseJSON<T>(message : string) : T | undefined
    {
        try
        {
            return JSON.parse(message);
        }
        catch (ex)
        {
            if(ex instanceof SyntaxError)
            {
                logger.warn('Failed to parse message:', message);
                logger.debug('Parse error:', ex.stack);
            }
            else if(ex instanceof Error)
            {
                logger.warn('Unexpected error parsing message:', ex.message);
                logger.debug('Parse error:', ex.stack);
            }
            else
            {
                logger.error('Unexpected error parsing message:', ex);
            }
        }
    }

    private _handleCommand(_channel : string, message : string) : void
    {
        const parsed = this._parseJSON<KnownServiceCommand>(message);
        if(parsed)
        {
            this.emit('incomingCommand', parsed);
        }
    }

    private _handleCommandResponse(_channel : string, message : string) : void
    {
        const parsed = this._parseJSON<ServiceCommandResponse>(message);
        if(parsed)
        {
            this.emit('incomingCommandResponse', parsed);
        }
    }

    private _handleIncomingRequest(envelope : RequestEnvelope | PostEnvelope) : void
    {
        this.emit('incomingRequest', envelope);
    }

    private _handleIncomingResponse(envelope : ResponseEnvelope) : void
    {
        // First, we ack the message. Unlike requests, we ack this immediately.
        if(this.#sendConn && this.#responseQueue && this.#responseListenDetails)
        {
            // We don't bother waiting for this to succeed or fail it'll happen in its own time and that's fine.
            this._ackMessage(
                this.#sendConn,
                this.#responseQueue,
                this.#responseListenDetails.consumerGroup,
                { id: envelope.id, messageType: 'response' },
                this.#enforceResponseQueueLimits,
                this.deleteOnAck.response
            );
        }

        this.emit('incomingResponse', envelope);
    }

    private async _listenLoop<Envelope extends RequestEnvelope | PostEnvelope | ResponseEnvelope>(
        redisConn : Redis,
        queue : string,
        consumerGroup : string,
        consumer : string,
        handler : Handler<Envelope>,
        listenCheck : ListenCheck = async() => true,
        concurrencyCheck : ConcurrencyCheck = async() => true
    ) : Promise<void>
    {
        if(redisConn)
        {
            // Call the engine's loop
            if(redisConn.status === 'ready')
            {
                logger.trace('listenLoop called.');
                if(await concurrencyCheck())
                {
                    logger.trace('`concurrencyCheck()` check passed.');
                    const message = await redisRA.blockingRead(redisConn, queue, consumerGroup, consumer)
                        .catch(async(error) =>
                        {
                            logger.warn(
                                `Consumer group missing on queue '${ queue }'; recreating... some messages may be`
                                + ` reprocessed.`
                            );

                            // We _should_ use `error.code`, but that property is missing on these errors, due to a bug.
                            // @see https://github.com/luin/ioredis/issues/839
                            if(error.message.includes('NOGROUP'))
                            {
                                // Ensure the consumer group
                                await redisRA.ensureConsumerGroup(redisConn, queue, consumerGroup, '0');

                                // Create our consumer
                                await redisRA.createConsumer(redisConn, queue, consumerGroup, consumer);
                            }
                            else
                            {
                                throw error;
                            }
                        });

                    if(message)
                    {
                        // TODO: There's no enforcement here! We should have some validation the envelope is correctly
                        //  formatted! (This should be AVJ based.)
                        const envelope : Envelope = {
                            ...message.msg as Envelope,
                        };

                        if((message.msg as Envelope).messageType === 'response')
                        {
                            // Store the internal ID
                            this.#responseIDs.set(envelope.id, message.id);
                        }
                        else
                        {
                            // Store the internal ID
                            this.#requestIDs.set(envelope.id, message.id);
                        }

                        // Call the handler
                        handler(envelope);
                    }
                }
                else
                {
                    logger.trace('`concurrencyCheck()` check failed. Skipping pulling messages.');

                    // If we're too busy to work messages, let's wait for 50ms, and then try again.
                    await setTimeout(50);
                }
            }
            else
            {
                // If redis connection is not ready, slow down the listen loop
                await setTimeout(200);
            }

            // If we're still listening, we call ourselves recursively. We do _not_ await the promise.
            if(await listenCheck())
            {
                logger.trace('Calling _listenLoop again.');
                this._listenLoop<Envelope>(
                    redisConn,
                    queue,
                    consumerGroup,
                    consumer,
                    handler,
                    listenCheck,
                    concurrencyCheck
                );
            }
        }
    }

    private async _ackMessage(
        redisConn : Redis,
        queue : string,
        group : string,
        envelope : { id : string, messageType : string },
        enforceQueueLength ?: EnforceQueueLengthFunc,
        deleteMessage = false
    ) : Promise<void>
    {
        const redisID = envelope.messageType === 'response'
            ? this.#responseIDs.get(envelope.id) : this.#requestIDs.get(envelope.id);

        if(redisID)
        {
            await redisRA.ackMessage(redisConn, queue, group, redisID, deleteMessage);
        }
        else
        {
            logger.warn(`Attempted to ack message '${ envelope.id }' but no matching redis ID found.`);
        }

        // Delete the redis ID
        if(envelope.messageType === 'response')
        {
            this.#responseIDs.delete(envelope.id);
        }
        else
        {
            this.#requestIDs.delete(envelope.id);
        }

        // Enforce the queue length limit
        if(enforceQueueLength)
        {
            // The reason this is a void function is we don't want to wait for the trim to finish before resolving
            // this function.
            enforceQueueLength();
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    async init(config : RedisStreamsBackendOptions) : Promise<void>
    {
        if(this.#initialized)
        {
            throw new AlreadyInitializedError('RedisStreams Backend Initialization');
        }

        this.#config = {
            ...config,

            // Default to isolating commands
            isolateCommands: config.isolateCommands ?? true,
        };

        this.#controlConn = redisRA.getConnection(this.#config.redis, 'Control Connection');
        this.#sendConn = redisRA.getConnection(this.#config.redis, 'Send Connection');

        this.#listeningForCommands = false;
        this.#listeningForCommandResponses = false;
        this.#listeningForRequests = false;
        this.#listeningForResponses = false;

        this.#initialized = true;

        logger.info('Redis Streams backend initialized.');
    }

    async enableDiscovery(service : StrataService) : Promise<void>
    {
        logger.trace(`enableDiscovery('<Service:${ service.serviceGroup }', '${ service.id }>') called.`);

        this.#discovery = new RedisDiscovery(this.#config, service);

        // Start discovery heartbeat
        await this.#discovery.startDiscoveryHeartbeat();
    }

    async disableDiscovery() : Promise<void>
    {
        logger.trace('disableDiscovery() called.');

        if(this.#discovery)
        {
            // Stop discovery heartbeat
            await this.#discovery.teardown();
            this.#discovery = undefined;
        }
    }

    async listServices() : Promise<DiscoveredServices>
    {
        logger.trace('listServices() called.');
        if(!this.#discovery)
        {
            this.#discovery = new RedisDiscovery({ redis: this.#config.redis });
        }

        return this.#discovery.listServices();
    }

    async listenForCommands(serviceGroup : string, serviceID : string) : Promise<void>
    {
        logger.trace(`listenForCommands('${ serviceGroup }', '${ serviceID }') called.`);

        let servicesPrefix = 'Services';
        if(this.#config.isolateCommands)
        {
            const redisDB = this.#config.redis.db ?? 0;
            servicesPrefix = `Services-DB${ redisDB }`;
        }

        if(!this.#listeningForCommands)
        {
            const commandChannels = [
                `${ servicesPrefix }`,
                `${ servicesPrefix }:${ serviceGroup }`,
                `${ servicesPrefix }:${ serviceGroup }:${ serviceID }`,
            ];

            // Build Connection
            this.#commConn = redisRA.getConnection(this.#config.redis, 'Command Connection');

            // Prepare message handler
            this.#commConn.on('message', this._handleCommand.bind(this));

            // Subscribe to control commands
            await this.#commConn.subscribe(...commandChannels);

            // Set listening flag
            this.#listeningForCommands = true;

            logger.debug(`listenForCommands('${ serviceGroup }', '${ serviceID }') successfully called.`);
        }
        else
        {
            logger.warn(
                'Attempted to listen for commands, but was already listening. This is likely a (harmless) bug.'
            );
        }
    }

    async listenForCommandResponses(clientID : string) : Promise<void>
    {
        if(!this.#listeningForCommandResponses)
        {
            // Build Connection
            this.#commRespConn = redisRA.getConnection(this.#config.redis, 'Command Response Connection');

            // Prepare message handler
            this.#commRespConn.on('message', this._handleCommandResponse.bind(this));

            // Subscribe to control commands
            await this.#commRespConn.subscribe(`Clients:${ clientID }`);

            // Set listening flag
            this.#listeningForCommandResponses = true;

            logger.debug(`listenForCommandResponses('${ clientID }') successfully called.`);
        }
        else
        {
            logger.warn(
                'Attempted to listen for command responses, but was already listening. This is likely a (harmless) bug.'
            );
        }

        logger.trace(`listenForCommandResponses('${ clientID }') called.`);
    }

    async listenForRequests(
        serviceGroup : string,
        serviceID : string,
        waterMarkCheck : ConcurrencyCheck
    ) : Promise<void>
    {
        logger.trace(`listenForRequests('${ serviceGroup }', '${ serviceID }', <waterMarkCheck>) called.`);

        if(!this.#listeningForRequests)
        {
            // Build Connection
            this.#requestConn = redisRA.getConnection(this.#config.redis, 'Request Connection');

            const queueConfig : QueueConfig = this.#config.queue ?? {};
            const queue = this._buildQueueName('Requests', serviceGroup);

            // Ensure the consumer group
            await redisRA.ensureConsumerGroup(this.#requestConn, queue, serviceGroup);

            // Create our consumer
            await redisRA.createConsumer(this.#requestConn, queue, serviceGroup, serviceID);

            // Set this separately to keep Typescript happy.
            this.#requestQueue = queue;

            // Build our debounced request queue limit function
            this.#enforceRequestQueueLimits = ld.debounce(
                () => this._enforceQueueLength(queue, this.#controlConn, this.queueLimits.request),
                queueConfig.trimWait ?? 1000,
                {
                    leading: true,
                    maxWait: queueConfig.maxTrimWait ?? 5000,
                }
            );

            // Build listen details
            this.#requestListenDetails = {
                queue,
                consumerGroup: serviceGroup,
                consumer: serviceID,
                redisClientID: await redisRA.getClientID(this.#requestConn),
            };

            // Set listening flag
            this.#listeningForRequests = true;

            // Start the loop!
            this._listenLoop<RequestEnvelope | PostEnvelope>(
                this.#requestConn,
                queue,
                serviceGroup,
                serviceID,
                this._handleIncomingRequest.bind(this),
                async() => this.#listeningForRequests,
                waterMarkCheck
            );

            logger.debug(
                `listenForRequests('${ serviceGroup }', '${ serviceID }', <waterMarkCheck>) successfully called.`
            );
        }
        else
        {
            logger.warn(
                'Attempted to listen for requests, but was already listening. This is likely a (harmless) bug.'
            );
        }
    }

    async listenForResponses(clientID : string, clientName ?: string) : Promise<void>
    {
        logger.trace(`listenForResponses('${ clientID }', '${ clientName }') called.`);

        if(!this.#listeningForResponses)
        {
            // Build Connection
            this.#responseConn = redisRA.getConnection(this.#config.redis, 'Response Connection');

            const clientText = `${ clientName ? `${ clientName }:` : '' }${ clientID }`;
            const queueConfig : QueueConfig = this.#config.queue ?? {};
            const queue = this._buildQueueName('Responses', clientText);
            const consumerGroup = `Client:${ clientText }`;

            // Ensure the consumer group
            await redisRA.ensureConsumerGroup(this.#responseConn, queue, consumerGroup);

            // Create our consumer
            await redisRA.createConsumer(this.#responseConn, queue, consumerGroup, clientID);

            // Set this separately to keep Typescript happy.
            this.#responseQueue = queue;

            // Build our debounced response queue limit function
            this.#enforceResponseQueueLimits = ld.debounce(
                () => this._enforceQueueLength(queue, this.#controlConn, this.queueLimits.response),
                queueConfig.trimWait ?? 1000,
                {
                    leading: true,
                    maxWait: queueConfig.maxTrimWait ?? 5000,
                }
            );

            // Build listen details
            this.#responseListenDetails = {
                queue,
                consumerGroup,
                consumer: clientID,
                redisClientID: await redisRA.getClientID(this.#responseConn),
            };

            // Set listening flag
            this.#listeningForResponses = true;

            // Start the loop!
            this._listenLoop<ResponseEnvelope>(
                this.#responseConn,
                queue,
                consumerGroup,
                clientID,
                this._handleIncomingResponse.bind(this),
                async() => this.#listeningForResponses
            );

            logger.debug(`listenForResponses('${ clientID }', '${ clientName }') successfully called.`);
        }
        else
        {
            logger.warn(
                'Attempted to listen for responses, but was already listening. This is likely a (harmless) bug.'
            );
        }
    }

    async sendCommand(target : string, command : KnownServiceCommand) : Promise<void>
    {
        logger.trace(`sendCommand('${ target }', '${ JSON.stringify(command, null, 4) }') called.`);

        // If we're isolating commands, we need to rewrite the 'Services' portion of the target to include the
        // DB number.
        if(this.#config.isolateCommands)
        {
            const redisDB = this.#config.redis.db ?? 0;

            // Make sure the user didn't already do this substitution
            if(!/^Services-DB\d/.test(target))
            {
                target = target.replace(/^Services/, `Services-DB${ redisDB }`);

                logger.trace(`Rewrote target to include DB number: '${ target }'.`);
            }
        }

        if(this.#controlConn)
        {
            await redisRA.publish(this.#controlConn, target, JSON.stringify(command));
        }
        else
        {
            logger.warn('Attempting to send command, but control connection is missing.');
        }
    }

    async sendCommandResponse(target : string, envelope : ServiceCommandResponse) : Promise<void>
    {
        logger.trace(`sendCommandResponse('${ JSON.stringify(envelope, null, 4) }') called.`);

        if(this.#controlConn)
        {
            await redisRA.publish(this.#controlConn, target, JSON.stringify(envelope));
        }
        else
        {
            logger.warn('Attempting to send command, but control connection is missing.');
        }
    }

    async sendRequest(serviceGroup : string, envelope : RequestEnvelope | PostEnvelope) : Promise<void>
    {
        logger.trace(`sendRequest('${ serviceGroup }', '${ JSON.stringify(envelope, null, 4) }') called.`);

        if(this.#sendConn)
        {
            const sendQueue = this._buildQueueName('Requests', serviceGroup);

            // Add responseQueue
            if(envelope.messageType === 'request')
            {
                envelope.responseQueue = this.#responseQueue;
            }

            // Send the request
            await redisRA.insertMessage(this.#sendConn, sendQueue, { ...envelope });
        }
        else
        {
            logger.error(`Attempted to send request, but there is no connection.`);
        }
    }

    async sendResponse(request : StrataRequest) : Promise<void>
    {
        logger.trace(`sendResponse('${ JSON.stringify(request.renderRequest(), null, 4) }') called.`);

        if(request.messageType === 'request')
        {
            if(!request.responseQueue)
            {
                logger.warn(`Ignoring attempt to respond to request '${ request.id }'; no response queue.`);
            }
            else if(this.#sendConn)
            {
                // Send the response
                await redisRA.insertMessage(this.#sendConn, request.responseQueue, { ...request.renderResponse() });
            }
            else
            {
                logger.error(`Attempted to send response, but there is no connection, or we're not listening.`);
            }
        }
        if(this.#sendConn && this.#requestQueue && this.#requestListenDetails)
        {
            // We ack the original request message, not the response. That is why all the arguments here are dealing
            // with the request details, not the response ones.
            await this._ackMessage(
                this.#sendConn,
                this.#requestQueue,
                this.#requestListenDetails.consumerGroup,
                { id: request.id, messageType: 'request' },
                this.#enforceRequestQueueLimits,
                this.deleteOnAck.request
            );
        }
    }

    async stopListeningForCommands() : Promise<void>
    {
        logger.trace('stopListeningForCommands() called.');

        if(this.#commConn)
        {
            this.#commConn.punsubscribe('Services*');
        }

        this.#listeningForCommands = false;

        logger.debug('stopListeningForCommands() complete.');
    }

    async stopListeningForCommandResponses() : Promise<void>
    {
        logger.trace('stopListeningForCommands() called.');
        if(this.#commRespConn)
        {
            this.#commRespConn.punsubscribe('Clients*');
        }

        this.#listeningForCommands = false;

        logger.debug('stopListeningForCommands() complete.');
    }

    async stopListeningForRequests(serviceGroup : string) : Promise<void>
    {
        logger.trace(`stopListeningForRequests('${ serviceGroup }') called.`);

        // Stop listening
        this.#listeningForRequests = false;

        // If we have listen details, we attempt to clean up.
        if(this.#requestListenDetails && this.#controlConn)
        {
            // Unblock the listen connection
            await redisRA.unblock(this.#controlConn, this.#requestListenDetails.redisClientID);

            // Delete the consumer
            await redisRA.deleteConsumer(
                this.#controlConn,
                this.#requestListenDetails.queue,
                this.#requestListenDetails.consumerGroup,
                this.#requestListenDetails.consumer
            );
        }

        logger.debug('stopListeningForRequests() complete.');
    }

    async stopListeningForResponses() : Promise<void>
    {
        logger.trace('stopListeningForResponses() called.');

        // Stop listening
        this.#listeningForResponses = false;

        // If we have listen details, we attempt to clean up.
        if(this.#responseListenDetails && this.#controlConn)
        {
            // Unblock the listen connection
            await redisRA.unblock(this.#controlConn, this.#responseListenDetails.redisClientID);

            // Delete the consumer
            await redisRA.deleteConsumer(
                this.#controlConn,
                this.#responseListenDetails.queue,
                this.#responseListenDetails.consumerGroup,
                this.#responseListenDetails.consumer
            );
        }

        logger.debug('stopListeningForResponses() complete.');
    }

    async teardown() : Promise<void>
    {
        logger.trace('teardown() called.');

        if(this.#listeningForRequests)
        {
            // The parameters don't matter here, as we don't use them to stop listening.
            await this.stopListeningForRequests('');
        }

        if(this.#listeningForResponses)
        {
            await this.stopListeningForResponses();
        }

        if(this.#listeningForCommands)
        {
            await this.stopListeningForCommands();
        }

        if(this.#listeningForCommandResponses)
        {
            await this.stopListeningForCommandResponses();
        }

        if(this.#discovery)
        {
            await this.disableDiscovery();
        }

        // Disconnect from Redis
        redisRA.disconnect(this.#controlConn);
        redisRA.disconnect(this.#requestConn);
        redisRA.disconnect(this.#responseConn);
        redisRA.disconnect(this.#sendConn);
        redisRA.disconnect(this.#commConn);

        // Reset state
        this.#controlConn = undefined;
        this.#requestConn = undefined;
        this.#responseConn = undefined;
        this.#sendConn = undefined;
        this.#commConn = undefined;
        this.#requestListenDetails = undefined;
        this.#responseListenDetails = undefined;
        this.#requestQueue = undefined;
        this.#responseQueue = undefined;
        this.#initialized = false;

        this.#config = { type: 'redis-streams', redis: {} };
        this.#enforceRequestQueueLimits = () => { /* nothing */ };
        this.#enforceResponseQueueLimits = () => { /* nothing */ };

        logger.debug('teardown() complete.');
    }
}

// ---------------------------------------------------------------------------------------------------------------------
