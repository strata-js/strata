//----------------------------------------------------------------------------------------------------------------------
// RedisStreamsResourceAccess
//----------------------------------------------------------------------------------------------------------------------

import { Redis, RedisOptions } from 'ioredis';
import { logging } from '@strata-js/util-logging';

// Interfaces
import { ConsumerDetails, QueueDetails, ServiceGroupDetails } from '../../../interfaces/discovery.js';
import { promiseTimeout } from '../../../utils/timeout.js';
import { InternalTimeoutError } from '../../../lib/errors.js';

import { RedisResourceAccess } from '../common/redis.js';

//----------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('redisRA');

//----------------------------------------------------------------------------------------------------------------------

class RedisStreamsResourceAccess
{
    #redisRA = new RedisResourceAccess();

    //------------------------------------------------------------------------------------------------------------------

    public getConnection(connectionConfig : RedisOptions, connectionName ?: string) : Redis
    {
        return this.#redisRA.getConnection(connectionConfig, connectionName);
    }

    public async ensureConsumerGroup(conn : Redis, key : string, group : string, startID = '$') : Promise<void>
    {
        await conn.xgroup('CREATE', key, group, startID, 'MKSTREAM')
            .catch((err) =>
            {
                // FIXME: We _should_ be able to look for a `code` property on the error object, but that doesn't seem
                // to work. So instead, we look for it in the message text.
                // @see https://github.com/luin/ioredis/issues/839
                if(!err.message.includes('BUSYGROUP'))
                {
                    throw err;
                }
            });
    }

    public async createConsumer(conn : Redis, key : string, group : string, consumer : string) : Promise<void>
    {
        await conn.xgroup('CREATECONSUMER', key, group, consumer);
    }

    public async deleteConsumer(conn : Redis, key : string, group : string, consumer : string) : Promise<void>
    {
        await conn.xgroup('DELCONSUMER', key, group, consumer);
    }

    public disconnect(conn ?: Redis) : void
    {
        return this.#redisRA.disconnect(conn);
    }

    public async getClientID(conn : Redis) : Promise<string>
    {
        return this.#redisRA.getClientID(conn);
    }

    public async listQueues(conn : Redis) : Promise<string[]>
    {
        return new Promise((resolve, reject) =>
        {
            const results = new Set<string>();
            const scanStream = conn.scanStream({ type: 'stream' });

            scanStream.on('data', (resultKeys) =>
            {
                resultKeys.forEach((key) => results.add(key));
            });

            scanStream.on('error', (error) => reject(error));
            scanStream.on('end', () => resolve([ ...results ]));
        });
    }

    public async listQueueDetails(conn : Redis, key : string) : Promise<QueueDetails>
    {
        const details : any = await conn.xinfo('STREAM', key);
        const detailsObj = {};
        details.forEach((keyOrValue : string | number, index : number) =>
        {
            if(index % 2 === 0)
            {
                detailsObj[keyOrValue] = details[index + 1];
            }
        });

        return detailsObj as QueueDetails;
    }

    public async listGroups(conn : Redis, key : string) : Promise<ServiceGroupDetails[]>
    {
        const results : any = await conn.xinfo('GROUPS', key);
        return results
            .filter((group : string[]) =>
            {
                const nameIdx = group.findIndex((item) => item.toLowerCase() === 'name');
                return nameIdx === -1 || !(group[nameIdx + 1].startsWith('$') && group[nameIdx + 1].endsWith('$'));
            })
            .map((group : string[]) =>
            {
                const groupObj = {};
                group.forEach((keyOrValue, index) =>
                {
                    if(index % 2 === 0)
                    {
                        groupObj[keyOrValue] = group[index + 1];
                    }
                });

                return groupObj;
            });
    }

    public async listConsumers(conn : Redis, key : string, group : string) : Promise<ConsumerDetails[]>
    {
        const results : any = await conn.xinfo('CONSUMERS', key, group);
        return results
            .filter((consumer : string[]) =>
            {
                const nameIdx = consumer.findIndex((item) => item.toLowerCase() === 'name');
                return nameIdx === -1
                    || !(consumer[nameIdx + 1].startsWith('$') && consumer[nameIdx + 1].endsWith('$'));
            })
            .map((consumer : string[]) =>
            {
                const consumerObj = {};
                consumer.forEach((keyOrValue, index) =>
                {
                    if(index % 2 === 0)
                    {
                        consumerObj[keyOrValue] = consumer[index + 1];
                    }
                });

                return consumerObj;
            });
    }

    public async blockingRead(
        conn : Redis,
        key : string,
        group : string,
        consumer : string
    ) : Promise<{ id : string, msg : Record<string, unknown> } | undefined>
    {
        const blockTimeout = 2000;

        logger.trace('Starting a blocking read...');

        // We have to do our own timeout, because ioredis doesn't always come back after a reconnect.
        const results : [string, string[]][] | undefined = await promiseTimeout(
            conn.xreadgroup(
                'GROUP',
                group,
                consumer,
                'COUNT',
                1,
                'BLOCK',
                blockTimeout,
                'STREAMS',
                key,
                '>'
            ) as Promise<[string, string[]][]>,
            blockTimeout + 200
        )
            .catch((err) =>
            {
                if(err instanceof InternalTimeoutError)
                {
                    logger.trace('Internal timeout waiting for blocking read to return.');
                    return undefined;
                }

                throw err;
            });

        logger.trace('Blocking read finished.');

        if(results)
        {
            // This assumes multiple streams, but we don't support that, so pull out the actual message.
            const result = results[0][1][0];

            // The format of result is: `[ MessageID, [ Key, Value ] ]`, and the format we want to return is:
            // `{ id: MessageID, msg: Value }`
            return {
                id: result[0],
                msg: JSON.parse(result[1][1]),
            };
        }
    }

    public async insertMessage(conn : Redis, key : string, message : Record<string, unknown>) : Promise<string | null>
    {
        const messageJSON = JSON.stringify(message);
        return conn.xadd(key, '*', 'msg', messageJSON);
    }

    public async ackMessage(
        conn : Redis,
        key : string,
        group : string,
        messageID : string,
        deleteMessage = false
    ) : Promise<void>
    {
        await conn.xack(key, group, messageID);

        if(deleteMessage)
        {
            await this.delMessage(conn, key, messageID);
        }
    }

    public async delMessage(conn : Redis, key : string, messageID : string) : Promise<void>
    {
        await conn.xdel(key, messageID);
    }

    public async trimQueue(conn : Redis, key : string, limit : number) : Promise<void>
    {
        await conn.xtrim(key, 'MAXLEN', '~', `${ limit }`);
    }

    public async unblock(conn : Redis, name : string) : Promise<void>
    {
        await this.#redisRA.unblock(conn, name);
    }

    public async publish(conn : Redis, channel : string, message : string) : Promise<void>
    {
        await this.#redisRA.publish(conn, channel, message);
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new RedisStreamsResourceAccess();

//----------------------------------------------------------------------------------------------------------------------
