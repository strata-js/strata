//----------------------------------------------------------------------------------------------------------------------
// Redis Resource Access
//----------------------------------------------------------------------------------------------------------------------

import { Redis, RedisOptions } from 'ioredis';
import { logging } from '@strata-js/util-logging';

//----------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('redisRA');

//----------------------------------------------------------------------------------------------------------------------

export class RedisResourceAccess
{
    private _handleRedisConnectionEnd() : void
    {
        logger.warn('Redis connection ended. (Will no longer retry.)');
        throw new Error('Redis connection ended. (Will no longer retry.)');
    }

    //------------------------------------------------------------------------------------------------------------------

    getConnection(connectionConfig : RedisOptions, connectionName ?: string) : Redis
    {
        const maxTime = 15000;

        const redisConn = new Redis({
            // Defaults
            maxRetriesPerRequest: null,
            keepAlive: 180000, // enable keepAlive and set the idleDelay to 3 minutes

            // Config
            ...connectionConfig,

            // Overrides
            retryStrategy(times)
            {
                // If it's a hiccup, reconnect immediately
                if(times <= 1)
                {
                    return 0;
                }

                // Make sure `delay` is less than `maxTime`.
                return Math.min((0.1 * (1.2 ^ times)) * 1000, maxTime);
            },
            connectionName,
        });

        // Connect to redis events
        redisConn.on('ready', () =>
        {
            logger.debug(`[${ redisConn.options.connectionName }] Redis is connected and server reports ready.`);
        });

        redisConn.on('close', () =>
        {
            if(redisConn.status !== 'reconnecting')
            {
                logger.warn(`[${ redisConn.options.connectionName }] Redis connection closed.`);
            }
        });

        redisConn.on('end', this._handleRedisConnectionEnd);

        redisConn.on('error', (error) =>
        {
            // @ts-expect-error The ioredis types define this as an Error, the code adds this additional property
            if(error.syscall === 'connect')
            {
                logger.error(`[${ redisConn.options.connectionName }] Redis connection error:`, error.message);
            }
            else
            {
                logger.error(`[${ redisConn.options.connectionName }] Redis error:`, error.message);
            }
        });

        return redisConn;
    }

    disconnect(conn ?: Redis) : void
    {
        if(conn)
        {
            conn.off('end', this._handleRedisConnectionEnd);
            conn.disconnect();

            logger.debug(`[${ conn.options.connectionName }] Redis disconnected.`);
        }
    }

    async getClientID(conn : Redis) : Promise<string>
    {
        return (await conn.client('ID')).toString();
    }

    async unblock(conn : Redis, name : string) : Promise<void>
    {
        logger.trace('Unblocking redis client:', name);
        await conn.client('UNBLOCK', name);
    }

    async publish(conn : Redis, channel : string, message : string) : Promise<void>
    {
        await conn.publish(channel, message);
    }
}

//----------------------------------------------------------------------------------------------------------------------
