//----------------------------------------------------------------------------------------------------------------------
// Redis Discovery Logic
//----------------------------------------------------------------------------------------------------------------------

import { setTimeout } from 'node:timers/promises';
import { Redis, RedisOptions } from 'ioredis';
import { logging } from '@strata-js/util-logging';

// Interfaces
import { StrataService } from '../../../clients/service.js';
import { RedisBackendConfig } from '../../../interfaces/config.js';
import { DiscoveredService, DiscoveredServices } from '../../../interfaces/discovery.js';

// Resource Access
import { RedisResourceAccess } from './redis.js';

//----------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('redisDiscovery');
type RedisDiscoveryConfig = Pick<RedisBackendConfig, 'discovery' | 'redis'>;

type JSONValue =
    | string
    | number
    | boolean
    | { [x : string] : JSONValue }
    | JSONValue[];

//----------------------------------------------------------------------------------------------------------------------

export class RedisDiscovery
{
    #redisClient ?: Redis;
    #service ?: StrataService;
    #discoveryEnabled = false;

    // Service info heartbeat expiration (in seconds)
    #heartbeatTTL = 5 * 60; // 5 minutes

    // Service info heartbeat interval (in seconds)
    #heartbeatInterval = 60; // 1 minute

    // Redis DB for service discovery
    #heartbeatDB = 0;

    #redisRA = new RedisResourceAccess();
    #config : RedisDiscoveryConfig;

    get redisConfig() : RedisOptions
    {
        return this.#config.redis;
    }

    //------------------------------------------------------------------------------------------------------------------
    // Private
    //------------------------------------------------------------------------------------------------------------------

    _getServiceKey(service : StrataService) : string
    {
        return `Strata:Services:${ service.serviceGroup }:${ service.id }`;
    }

    async _logService(service : StrataService) : Promise<void>
    {
        if(this.#redisClient)
        {
            const key = this._getServiceKey(service);

            // Make sure we're on the right db
            await this.#redisClient.select(this.#heartbeatDB);

            // Set the key
            await this.#redisClient.hset(key, {
                ...service.info,
                backend: JSON.stringify(service.info.backend),
                contexts: JSON.stringify(service.contexts),
                lastBusy: JSON.stringify(service.lastBusy),
                lastSeen: (new Date()).toISOString(),
            });

            // Set the TTL to our interval
            await this.#redisClient.expire(key, this.#heartbeatTTL);
        }
    }

    async _heartbeatLoop(service : StrataService) : Promise<void>
    {
        await this._logService(service);

        if(this.#discoveryEnabled)
        {
            // Wait for the interval
            await setTimeout(this.#heartbeatInterval * 1000);

            // Call the loop again
            // Note: We intentionally do no call `await` here, otherwise we'd overrun the stack.
            this._heartbeatLoop(service);
        }
    }

    _parseJSON<T extends JSONValue>(value : string) : T | undefined
    {
        try
        {
            return JSON.parse(value) as T;
        }
        catch (err : any)
        {
            logger.error(`Failed to parse JSON:`, err.stack);
            return undefined;
        }
    }

    _parseLastBusy(lastBusyStr : string) : { time : Date, lag : number }
    {
        const lastBusy = {
            time: new Date(),
            lag: -1,
        };

        try
        {
            const lastBusyObj = this._parseJSON<Record<string, JSONValue>>(lastBusyStr);
            lastBusy.time = new Date(lastBusyObj?.time as string);

            const lag = lastBusyObj?.lag;
            if(typeof lag === 'number')
            {
                lastBusy.lag = lag;
            }
            else if(typeof lag === 'string')
            {
                lastBusy.lag = parseInt(lag);
            }
            else
            {
                lastBusy.lag = -1;
            }
        }
        catch (err : any)
        {
            logger.error(`Failed to parse lastBusy:`, err.stack);
        }

        return lastBusy;
    }

    async _parseServiceInfoV1(serviceInfo : Record<string, string>) : Promise<DiscoveredService>
    {
        const info = {
            id: serviceInfo.id,
            serviceGroup: serviceInfo.serviceGroup,
            serviceName: serviceInfo.serviceName,
            name: serviceInfo.name,
            version: serviceInfo.version,
            strataVersion: serviceInfo.strataVersion,
            hostname: serviceInfo.hostname,
            environment: serviceInfo.environment,
            queue: serviceInfo.queue,
            lastSeen: serviceInfo.lastSeen,
        };

        // Create false backend object
        const backend = {
            type: 'redis-streams',
            redis: {
                // We can lie about the host and port, since it has to be connecting to this redis for us to see it.
                host: this.#config.redis.host,
                port: this.#config.redis.port,

                // Pull out the DB since it's in the v1 service info
                db: serviceInfo.db ?? 0,
            },
            requestPrefix: serviceInfo.requestPrefix ?? 'Request',
        };

        return {
            ...info,
            concurrency: this._parseJSON(serviceInfo.concurrency) ?? -1,
            outstanding: this._parseJSON(serviceInfo.outstanding) ?? -1,
            contexts: this._parseJSON(serviceInfo.contexts) ?? {},
            lastBusy: this._parseLastBusy(serviceInfo.lastBusy),
            backend,
        };
    }

    async _parseServiceInfoV2(serviceInfo : Record<string, string>) : Promise<DiscoveredService>
    {
        return {
            id: serviceInfo.id,
            serviceGroup: serviceInfo.serviceGroup,
            serviceName: serviceInfo.serviceName,
            version: serviceInfo.version,
            strataVersion: serviceInfo.strataVersion,
            hostname: serviceInfo.hostname,
            environment: serviceInfo.environment,
            lastSeen: serviceInfo.lastSeen,
            concurrency: parseInt(serviceInfo.concurrency),
            outstanding: parseInt(serviceInfo.outstanding),
            lastBusy: this._parseLastBusy(serviceInfo.lastBusy),
            contexts: this._parseJSON(serviceInfo.contexts) ?? {},
            backend: this._parseJSON(serviceInfo.backend) ?? { type: 'unknown' },
        };
    }

    async _parseServiceInfo(serviceInfo : Record<string, string>) : Promise<DiscoveredService>
    {
        if(serviceInfo.strataVersion.startsWith('1'))
        {
            return this._parseServiceInfoV1(serviceInfo);
        }
        else
        {
            return this._parseServiceInfoV2(serviceInfo);
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    constructor(config : RedisDiscoveryConfig, service ?: StrataService)
    {
        this.#service = service;
        this.#config = config;
        this.#discoveryEnabled = false;

        this.#heartbeatTTL = config.discovery?.ttl ?? this.#heartbeatTTL;
        this.#heartbeatInterval = config.discovery?.interval ?? this.#heartbeatInterval;
        this.#heartbeatDB = config.discovery?.db ?? config.redis.db ?? this.#heartbeatDB;
    }

    async startDiscoveryHeartbeat() : Promise<void>
    {
        logger.trace(`Starting discovery heartbeat for service '${ this.#service?.id ?? 'unknown' }'.`);

        if(!this.#service)
        {
            throw new Error('Cannot start discovery heartbeat without a service!');
        }

        if(!this.#redisClient)
        {
            this.#redisClient = this.#redisRA.getConnection(this.redisConfig, 'Discovery Connection');
        }

        this.#discoveryEnabled = true;

        // Start the heartbeat loop
        this._heartbeatLoop(this.#service);
    }

    async stopDiscoveryHeartbeat() : Promise<void>
    {
        logger.trace(`Stopping discovery heartbeat for service '${ this.#service?.id ?? 'unknown' }'.`);

        if(this.#redisClient)
        {
            if(this.#service)
            {
                // Delete service entry
                const key = this._getServiceKey(this.#service);
                await this.#redisClient.del(key);
            }
        }

        this.#redisRA.disconnect(this.#redisClient);
        this.#redisClient = undefined;

        this.#discoveryEnabled = false;
    }

    async listServices() : Promise<DiscoveredServices>
    {
        if(!this.#redisClient)
        {
            this.#redisClient = this.#redisRA.getConnection(this.redisConfig, 'Discovery Connection');
        }

        // Select the right DB
        await this.#redisClient.select(this.#heartbeatDB);

        const keys = await this.#redisClient.keys('Strata:Services:*');
        const services : (DiscoveredService | undefined)[] = await Promise.all(keys.map(async(key) =>
        {
            if(this.#redisClient)
            {
                const serviceInfo = await this.#redisClient.hgetall(key);
                if(serviceInfo)
                {
                    return this._parseServiceInfo(serviceInfo);
                }
            }

            return undefined;
        }));

        return services.reduce((accum, service) =>
        {
            if(service)
            {
                if(!accum[service.serviceGroup])
                {
                    accum[service.serviceGroup] = {};
                }

                accum[service.serviceGroup][service.id] = service;
            }

            return accum;
        }, {});
    }

    async teardown() : Promise<void>
    {
        logger.debug(`Tearing down RedisDiscovery for service '${ this.#service?.id ?? 'unknown' }'.`);

        // Just stop the heartbeat
        await this.stopDiscoveryHeartbeat();
    }
}

//----------------------------------------------------------------------------------------------------------------------
