// ---------------------------------------------------------------------------------------------------------------------
// Base Strata Backend
// ---------------------------------------------------------------------------------------------------------------------

import { TypedEmitter } from '../../lib/emitter.js';

// Interfaces
import { KnownServiceCommand, ServiceCommandResponse } from '../../interfaces/command.js';
import {
    ConcurrencyCheck,
    PostEnvelope,
    RequestEnvelope,
    ResponseEnvelope,
} from '../../interfaces/messages.js';
import { DiscoveredServices } from '../../interfaces/discovery.js';
import { BackendConfig } from '../../interfaces/config.js';
import { StrataService } from '../../clients/service.js';
import { BackendInfo } from '../../interfaces/service.js';
import { StrataRequest } from '../../lib/request.js';

// ---------------------------------------------------------------------------------------------------------------------

export interface StrataBackendEvents
{
    incomingCommand : (command : KnownServiceCommand) => void;
    incomingCommandResponse : (response : ServiceCommandResponse) => void;
    incomingRequest : (request : RequestEnvelope | PostEnvelope) => void;
    incomingResponse : (response : ResponseEnvelope) => void;
}

// ---------------------------------------------------------------------------------------------------------------------

export abstract class BaseStrataBackend<Config extends BackendConfig> extends TypedEmitter<StrataBackendEvents>
{
    config ?: Config;

    get info() : BackendInfo { return { type: this.config?.type ?? 'unknown' }; }
    // eslint-disable-next-line @typescript-eslint/class-literal-property-style
    get initialized() : boolean { return false; }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Initialize the backend. In Derived classes, all setup for the backend should happen here, including making
     * network connections or initializing internal state.
     *
     * @param config - Configuration options for the backend.
     */
    abstract init(config ?: Config) : Promise<void>;

    // -----------------------------------------------------------------------------------------------------------------
    // Request Handling
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Start listening for incoming requests. When a request comes in, the backend should emit an `incomingRequest`
     * event.
     *
     * @param serviceGroup - The service group this service is a member of.
     * @param serviceID - The unique ID of this service.
     * @param waterMarkCheck - A function that tells the service if we need to pause pulling requests or not.
     */
    abstract listenForRequests(
        serviceGroup : string,
        serviceID : string,
        waterMarkCheck : ConcurrencyCheck
    ) : Promise<void>;

    /**
     * Start listening for incoming responses.
     *
     * @param clientID - The id of the client being responded to.
     * @param clientName - The name of the client being responded to.
     */
    abstract listenForResponses(clientID : string, clientName ?: string) : Promise<void>;

    /**
     * Stop listening for Requests to the given service group
     *
     * @param serviceGroup - The service group to stop listening to requests from.
     */
    abstract stopListeningForRequests(serviceGroup : string) : Promise<void>;

    /**
     * Stop listening for responses to our outstanding requests.
     */
    abstract stopListeningForResponses() : Promise<void>;

    /**
     * Send a request or a post to another service.
     *
     * @param serviceGroup - The service group this service is a member of.
     * @param envelope - The request to send.
     * @param clientID - The unique ID of the client who sent the request. If not supplied, the message is assumed to
     *  be a post and not require a reply.
     */
    abstract sendRequest(
        serviceGroup : string,
        envelope : RequestEnvelope | PostEnvelope,
        clientID ?: string
    ) : Promise<void>;

    /**
     * Send a response to a request to the client who made the request.
     *
     * @param request - The strata request.
     */
    abstract sendResponse(request : StrataRequest) : Promise<void>;

    // -----------------------------------------------------------------------------------------------------------------
    // Commands
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Start listening for commands. The serviceGroup and serviceID are passed in so the backend can know what group
     * and id to listen for.
     *
     * @param serviceGroup - The service group this service is a member of.
     * @param serviceID - The unique ID of this service.
     */
    abstract listenForCommands(serviceGroup : string, serviceID : string) : Promise<void>;

    /**
     * Stop listening for incoming commands.
     */
    abstract stopListeningForCommands() : Promise<void>;

    /**
     * Listen for command replies. This is used by the client to listen for replies to commands it has sent.
     *
     * @param clientID - The unique ID of the client who sent the command.
     */
    abstract listenForCommandResponses(clientID : string) : Promise<void>;

    /**
     * Stop listening for command responses.
     */
    abstract stopListeningForCommandResponses() : Promise<void>;

    /**
     * Send a service command. The target for the command can be all services (i.e. `Services`), a service group
     * (i.e. `Services:<ServiceGroup>`) or an individual service (i.e. `Services:<ServiceGroup>:<ServiceID>`).
     *
     * @param target - The target for the command. (Of the form `Services:<ServiceGroup>:<ServiceID>`.)
     * @param command - The service command to send.
     */
    abstract sendCommand(target : string, command : KnownServiceCommand) : Promise<void>;

    /**
     * Send a command response to the client who sent the command.
     *
     * @param target - The target for the command. (Of the form `Services:<ServiceGroup>:<ServiceID>`.)
     * @param envelope - The command response to send.
     */
    abstract sendCommandResponse(target : string, envelope : ServiceCommandResponse) : Promise<void>;

    // -----------------------------------------------------------------------------------------------------------------
    // Discovery
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Enables the ability for this service to be discovered, and adds its information to the discovery results.
     *
     * @param service - The service to enable discovery for.
     */
    abstract enableDiscovery(service : StrataService) : Promise<void>;

    /**
     * Disables the ability for this service to be discovered, and removed its information from the discovery results.
     */
    abstract disableDiscovery() : Promise<void>;

    /**
     * List known/discoverable services.
     */
    abstract listServices() : Promise<DiscoveredServices>;

    // -----------------------------------------------------------------------------------------------------------------
    // Cleanup
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Tear down the backend. In Derived classes, all cleanup for the backend should happen here, including closing
     * network connections or cleaning up internal state.
     */
    abstract teardown() : Promise<void>;
}

// ---------------------------------------------------------------------------------------------------------------------
