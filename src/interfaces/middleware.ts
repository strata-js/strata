// ---------------------------------------------------------------------------------------------------------------------
// Middleware Interface
// ---------------------------------------------------------------------------------------------------------------------

import { StrataRequest } from '../lib/request.js';

// ---------------------------------------------------------------------------------------------------------------------

export interface OperationMiddleware<
    PayloadType = Record<string, unknown>,
    MetadataType = Record<string, unknown>,
    ResponsePayloadType = Record<string, unknown>,
>
{
    beforeRequest : (request : StrataRequest<PayloadType, MetadataType, ResponsePayloadType>) =>
    Promise<StrataRequest<PayloadType, MetadataType, ResponsePayloadType> | undefined>;

    success ?: (request : StrataRequest<PayloadType, MetadataType, ResponsePayloadType>) =>
    Promise<StrataRequest<PayloadType, MetadataType, ResponsePayloadType> | undefined>;

    failure ?: (request : StrataRequest<PayloadType, MetadataType, ResponsePayloadType>) =>
    Promise<StrataRequest<PayloadType, MetadataType, ResponsePayloadType> | undefined>;

    teardown ?: () => Promise<void>;
}

// ---------------------------------------------------------------------------------------------------------------------
