// ---------------------------------------------------------------------------------------------------------------------
// Context Interfaces
// ---------------------------------------------------------------------------------------------------------------------

type NotType<T, E, M> = T extends E ? M : T;

export type IsValidPayloadType<T> = NotType<T, unknown[], 'payload must be an object'>
    & NotType<T, string, 'payload must be an object'>
    & NotType<T, Buffer, 'payload must be an object'>
    & NotType<T, number, 'payload must be an object'>
    & NotType<T, boolean, 'payload must be an object'>
    & NotType<T, (...args : any[]) => any, 'payload must be an object'>
    & NotType<T, symbol, 'payload must be an object'>;

// ---------------------------------------------------------------------------------------------------------------------

export type OperationHandler<PayloadReturnType> = (...args : any[]) => Promise<IsValidPayloadType<PayloadReturnType>>;

// ---------------------------------------------------------------------------------------------------------------------
