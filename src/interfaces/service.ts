//----------------------------------------------------------------------------------------------------------------------
// Service interfaces
//----------------------------------------------------------------------------------------------------------------------

export interface BackendInfo
{
    type : string;
    [key : string] : any;
}

export interface ServiceInfo
{
    serviceName : string;
    serviceGroup : string;
    environment : string;
    hostname : string;
    id : string;
    version : string;
    strataVersion : string;
    concurrency : number;
    outstanding : number;
    lastBusy : { time : Date, lag : number };
    contexts : Record<string, string[]>;
    backend : BackendInfo;

    // @deprecated - This is a legacy field that is required for backwards compatibility with v1 clients.
    name ?: string;

    // @deprecated - This is a legacy field that is required for backwards compatibility with v1 clients.
    queue ?: string;
}

//----------------------------------------------------------------------------------------------------------------------
