// ---------------------------------------------------------------------------------------------------------------------
// Operation LocalStorage
// ---------------------------------------------------------------------------------------------------------------------

import { ResponseMessage } from './messages.js';
import { StrataRequest } from '../lib/request.js';

// ---------------------------------------------------------------------------------------------------------------------

export interface LocalStorage
{
    currentRequestID : string;
    currentRequest : StrataRequest;
    // TODO: We should define this better. Maybe some way for the service to specify a type for user information?
    user ?: Record<string, unknown>;
    messages : ResponseMessage[];

    // Allow extra properties to be accessed
    [ key : string ] : unknown;
}

export type RunFunction = () => Promise<unknown>;

// ---------------------------------------------------------------------------------------------------------------------
