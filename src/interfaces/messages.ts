// ---------------------------------------------------------------------------------------------------------------------
// Message Interfaces
// ---------------------------------------------------------------------------------------------------------------------

export type ConcurrencyCheck = () => Promise<boolean>;

// ---------------------------------------------------------------------------------------------------------------------

export interface MessageEnvelope<PayloadType = Record<string, unknown>>
{
    id : string;
    messageType : 'request' | 'response' | 'post';
    context : string;
    operation : string;
    timestamp : string;
    payload : PayloadType;
}

// ---------------------------------------------------------------------------------------------------------------------

export interface PostEnvelope<
    PayloadType = Record<string, unknown>,
    MetadataType = Record<string, unknown>,
> extends MessageEnvelope<PayloadType>
{
    messageType : 'post';
    metadata : MetadataType;
    auth ?: string;
    priorRequest ?: string;
    client ?: string;
    requestChain ?: string[];
}

// ---------------------------------------------------------------------------------------------------------------------

export interface RequestEnvelope<
    PayloadType = Record<string, unknown>,
    MetadataType = Record<string, unknown>,
> extends MessageEnvelope<PayloadType>
{
    messageType : 'request';
    responseQueue ?: string;
    timeout ?: number;
    metadata : MetadataType;
    auth ?: string;
    priorRequest ?: string;
    client ?: string;
    requestChain ?: string[];
}

export type RequestOrPostEnvelope<P = Record<string, unknown>, M = Record<string, unknown>> =
    (RequestEnvelope<P, M> | PostEnvelope<P, M>);

// ---------------------------------------------------------------------------------------------------------------------

export const ResponseMessageSeverity = [ 'error', 'warning', 'info', 'debug' ] as const;
export type ResponseMessageSeverity = typeof ResponseMessageSeverity[number];

export interface ResponseMessage<DetailsType = Record<string, unknown>>
{
    severity : ResponseMessageSeverity;
    message : string;
    code ?: string;
    type ?: string;
    details ?: DetailsType;
    stack ?: string;
}

export interface ResponseEnvelope<PayloadType = Record<string, unknown>> extends MessageEnvelope<PayloadType>
{
    id : string;
    messageType : 'response';
    context : string;
    operation : string;
    timestamp : string;
    payload : PayloadType;
    status : 'succeeded' | 'failed' | 'pending';
    messages : ResponseMessage[];
    service : string;
}

export type InternalResponseEnvelope<P = Record<string, unknown>> = ResponseEnvelope<P> & { internalID : string; };

// ---------------------------------------------------------------------------------------------------------------------
