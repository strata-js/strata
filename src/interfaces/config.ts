//----------------------------------------------------------------------------------------------------------------------
// Config Interfaces
//----------------------------------------------------------------------------------------------------------------------

import { RedisOptions } from 'ioredis';
import { LoggerConfig } from '@strata-js/util-logging';

//----------------------------------------------------------------------------------------------------------------------

export interface DiscoveryConfig
{
    enabled ?: boolean;
}

export interface RedisDiscoveryConfig extends DiscoveryConfig
{
    /**
     * The interval at which the service discovery key is updated in seconds
     */
    interval ?: number;

    /**
     * Expiration time for the service discovery key in seconds.
     */
    ttl ?: number;

    /**
     * The Redis DB to use for the service discovery
     */
    db ?: number;
}

export interface BackendConfig
{
    type : string;
    discovery ?: DiscoveryConfig;
    [key : string] : any;
}

export interface RedisBackendConfig extends BackendConfig
{
    discovery ?: RedisDiscoveryConfig;
    redis : RedisOptions;
    isolateCommands ?: boolean;
}

//----------------------------------------------------------------------------------------------------------------------

export interface TooBusyConfig
{
    maxLag ?: number;
    interval ?: number;
    smoothingFactorOnRise ?: number;
    smoothingFactorOnFall ?: number;
}

export interface ClientConfig
{
    name ?: string;
    id ?: string;
}

export interface ServiceConfig
{
    serviceGroup : string;
    concurrency ?: number;
    defaultRequestTimeout ?: number;
    toobusy ?: TooBusyConfig;
}

export interface StrataConfig
{
    logging ?: LoggerConfig;
    client ?: ClientConfig;
    service : ServiceConfig;
    backend : BackendConfig;
    aliases ?: Record<string, string>;
    interruptsToForceShutdown ?: number;
    shutdownTimeout ?: number;
}

// A version of StrataConfig that does not require client to be defined.
export type StrataServiceConfig = StrataConfig;

// A version of StrataConfig that does not require service to be defined.
export type StrataClientConfig = Omit<StrataConfig, 'service'> & {
    service ?: Partial<ServiceConfig>;
};

//----------------------------------------------------------------------------------------------------------------------
