// ---------------------------------------------------------------------------------------------------------------------
// Command Interfaces
// ---------------------------------------------------------------------------------------------------------------------

import { IsValidPayloadType } from './context.js';

// ---------------------------------------------------------------------------------------------------------------------
// Command Payloads
// ---------------------------------------------------------------------------------------------------------------------

export interface StrataConcurrencyCommandPayload
{
    concurrency : number;
}

export interface StrataShutdownCommandPayload
{
    graceful ?: boolean;
    exitCode ?: number;
}

export interface StrataTooBusyCommandPayload
{
    maxLag ?: number;
    interval ?: number;
    smoothingFactorOnRise ?: number;
    smoothingFactorOnFall ?: number;
}

// ---------------------------------------------------------------------------------------------------------------------
// Command Envelopes
// ---------------------------------------------------------------------------------------------------------------------

const validCommandNames = [ 'info', 'concurrency', 'shutdown', 'toobusy' ] as const;
export type ValidCommandName = typeof validCommandNames[number];

export interface ServiceCommandResponse<Payload = Record<string, unknown>>
{
    id : string,
    payload ?: IsValidPayloadType<Payload>
}

export interface ServiceCommand<Payload = Record<string, unknown>>
{
    id : string,
    command : ValidCommandName;
    payload ?: IsValidPayloadType<Payload>;
}

export interface StrataInfoCommand extends ServiceCommand<undefined>
{
    command : 'info';
    responseChannel : string;
}

export interface StrataConcurrencyCommand extends ServiceCommand<StrataConcurrencyCommandPayload>
{
    command : 'concurrency';
    payload : StrataConcurrencyCommandPayload;
}

export interface StrataShutdownCommand extends ServiceCommand<StrataShutdownCommandPayload>
{
    command : 'shutdown';
    payload ?: StrataShutdownCommandPayload;
}

export interface StrataToobusyCommand extends ServiceCommand<StrataTooBusyCommandPayload>
{
    command : 'toobusy';
    payload : StrataTooBusyCommandPayload;
}

export type KnownServiceCommand = StrataInfoCommand | StrataConcurrencyCommand | StrataShutdownCommand
    | StrataToobusyCommand;

// ---------------------------------------------------------------------------------------------------------------------
