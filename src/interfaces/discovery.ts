// ---------------------------------------------------------------------------------------------------------------------
// Interfaces related to Service Discovery
// ---------------------------------------------------------------------------------------------------------------------

import { ServiceInfo } from './service.js';

// ---------------------------------------------------------------------------------------------------------------------

export interface QueueDetails 
{
    'length' : number;
    'radix-tree-keys' : number;
    'radix-tree-nodes' : number;
    'last-generated-id' : string;
    'groups' : number;
    'first-entry' : string[];
    'last-entry' : string[];
}

export interface ConsumerDetails 
{
    name : string;
    outstanding : number;
}

export interface ServiceGroupDetails 
{
    'name' : string;
    'consumers' ?: ConsumerDetails[];
    'pending' : number;
    'last-delivered-id' : string;
}

export interface ServiceGroup 
{
    queues : Record<string, {
        contexts : Record<string, string[]>;
        consumers : ConsumerDetails[];
    }>;
}

export interface DiscoveredQueueDetails 
{
    groups : ServiceGroupDetails[];
}

export type DiscoveredQueues = Record<string, DiscoveredQueueDetails>;

export type DiscoveredServiceGroups = Record<string, ServiceGroup>;

// ---------------------------------------------------------------------------------------------------------------------

export interface DiscoveredService extends ServiceInfo 
{
    lastSeen : string;
}

export type DiscoveredServices = Record<string, Record<string, DiscoveredService>>;

// ---------------------------------------------------------------------------------------------------------------------
