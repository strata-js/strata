// ---------------------------------------------------------------------------------------------------------------------
// Strata Main Module
// ---------------------------------------------------------------------------------------------------------------------

// Utils
import localStorage from './utils/localStorage.js';

// ---------------------------------------------------------------------------------------------------------------------

// Export Internal classes
export { StrataContext as Context } from './lib/context.js';
export { StrataRequest as Request } from './lib/request.js';
export { StrataClient } from './clients/client.js';
export { StrataService } from './clients/service.js';

export * as errors from './lib/errors.js';

// Export Interfaces
export * from './interfaces/backend.js';
export * from './interfaces/command.js';
export * from './interfaces/context.js';
export * from './interfaces/discovery.js';
export * from './interfaces/localStorage.js';
export * from './interfaces/messages.js';
export * from './interfaces/middleware.js';
export * from './interfaces/service.js';

// Only export specific pieces of the configuration
export type { StrataConfig, StrataServiceConfig, StrataClientConfig } from './interfaces/config.js';

// Export Managers
export * from '@strata-js/util-logging';

// Export Utils
export { localStorage };
export * from './utils/request.js';

// ---------------------------------------------------------------------------------------------------------------------
