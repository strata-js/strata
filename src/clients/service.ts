//----------------------------------------------------------------------------------------------------------------------
// StrataService
//----------------------------------------------------------------------------------------------------------------------

import toobusy from 'node-toobusy';
import ld from 'lodash';
import { logging } from '@strata-js/util-logging';
import { StrataContext } from '../lib/context.js';
import { StrataRequest } from '../lib/request.js';
import {
    AlreadyInitializedError,
    AlreadyRegisteredError,
    UnknownContextError,
} from '../lib/errors.js';

// Interfaces
import { ServiceInfo } from '../interfaces/service.js';
import { BackendConstructor } from '../interfaces/backend.js';
import { OperationMiddleware } from '../interfaces/middleware.js';
import { StrataServiceConfig, TooBusyConfig } from '../interfaces/config.js';

// Engines
import { ServiceQueueEngine } from '../engines/queue.js';

// Managers
import backendMan from '../managers/backend.js';

// Libs
import { getHostname } from '../utils/hostname.js';

// Resource Access
import { BaseStrataBackend } from '../resource-access/backends/base.js';

// Utils
import { genID } from '../utils/id.js';
import shutdownUtil from '../utils/shutdown.js';
import versionUtil from '../utils/version.js';

//----------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('service');

//----------------------------------------------------------------------------------------------------------------------

export class StrataService
{
    readonly #id = genID();
    readonly #queueEngine : ServiceQueueEngine;
    readonly #middleware = new Set<OperationMiddleware>();
    readonly #contexts = new Map<string, StrataContext>();
    readonly #teardownFunctions = new Set<() => Promise<void>>();

    #serviceGroup : string;
    #concurrency : number;

    #config : StrataServiceConfig = {
        backend: { type: 'none' },
        service: { serviceGroup: 'UnknownService' },
    };

    #initialized = false;
    #lastBusy = { time: new Date(), lag: 0 };

    //------------------------------------------------------------------------------------------------------------------
    // Properties
    //------------------------------------------------------------------------------------------------------------------

    get config() : StrataServiceConfig
    {
        return this.#config;
    }

    get id() : string
    {
        return this.#id;
    }

    get name() : string
    {
        return versionUtil.appName;
    }

    get displayName() : string
    {
        return ld.startCase(this.name);
    }

    get serviceGroup() : string
    {
        return this.#serviceGroup;
    }

    get version() : string
    {
        return versionUtil.appVersion;
    }

    get concurrency() : number
    {
        return this.#concurrency;
    }

    get initialized() : boolean
    {
        return this.#initialized;
    }

    get outstandingRequests() : Map<string, StrataRequest>
    {
        return this.#queueEngine.outstandingRequests;
    }

    get lastBusy() : { time : Date, lag : number }
    {
        return this.#lastBusy;
    }

    get queueEngine() : ServiceQueueEngine
    {
        return this.#queueEngine;
    }

    get middleware() : Set<OperationMiddleware>
    {
        return this.#middleware;
    }

    get backends() : string[]
    {
        return backendMan.getNames();
    }

    get contexts() : Record<string, string[]>
    {
        return Array.from(this.#contexts.keys())
            .reduce((accum, contextName) =>
            {
                const contextInst = this.#contexts.get(contextName);
                if(contextInst)
                {
                    accum[contextName] = contextInst.getOperationNames();
                }
                return accum;
            }, {});
    }

    get info() : ServiceInfo
    {
        return {
            id: this.id,
            serviceName: this.displayName,
            serviceGroup: this.serviceGroup,
            version: this.version,
            strataVersion: versionUtil.libVersion,
            environment: process.env.ENVIRONMENT ?? 'local',
            concurrency: this.concurrency,
            outstanding: this.outstandingRequests.size,
            hostname: getHostname(),
            lastBusy: this.lastBusy,
            contexts: this.contexts,
            backend: this.#queueEngine.backend.info,

            // For backwards compatibility with v1 clients
            name: this.serviceGroup,
            queue: `Requests:${ this.serviceGroup }`,
        };
    }

    //------------------------------------------------------------------------------------------------------------------

    constructor(config : StrataServiceConfig)
    {
        this.#config = config;

        // Configure service options
        this.#serviceGroup = config.service?.serviceGroup ?? 'UnknownService';
        this.#concurrency = config.service?.concurrency ?? 32;

        // Configure node-toobusy
        const busyOptions = config.service?.toobusy;
        if(busyOptions)
        {
            for(const opt in busyOptions)
            {
                toobusy[opt] = busyOptions[opt];
            }
        }

        toobusy.onLag((lag) =>
        {
            this.#lastBusy = { time: new Date(), lag };
        });

        // Create the queue engine
        this.#queueEngine = new ServiceQueueEngine(this);

        // Set up the shutdown utility
        shutdownUtil.registerService(this);
        shutdownUtil.init(config.shutdownTimeout, config.interruptsToForceShutdown);

        // Register the service context
        const serviceContext = new StrataContext();
        serviceContext.registerOperation('info', async() => this.info);
        this.registerContext('service', serviceContext);
    }

    //------------------------------------------------------------------------------------------------------------------
    // Internal Methods
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Returns true if the service is ok to pick up more messages.
     *
     * @private
     */
    private async $highWaterMarkCheck() : Promise<boolean>
    {
        let belowWatermark = true;

        // Check if we're too busy
        belowWatermark = belowWatermark && !toobusy();

        // Check our concurrency limits
        belowWatermark = belowWatermark && this.outstandingRequests.size < this.#concurrency;

        // Return results
        return belowWatermark;
    }

    //------------------------------------------------------------------------------------------------------------------
    // Public Methods
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Initialize the service, and start listening for incoming messages. This function should be the last thing you
     * call, after registering all middleware and contexts and configuration.
     */
    public async start() : Promise<void>
    {
        if(this.initialized)
        {
            throw new AlreadyInitializedError('Initialization', 'service');
        }

        // Initialize utils
        await versionUtil.init();

        // Initialize the message queue
        await this.#queueEngine.init();

        // Check to see if we should enable discovery (defaults to true, if not specified)
        if(this.config.backend.discovery?.enabled !== false)
        {
            this.#queueEngine.enableDiscovery();
        }

        // Listen to the queue
        await this.#queueEngine.listenForCommands(this.serviceGroup, this.id);
        await this.#queueEngine.listenForRequests(this.serviceGroup, this.id, this.$highWaterMarkCheck.bind(this));

        // Now we're initialized
        this.#initialized = true;

        // Start up message
        logger.info(`${ this.displayName } v${ this.version } (${ this.id }) listening to service group`
            + ` '${ this.serviceGroup }'`);
    }

    /**
     * Registers a teardown function to be executed when the service is torn down.
     * @param teardownFn
     */
    public registerTeardown(teardownFn : () => Promise<void>) : void
    {
        this.#teardownFunctions.add(teardownFn);
    }

    /**
     * Registers a context with the service. All registered operation functions will be exposed via this context.
     *
     * @param contextName - The name to expose this context under.
     * @param context - The context to register.
     */
    public registerContext(contextName : string, context : StrataContext) : void
    {
        if(this.#contexts.has(contextName))
        {
            throw new AlreadyRegisteredError(contextName, 'context');
        }

        this.#contexts.set(contextName, context);
        logger.debug(`Successfully registered '${ contextName }' context.`);
    }

    /**
     * Retrieves a context by name. If the context isn't found, it throws an error.
     *
     * @param contextName - The context name to retrieve.
     *
     * @throws Throws an error in the event a context isn't found. The caller is expected to handle this and fail any
     * requests that triggered retrieving this context.
     *
     * @returns Returns the requested context.
     */
    public getContext(contextName : string) : StrataContext
    {
        const context = this.#contexts.get(contextName);
        if(!context)
        {
            throw new UnknownContextError(contextName);
        }

        return context;
    }

    /**
     * Registers a backend constructor under a given name. This allows the backend to be used with Strata under the
     * given name. (All our built-in backends use this registration internally.) Throws an error if the backend is
     * already registered.
     *
     * @param backendName - The name of the backend. (This should match the `name` parameter in the configuration.)
     * @param backendClass - A constructor for the backend instance.
     */
    public registerBackend(backendName : string, backendClass : BackendConstructor) : void
    {
        backendMan.register(backendName, backendClass);
        logger.debug(`Successfully registered '${ backendName }' backend.`);
    }

    /**
     * Get an instance of the given backend.
     *
     * @param backendName - The name of the backend. (This should match the `name` parameter in the configuration.)
     */
    public getBackend<T extends BaseStrataBackend<any>>(backendName : string) : T
    {
        return backendMan.getInstance<T>(backendName);
    }

    /**
     * Registers a piece (or pieces) of middleware to be used for all operation calls. Middleware registered with this
     * method are considered 'global' middleware.
     *
     * @param middleware - The middleware to register.
     */
    public useMiddleware(middleware : OperationMiddleware | OperationMiddleware[]) : void
    {
        if(!Array.isArray(middleware))
        {
            middleware = [ middleware ];
        }

        middleware.forEach((mw) =>
        {
            this.#middleware.add(mw);
        });
    }

    /**
     * Sets the number of messages this service will process at any given time.
     *
     * @param concurrency - the concurrency of the service.
     */
    public setConcurrency(concurrency : number) : void
    {
        logger.debug('Setting concurrency:', concurrency);
        this.#concurrency = concurrency;
    }

    /**
     * Set option for node-toobusy dynamic concurrency control
     *
     * @param busyOptions
     */
    public setTooBusy(busyOptions : TooBusyConfig) : void
    {
        logger.debug('Setting too busy options:', JSON.stringify(busyOptions));

        for(const opt in busyOptions)
        {
            toobusy[opt] = busyOptions[opt];
        }
    }

    /**
     * Shuts down and cleans up the service.
     *
     * **This should never be called from external code.** (It is only public, because it needs to be exposed beyond
     * this class.)
     */
    async teardown() : Promise<void>
    {
        const { contexts, ...simpleInfo } = this.info;
        logger.info(`Tearing down service '${ this.displayName }':`, simpleInfo);

        // We need to immediately stop listening for requests and commands.
        await this.#queueEngine.stopListeningForRequests(this.name);
        await this.#queueEngine.stopListeningForCommands();

        // Disable discovery
        await this.#queueEngine.disableDiscovery();

        // Wait for all the outstanding requests to be finished.
        await Promise.all([
            ...this.outstandingRequests.values(),
        ]
            .map((request) => request.promise.catch()));

        // Cleanup message queue
        await this.#queueEngine.teardown();

        // Call any teardown functions for the context/operations middlewares
        await Promise.all([ ...this.#contexts ].map(([ _name, context ]) =>
        {
            [ ...context.operationsMiddlewareList, ...context.contextMiddlewareList ].map((mw) =>
            {
                if(typeof mw.teardown === 'function')
                {
                    return mw.teardown();
                }
            });
        }));

        // Call any teardown functions for the service middlewares
        await Promise.all([ ...this.#middleware ].map((mw) =>
        {
            if(typeof mw.teardown === 'function')
            {
                return mw.teardown();
            }
        }));

        // Call any registered teardown functions
        await Promise.all([ ...this.#teardownFunctions ].map((teardownFn) => teardownFn()));
    }
}

//----------------------------------------------------------------------------------------------------------------------
