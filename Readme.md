# Strata.js

<div align="center">

![Strata Logo](https://gitlab.com/strata-js/strata/-/raw/master/docs/images/logo_banner_white.png)
</div>

> The fast, opinionated, minimalist microservice framework for node.

Strata.js is a microservice framework built off of [Redis Streams][redis-streams] and inspired by [expressjs][].

## Installation

Strata.js is published on the public npm registry, under the [@strata-js][strata-scope] scope. Simply install it like normal:

```bash
// npm
$ npm add @strata-js/strata

// yarn
$ yarn add @strata-js/strata
```
[strata-scope]: https://www.npmjs.com/org/strata-js
[redis-streams]: https://redis.io/topics/streams-intro
[expressjs]: https://expressjs.com/

### Middleware

Strata.js has some official middleware, which you can find on GitLab:

* https://gitlab.com/stratajs/middleware

These packages are also published under the `@strata-js` scope, in the form of `@strata-js/middleware-<thing>.

### Utilities

Strata.js also has some official utilities, which you can find on GitLab:

* https://gitlab.com/stratajs/utils

These packages are also published under the `@strata-js` scope, in the form of `@strata-js/util-<thing>.

## Documentation

Strata.js's documentation is currently a work in progress, but can be found in the [docs](./docs) folder.
