# Strata.js Documentation

## Guide

* [How Strata.js Works](./guide/overview.md)
* [Redis Streams Usage](./guide/redis-streams.md)
* [On-wire Message Protocol](./guide/protocol.md)
* [Strata.js Contexts](./guide/contexts.md)
* [Strata.js Middleware](./guide/middleware.md)
* [Basics of Strata.js Services](./guide/service.md)
* [Basics of Strata.js Clients](./guide/client.md)
* [How to configure a service](./guide/configuration.md)

## Reference

Currently, we don't have any reference documentation. We're working on it, but it's not ready yet. You can check the 
source code directly for that.
