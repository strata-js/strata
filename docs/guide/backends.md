# StrataBackends

Strata uses a backend system to allow for support across multiple queue implementations, using the same core logic. 
The way this is designed, all services that need to communicate with each other must be on the same backend, however,
you can have multiple service instance or client instances connected to multiple backends. This should allow for the 
greatest possible flexibility, without being too opinionated about how you want to run your services.

Strata includes several backends to still try to be 'batteries-included'.

## Built-in Backends

The build in backends currently include:

* `'redis-streams'` - This is the most tested backend. It uses [RedisStreams][] for the request and response queues. 
  Commands are redis pub/subs, and discovery is a redis set.
* `'redis'` - This is a simplified version of the `'redis-streams'` backend, and uses Redis lists for the request and 
  response queues. Commands are redis pub/subs, and discovery is a redis set.
* `'null'` - This is a backend that does nothing. It's useful for testing, and not much more.

## Custom Backends

You can also create your own backends. To do so, you will need to create a class that implements the `Backend` 
interface. (Look at the `'null'` backend for a very basic implementation example.) A 'backend' can be any 
communication system you want, as long as you can make it work inside the `Backend` interface.

A simple example of a do-nothing backend might look like this:

```typescript
import { logging } from '@strata-js/util-logging';
import {
    BackendConfig, 
    BaseStrataBackend,
    ConcurrencyCheck,
    DiscoveredServices,
    KnownServiceCommand,
    PostEnvelope,
    RequestEnvelope,
    ResponseEnvelope,
    ServiceCommandResponse,
    StrataService
} from '@strata-js/strata';


const logger = logging.getLogger('nullBackend');

export interface ExampleBackendOptions extends BackendConfig
{
    type : 'example';
}

export class ExampleBackend extends BaseStrataBackend<ExampleBackendOptions>
{
    #initialized = false;

    get initialized() : boolean { return this.#initialized; }

    async init() : Promise<void>
    {
        logger.info('Example backend initialized.');

        this.#initialized = true;
    }

    async disableDiscovery() : Promise<void>
    {
        // TODO: Disable service discovery
    }

    async enableDiscovery(service : StrataService) : Promise<void>
    {
        // TODO: Enable service discovery
    }

    async listServices() : Promise<DiscoveredServices>
    {
        // TODO: List other running services
        return [];
    }

    async listenForCommands(serviceGroup : string, serviceID : string) : Promise<void>
    {
        // TODO: Listen for commands
    }

    async listenForRequests(
        serviceGroup : string,
        serviceID : string,
        _waterMarkCheck : ConcurrencyCheck
    ) : Promise<void>
    {
        // TODO: Listen for requests
    }

    async listenForResponses(clientID : string, clientName ?: string) : Promise<void>
    {
        // TODO: Listen for responses
    }

    async listenForCommandResponses(clientID : string) : Promise<void>
    {
        // TODO: Listen for command responses
    }

    async sendCommand(target : string, command : KnownServiceCommand) : Promise<void>
    {
        // TODO: Send command
    }

    async sendCommandResponse(target : string, envelope : ServiceCommandResponse) : Promise<void>
    {
        // TODO: Send command response
    }

    async sendRequest(serviceGroup : string, envelope : RequestEnvelope | PostEnvelope) : Promise<void>
    {
        // TODO: Send request
    }

    async sendResponse(target : string, envelope : ResponseEnvelope) : Promise<void>
    {
        // TODO: Send response
    }

    async stopListeningForCommands() : Promise<void>
    {
        // TODO: Stop listening for commands
    }

    async stopListeningForCommandResponses() : Promise<void>
    {
        // TODO: Stop listening for command responses
    }

    async stopListeningForRequests(serviceGroup : string) : Promise<void>
    {
        // TODO: Stop listening for requests
    }

    async stopListeningForResponses() : Promise<void>
    {
        // TODO: Stop listening for responses
    }

    async teardown() : Promise<void>
    {
        // TODO: Teardown backend
    }
}
```

### Using a Custom Backend

To use a custom backend, you will need to register it. This can be done by calling `registerBackend()` on either the 
Strata service, or client instances. (Note: You can register a backend on both, but the last one to be registered 
under that name will win.)

```typescript
import { ExampleBackend } from './example-backend';
import { StrataService } from '@strata-js/strata';

// Configuration
const config = {
    service: {
        serviceGroup: 'MyServiceGroup'
    },
    backend: {
        type: 'example'
    }
};

// Create the service instance
const service = new StrataService(config);

// Register the backend
service.registerBackend('example', ExampleBackend);

// Start the service
await service.start();
```

You can do the same thing with a client:

```typescript
import { ExampleBackend } from './example-backend';
import { StrataClient } from '@strata-js/strata';

// Configuration
const config = {
    client: {
        name: 'MyClient'
    },
    backend: {
        type: 'example'
    }
};

// Create the client instance
const client = new StrataClient(config);

// Register the backend
client.registerBackend('example', ExampleBackend);

// Start the client
await client.start();
```

[RedisStreams]: https://redis.io/docs/data-types/streams/
