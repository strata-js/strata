# Strata.js Request Object

This object represents the full request throughout the request lifecycle. It contains the initial request properties, as
well as response properties, as the response is built. Generally, it shouldn't need to be used directly, but if you 
are writing middleware, you can be forced to use it.

The request object has an internal promise that is resolved when the request is completed. This promise is exposed via
the `request.promise` property.

## Succeeding a Request

To succeed a request, you will need to call `request.succeed()`. This will set the response status to `success`, and
set the response payload to the value passed in.

```typescript
request.succeed({ foo: 'bar' });
```

Once a request has been succeeded, it's internal promise is resolved. It cannot be failed, or re-succeeded.

## Failing a Request

To fail a request, you will need to call `request.fail()`. This takes a 'reason', which can be either an error 
instance, a string, or a full payload object. This will set the response status to `failure`, and set the response 
payload to the reason passed in.

```typescript
request.fail({ error: 'Something went wrong' });
```

Once a request has been failed, it's internal promise is resolved. It cannot be succeeded, or re-failed.

## Rendering Requests and Responses

In the course of normal use, you shouldn't have to render requests or responses. However, if you are writing 
middleware, there may be times when you need to render a request or response. To do this, you will need to call
`request.render()` or `response.render()`. This will return the rendered object.

```typescript
// renderedRequest will be a plain object with no methods.
const renderedRequest = request.render();

// renderedResponse will be a plain object with no methods.
const renderedResponse = response.render();
```

These objects can be useful for logging, debugging, or for creating clones of the request.

