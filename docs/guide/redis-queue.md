# How Strata.js uses Redis Queues

The Strata.js `redis-queue` backend is built on top of `io-redis`, which just uses the [redis list][lists] API. 
Specifically, it uses the `BLPOP` and `RPUSH` commands to pop and push messages from the queue. It is a very simple, 
very fast api. While it doesn't have the same features as the [redis streams][streams] API, it is very performant, 
and is very conceptually simple, without any of the bookkeeping that streams requires.

## Service Usage

### Setup

There is no set up required for the service. It just needs to connect to redis.

### Listening for requests

To listen for requests, we do the following command:

`BLPOP queueName <timeout>`

That's it. We will get a message back, or we will timeout. If we get a message, we parse it, and start processing it,
hitting middleware, operation handlers, etc. Once the message is either succeeded or failed, we move on to sending the
response.

#### Queue Names

By default, we use the convention `Requests:${ serviceName }` for queues. We recommend, in environments where developers
need to run their own copies of services on the same bus as a global group of services, that they append 
`${ hostname() }` on the end of the queue name.

### Sending a response

Once a response has been succeeded or failed, and run through all the middleware, it is rendered out to the response
envelope format. We then send the following:

`RPUSH responseQueueName "{...}"`

That is it. We just push the response to the queue. The client will pop it off, and process it.

## Client Usage

### Setup

There is no set up required for the client. It just needs to connect to redis.

### Sending requests

To send a request, we do the following command:

`RPUSH queueName "{...}"`

That is it. We just push the request to the queue. The service will pop it off, and process it.

### Listening for responses

To listen for responses, we do the following command:

`BLPOP responseQueueName <timeout>`

That's it. We will get a message back, or we will timeout. If we get a message, we parse it, and start processing it,
hitting middleware, operation handlers, etc.

[streams]: https://redis.io/topics/streams-intro
[lists]: https://redis.io/docs/data-types/lists/
