# Strata.js Overview

## What is Strata.js?

Strata.js is a microservice framework for building SaaS solutions that communicate through a message bus and can scale
both horizontally and vertically. The term "microservice" is not necessarily accurate; Strata.js gives you tools for
building services, and how "micro" they are is entirely up to you. While we refer to Strata.js as "an opinionated
framework", it's opinionated about what a service is, how you scale, and what communications protocol you use, _not_ how
you build your services, or how your code is structured.

Think of Strata.js as the younger, cooler sister to [express.js](https://expressjs.com/). Instead of REST, we use
message queues.

## Message Queues And You

The fundamental principle of Strata.js is that message queues are magical things that give your services superpowers. In
its most basic form, a message queue can be thought of as in-memory list:

```
Write -> [ Message3, Message2, Message1 ] -> Read
```

New messages are pushed to the back of the queue, while your service reads from the front. A single message is "popped"
from the queue, handled by your service, and then once it's ready, it grabs another one. How is this magical? The magic
comes in when you add multiple services reading from the queue:

```
                                                      /-> Read (Service 1)
Write (Client 1) -> [ Message3, Message2, Message1 ] ---> Read (Service 2)
                                                      \-> Read (Service 3)
```

Each service gets a unique message (i.e. two services will never get the same message), but the client never had to know
about how many services are listening, or where to send them. It just writes to the queue, and moves on. If we need to
process more messages, we just add more services. If there's a fault in a service, we restart it, and the others pick up
the slack while it's down. Already we've taken the pains of distributing our work load from something complex like
clustering and turned it into something simple, where the service topology can be dynamic based on demand, available
resources, etc.

### There's always a "but..."

Astute readers will notice that a message queue is unidirectional. In order to have communication back to the client, we
need to use a second queue. This one is just an inverse of the first one:

```
                                                      /- Write (Service 1)
Read (Client 1) <- [ Message3, Message2, Message1 ] <--- Write (Service 2)
                                                      \- Write (Service 3)
```

One subtle piece of this, is that on the return channel, _multiple_ services write to a _single_ client. Each client
needs its own return queue, but all services share a single work queue. While this might sounds like it adds some
complexity, all it means is that with each request a client sends, it needs to inform the service where to put the
response, if it needs one. As long as there's some mechanism for matching responses to requests, it doesn't matter what
order the responses come in, the client can work it out.

### This is the Way

Strata.js handles this complexity by providing both a [service](service.md) and a [client](client.md). These two
pieces work hand in hand to make it easy for you to think in terms of "making requests" and "getting responses", instead
of worrying about the underlying mechanics of what that looks like. Even REST forces you to consider endpoints, headers,
cookies, etc. Strata.js makes those decisions for you (hence "opinionated") so that you can focus on the important
business logic of your services, without caring how the magic under the hood happens.

## Strata.js and Message Queues

Strata.js can use anything for its message queues, however, by default it uses either [redis queues](./redis-queues.md)
or [redis streams](./redist-streams.md) to handle the message queue functionality. Conceptually, a Strata.js service 
listens on a queue, while a Strata.js client pushes messages to that queue. One or more instances of the service 
will pop messages from the queue, work them, and respond. Each request is wrapped in an envelope that contains 
important information like the request id, the response queue, the context, operation, payload, metadata, 
authentication, etc. This envelope is what allows Strata.js to do its magic.

### Service Groups

Strata.js services are grouped together into "service groups". This is a logical grouping of services that are all
listening on the same queue. This allows you to have multiple instances of a service running, and have them all work 
from the same incoming pool of messages. This is how you scale (horizontally) with Strata. If you need to process 
messages faster, you can simply add more instances of the service to the group. Strata.js doesn't even have to do 
anything special, due to how everything is structured those new services will immediately start getting work from 
the queue and processing it.

The reason why we call this a `service group` instead of a `queue` is because Strata supports custom backends. 
There are valid implementations of a backend that don't use `queues` at all. As long as it can be forced into the 
correct interface, it can be used as a backend. (See [Backends](backends.md) for more information.)

## The Structure of a Service

A Strata.js service is made up of the `service` instance, `contexts` that handle various `operations`, `middleware` that
modifies incoming requests, and some number of `client` instances that handles making requests to services, and getting 
responses. (It is perfectly valid to have a service without a client instance; you only need one if you are making 
calls across service groups.)

### Contexts

You can think of a `Context` and a [express.js Router](https://expressjs.com/en/4x/api.html#router). You create an
instance of it, call some functions on it to handle `operations`, and you register it with the `service` instance. (For
more detail, see [Contexts](contexts.md).) The whole point is to provide a very simple mechanism for you to declare
the various `operations` your context supports, and get the requests to your code for processing.

#### Operations

An `operation` is like a "task" or "verb". It's a unit of work that can be done. If `contexts` are the
Router, `operations` are the endpoints. They are where your business logic goes.

```typescript
import { Context } from '@strata-js/strata';

// Declare context
const context = new Context();

context.registerOperation('doStuff', async(request) =>
{
    // Handle request
});

// Don't forget to export the context!
export default context;
```

### Middleware

Middleware are small libraries that modify requests or responses. Conceptually, they're wrapper functions that make some
changes. However, as express.js has shown, you can build powerful functionality by using pluggable middleware. 
(See [Middleware](middleware.md) for more information.)
