# Service Commands

> **NOTE:** This is v1 documentation. This information is out of date and some of this has changed with the 
> introduction of backends. This will be updated soon.

Strata.js services support the ability to send commands to running services. On the `redis` and `redis-streams` 
backends, this is done via a redis [pubsub][], however the mechanism isn't important. These messages are considered 
best-effort delivery, and will be processed in the order received. Delivery isn't guaranteed; while the actual 
communication guarantees are up to the backend, the `redis` and `redis-streams` backends will not retry sending a 
command if the service is not available, and it is best to assume that commands may be lost.

The intention for these commands is to allow for simple control of a running service, without having to restart it. 
Basic configuration changes, or even a graceful shutdown can be initiated via these commands. They are more intended 
for administrative use, and not for normal operation.

## How it works

Commands are modeled after a pattern matching channel model, similar to `psubscribe` in redis. Commands can be 
issued at one of three different levels:

* `Services` - These are processed by every service connected to this redis host/cluster.
* `Services:<ServiceGroup>` - These are processed by every service in a particular service group identified by 
  `ServiceGroup`.
* `Services:<ServiceGroup>:<ServiceID>` - These are processed by a single service identified by `ServiceID`. 

Commands are json objects with the following interface:

```typescript
interface ServiceCommand<Payload> {
    command : string;
    payload ?: Payload;
}
```

This simple envelope allows the service to handle specific messages, and warn if it receives something it doesn't 
understand.

## Commands

The following are the commands currently supported by Strata.js.

### Info

```json
{
    "command": "info",
    "payload": {
        "responseChannel": "Clients:MyClient"
    }
}
```

Get the information about any service responding. Payload contains a [pubsub][] channel to send the response to. This
response returns the same information as if responding to a [`service.info`][info] request. A response channel is
required.

#### Redis example:

`redis> PUBLISH "Services:MyService" "{\"command\":\"info\",\"payload\":{\"responseChannel\":\"Clients:MyClient\"}}"`

### Concurrency

```json
{
    "command": "concurrency",
    "payload": {
        "concurrency": 25
    }
}
```

Sets the concurrency of the service. Payload must be an object with a single key `concurrency` set to any number.

#### Redis example:

`redis> PUBLISH "Services:MyService" "{\"command\":\"concurrency\",\"payload\":{\"concurrency\":25}}"`

### TooBusy

```json
{
    "command": "toobusy",
    "payload": {
        "maxLag": 70,
        "interval": 500,
        "smoothingFactorOnRise": 0.33,
        "smoothingFactorOnFall": 0.75
    }
}
```

Sets the various options of `node-toobusy` to fine-tune its dynamic concurrency control

#### Redis example:
`redis> PUBLISH "Services:MyService" "{\"command\":\"toobusy\",\"payload\":{\"maxLag\":70,\"interval\":500,\"smoothingFactorOnRise\":0.33,\"smoothingFactorOnFall\":0.75}}"`


### Shutdown

```json
{
    "command": "shutdown",
    "payload": {
        "graceful": false,
        "exitCode": 99
    }
}
```

Initiate a shutdown of the service. If `graceful` is set to `false`, the service process will immediately exit. Any 
other value will be treated as if `true` and a graceful shutdown will be performed, before exiting. If `exitCode` is 
passed, the process will exit with the passed in error code, otherwise it will exit with `0`.

If you want a graceful shutdown with exit code 0, `payload` may be omitted.

#### Redis example:

`redis> PUBLISH "Services:MyService" "{\"command\":\"shutdown\",\"payload\":{\"graceful\":false,"exitCode":99}}"`

## Sending Commands

The Strata.js client is capable of sending these commands, with whatever specificity you want:

```typescript
// Assume we get a client somehow
client.command('Services:MyServiceGroup', 'concurrency', { concurrency: 5 });

// Or, if you want to send to a specific service
client.command('Services:MyServiceGroup:<ServiceID>', 'concurrency', { concurrency: 5 });
```

The `client.command()` function will send the desired command to the services you specify. 

### Sending directly without Strata

Depending on the implementation of the backend, it should be possible to send commands directly without the use of 
Strata.js. The commands are simple json objects, and can be sent to the appropriate channel.

Here is an example of sending a command to a service using the `redis` or `redis-streams` backend:

```typescript
import Redis from 'ioredis';

const redis = new Redis();

//Command payload
const command = {
    command: 'concurrency',
    payload: {
        concurrency: 5
    }
};

// Send a command to a service
redis.publish('Services:MyServiceGroup', JSON.stringify(command));
```

[info]: service.md#info-operation
[pubsub]: https://redis.io/topics/pubsub
