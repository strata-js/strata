# Configuration

Prior versions of Strata.js had a built-in configuration system. This has been removed. Instead, what we provide is 
a configuration interface to make it easier to provide your own configuration from whatever sources you'd like.

## Recommended Library

However, since it's annoying to have to write your own configuration loader, we do provide a recommended library written
by the Strata.js team. It's called `@strata-js/util-config`. It's a simple, easy to use, configuration loader that 
supports multiple configuration sources, `yaml`, `json` and `json5`. We now recommend yaml as the preferred 
configuration format, as its easier to read and write than json, and it supports comments.

You can check out the library here: [util-config](https://gitlab.com/strata-js/utils/config).

## Configuration Interface

The configuration interface is split into two parts: `StrataServiceConfig` and `StrataClientConfig`. The `StrataServiceConfig` is used to
configure a `service` instance, while the `StrataClientConfig` is used to configure a `client` instance. (The 
`StrataServiceConfig` is a superset of the `StrataClientConfig`, so you can use the same configuration for both.)

### StrataServiceConfig

* [src/interfaces/config.ts](../../src/interfaces/config.ts)

The `StrataServiceConfig` interface only required two properties: `service` and `backend`. These can easily be 
defined simply as:

```typescript
import { StrataServiceConfig } from '@strata-js/strata';

const config: StrataServiceConfig = {
    service: {
        serviceGroup: 'MyServiceGroup'
    },
    backend: {
        type: 'redis'
    }
};
```

These two properties are the only one required to actually make a strata service function. However, there are a 
number of other properties that can be set to configure the service. These are all optional, and have reasonable
defaults. (See [StrataServiceConfig](../../src/interfaces/config.ts) for more information.)

### StrataClientConfig

* [src/interfaces/config.ts](../../src/interfaces/config.ts)

The `StrataClientConfig` interface only requires two properties: `client` and `backend`. These can easily be
defined simply as:

```typescript
import { StrataClientConfig } from '@strata-js/strata';

const config: StrataClientConfig = {
    client: {
        name: 'MyClient'
    },
    backend: {
        type: 'redis'
    }
};
```

These two properties are the only one required to actually make a strata client function. However, there are a
number of other properties that can be set to configure the client. These are all optional, and have reasonable
defaults. (See [StrataClientConfig](../../src/interfaces/config.ts) for more information.)
