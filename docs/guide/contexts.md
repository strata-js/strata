# Strata.js Contexts

A request in Strata.js is broken into two parts: a "context" and an "operation". You can think of it as a more flexible
version of an HTTP router, and request handler. However, instead of having a full URL to parse, we simply have three
levels:

* The name of the service group, i.e. `MyService`.
* The name of the context, i.e. `customerOrders`.
* The name of the operation, i.e. `list`, `save`, `updatePurchaseOrder`, etc.

In Strata.js, Contexts are the glue that join the service's business logic to the mechanisms of listening for and
responding to requests. They are intended to be rather thin, and mainly focused on handling the vagaries of routing or
parsing the request; they should not contain business logic beyond making calls to the developer's business logic.

> If your context handlers are more than a few dozen lines of code, it's worth considering refactoring those to better 
> encapsulate the code volatility. If your business logic lives on its own and your contexts mere call it, then it's 
> easier to handle changes in Strata.js's API in the future, or a change in your business requirements. Treat contexts 
> as a thin interface for getting and responding to requests, and build your own business classes for handling the 
> logic of your service.

## Declaring a context

Declaring a context is as simple as importing `Context` from Strata.js, and then making a new instance of it. That's it!
Here's an example to demonstrate:

```typescript
import { Context } from '@strata-js/strata';

// Declare context
const context = new Context();

// Don't forget to export it!
export default context;
```

It should be noted that you will need access to the `Context` instance outside of this module, so you will want
to export it. 

### Registering Middleware

Registering middleware is as simple as calling `context.useMiddleware()` and passing in the middleware instance you wish
to use. This will ensure this middleware is called for all operations on this context (but not on other contexts).

(See [Strata.js Middleware](middleware.md) for more information.)

### Declaring an Operation Handler

In order for a context to perform any useful work, it needs operations declared. Doing so is as simple as
calling `context.registerOperation()` and passing it the name of the operation, and a handler function. It's also
possible to pass in middleware to configure for this operation, if it's middleware needs are different from other
operations.

#### Registering Operations Handlers by instance

Sometimes you have a single instance (such as, say a Manager class) and you would like to register all public functions
on it as operation handlers. You can easily do that with `context.registerOperationsForInstance()`. This allows you to
add all methods as operations, blacklist some, and register middleware across all operations.

One small caveat is that this is a convenience method, and will not cover all scenarios. It does its best to ignore 
internal functions, but it's not perfect. If you need more control, you can always use `context.registerOperation()` 
and register each explicitly.

### Example

```typescript
import { Context } from '@strata-js/strata';

// Middleware
import caching from './src/middleware/caching';
import geojson from './src/middleware/geojson';

//----------------------------------------------------------------------------------------------------------------------

// Build a context
const context = new Context();

//----------------------------------------------------------------------------------------------------------------------

// Register middleware
context.useMiddleware(geojson);

// Register operation
context.registerOperation('listGeoms', async() =>
{
    // Would look up a list of geometries, but this is an example...
    return [];
});

// Register operation with middleware
context.registerOperation('getCentroid', async(request) =>
{
    // We would calculate the centroid, but since this is an example...
    return [ 0, 0 ];
}, [ caching ]);

//----------------------------------------------------------------------------------------------------------------------

export default context;

//----------------------------------------------------------------------------------------------------------------------
```

## Register a context

Registering a context is as simple as calling `service.registerContext()` and passing it the name of the context,
followed by the context instance. All contexts must be registered before the service is instantiated.

```typescript

import { service } from '@strata-js/strata';

import orderContext from './src/contexts/orders';
import fooContext from './src/contexts/orders';

// Register contexts
service.registerContext('order', orderContext);
service.registerContext('foo', fooContext);
service.registerContext('bar', fooContext); // Yes, you can register a context under multiple names.
```

One interesting thing to notice is that we registered `fooContext` under both `'foo'` and `'bar'`. This is perfectly
valid. There are no downsides to this, but be aware it will be the same context instance handling both endpoints. If
you're doing something like storing state on the context (which you never should do!) it will be shared between them.
