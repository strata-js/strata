# Request Middleware

Strata.js supports its own version of a very common pattern in HTTP based servers: "middleware". In those systems
middleware are request handlers that are called before the final handler that may modify the request (or something about
the environment of the request.)

In Strata.js, middleware have three hooks they can listen for, in order to modify the request. These hooks allow for a
single piece of middleware to handle modifying both the request and the response, or setting up additional system
behavior as the response is processed. Unlike HTTP middleware, we have multiple functions that make up a single piece of
middleware. This is to allow for easier handling of cases where both the incoming and outgoing message must be modified.

## Middleware Hooks

This is the request flow through the system. All registered middleware will have these hooks called; if they don't
implement the particular hook, it will be skipped.

```mermaid
graph TD
    classDef internal fill:#ccc,stroke:#555,color:#555
    classDef hook fill:#fff2cc,stroke:#D6B656,color:#666,font-family: monospaced
    subgraph Service
    pr{{Incoming Request}}:::internal
    br(["beforeRequest()"]):::hook
    h{{Handle Request}}:::internal
    s(["success()#nbsp;"]):::hook
    f(["failure()#nbsp;"]):::hook
    p{{Send Response}}:::internal
    pr --> br
    br --> h
    h --> s & f
    s & f --> p
    end
    style Service fill:#9C9,stroke:#292,color:#272
```

### `beforeRequest()` Hook

The `beforeRequest()` hook is the only hook allowed to modify the incoming request. It is called before the operation
handler has been called, so the request will not have been succeeded or failed by this point. It _is_, however, allowed
to succeed or fail the request, at which point, all other `beforeRequest()` hooks will be skipped as well as the
operation handler. It will immediately skip to calling either the `success()` or `failure()` hooks, as appropriate.

_Note: When discussing failing or succeeding the request, we're simply referring to calling `request.succeed()`
or `request.fail()`. It is also possible to fail the request by throwing an exception._

(See [Strata.js Request](request.md) for more information.)

### `success()` Hook

The `success()` hook is triggered when the request succeeds. It is only allowed to modify the response, not the initial
request.

### `failure()` Hook

The `failure()` hook is triggered when the request fails. It is only allowed to modify the response, not the initial
request.

## Middleware Implementation

Because of the use of hooks, we expect middleware to be implemented as an object (or more likely, a class instance) with
the three functions:

```typescript
import { Request } from 'strata';

class ExampleMiddleware
{
    public async beforeMiddleware(request: Request): Promise<Request>
    {
        // ... do stuff ...
    }

    public async success(request: Request): Promise<Request>
    {
        // ... do stuff ...
    }

    public async failure(request: Request): Promise<Request>
    {
        // ... do stuff ...
    }
}
```

A few points to note:

* _Hooks are considered asynchronous._ - Even if the work being done is synchronous, this allows for the most
  flexibility.
* _Hooks are processed serially_ - Even though we treat them as asynchronous, they are processed serially, meaning a
  long-running hook will delay processing of the entire message.
* _Middleware classes will need to be instantiated._ - Even though using a class is recommended, you will need to make a
  new instance of the class before registering the middleware. (Typescript should catch this, however.)

## Registering Middleware

There are three different ways to register middleware:

1. Globally for all requests in the service.
2. Globally for all requests in a given context. `context.registerMiddleware()`
3. Individually per operation. `context.operation()`

Here are some examples for registering middleware in all three ways:

```typescript

import { service, Context } from 'strata';

// Middleware
import security from './src/middleware/security';
import caching from './src/middleware/caching';
import geojson from './src/middleware/geojson';

//----------------------------------------------------------------------------------------------------------------------

// Build a context
const context = new Context();

//----------------------------------------------------------------------------------------------------------------------

// Register Global Middleware
service.useMiddleware(security);

// Register Context Middleware
context.useMiddleware(geojson);

// Register Operation Middleware
context.operation('getCentroid', async(request) =>
{
    // We would calculate the centroid, but since this is an example.. return a hardcoded one.
    return [ 0, 0 ];
}, [ caching ]);
```

### Middleware Calling Order

The middleware is called in the order it was registered, however it is called outside-in for `beforeRequest()` and
inside-out for `success()` and `failure()`.

```mermaid
graph LR
    classDef hook fill:#fff2cc,stroke:#D6B656,color:#666,font-family: monospaced
    classDef internal fill:#ccc,stroke:#555,color:#555

    g(["Global Middleware"]):::hook
    c(["Context Middleware"]):::hook
    o(["Operation Middleware"]):::hook
    h{{Handle Request}}:::internal
    
    g -->|"beforeRequest()"| c
    c -->|"beforeRequest()"| o
    o -->|"Call Handler"| h
    h -->|"Return Results"| o
    o -->|"success() / failure()"| c
    c -->|"success() / failure()"| g
```

So, to be explicit, for `beforeRequest()`, the call order is:

1. Global Middleware
2. Context Middleware
3. Operation Middleware

For both `success()` and `failure()` the call order is:

1. Operation Middleware
2. Context Middleware
3. Global Middleware

Only `beforeMiddleware()` supports skipping the rest of the `beforeMiddleware()` if one of the registered middleware
either succeeds or fails the request. **The `success()` and `failure()` middleware will always be called.**

_Note: Due to the nature of error handling in async functions, some types of exceptions will break the middleware
calling chain, and may circumvent some or all of the `success()` and `failure()` middleware. It is up to middleware
authors to make the best effort to catch these cases in their middleware._
